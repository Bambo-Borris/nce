# Todd Packer - The Asset Packer
add_executable(tp main.cpp)

target_sources(tp PRIVATE appicon.rc)

target_compile_features(tp PUBLIC cxx_std_20)
target_link_libraries(tp PRIVATE nce SFML::Graphics nlohmann_json zip)

if (MSVC)
    add_compile_options("/MP")
    target_compile_options(tp PRIVATE /W4 /WX /permissive- /wd4068)
    target_compile_definitions(tp PRIVATE _CRT_SECURE_NO_WARNINGS)
    set_property(TARGET tp PROPERTY
    MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
elseif (CMAKE_CXX_COMPILER_ID MATCHES "(GNU|Clang)")
    target_compile_options(tp PRIVATE -Werror -Wall -Wextra -Wpedantic -Wshadow -Wconversion -Wno-c++98-compat)
endif ()
