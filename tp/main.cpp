#include <nce/AssetJSON.hpp>
#include <nce/AssetsInfo.hpp>
#include <nce/Types.hpp>

#include <chrono>
#include <nlohmann/json.hpp>
#include <tl/expected.hpp>
#include <vector>
#include <zip.h>

#include <cstdio>

#define CHECK_ZIP_ERR(errNum)                                                                                                                                  \
    if (errNum != 0) {                                                                                                                                         \
        printf("Error! Unable to open zip!\nReason: %s", zip_strerror(errNum));                                                                                \
        return EXIT_FAILURE;                                                                                                                                   \
    }

#define CHECK_ZIP_ENTRY_ERR(errNum)                                                                                                                            \
    if (errNum != 0) {                                                                                                                                         \
        printf("Error! Unable to open zip!\nReason: %s", zip_strerror(errNum));                                                                                \
        return EXIT_FAILURE;                                                                                                                                   \
    }

int main()
{
    const auto beginTime = std::chrono::high_resolution_clock::now();
    nce::AssetJSON assetJson(nce::AssetJSON::FileOrigin::Disk);
    const auto result = assetJson.getJSONData();
    if (!result.has_value()) {
        printf("Error! Unable to load asset.json!\nReason: %s", result.error().data());
        return EXIT_FAILURE;
    }
    i32 errNum;
    auto zip = zip_openwitherror("packed.dat", 6, 'w', &errNum);
    if (errNum != 0) {
        printf("Error! Unable to open zip!\nReason: %s", zip_strerror(errNum));
        return EXIT_FAILURE;
    }

    { // Pack the assets json file
        errNum = zip_entry_open(zip, nce::ASSET_DETAILS_FILE_NAME);
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_fwrite(zip, nce::ASSET_DETAILS_FILE_NAME);
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_close(zip);
        CHECK_ZIP_ENTRY_ERR(errNum);
    }

    for (const auto& font : result.value().fonts) {
        errNum = zip_entry_open(zip, font.path.data());
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_fwrite(zip, font.path.data());
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_close(zip);
        CHECK_ZIP_ENTRY_ERR(errNum);
    }

    for (const auto& texture : result.value().textures) {
        errNum = zip_entry_open(zip, texture.path.data());
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_fwrite(zip, texture.path.data());
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_close(zip);
        CHECK_ZIP_ENTRY_ERR(errNum);
    }

    for (const auto& shader : result.value().shaders) {
        errNum = zip_entry_open(zip, shader.path.data());
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_fwrite(zip, shader.path.data());
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_close(zip);
        CHECK_ZIP_ENTRY_ERR(errNum);
    }

    for (const auto& sound : result.value().sounds) {
        errNum = zip_entry_open(zip, sound.path.data());
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_fwrite(zip, sound.path.data());
        CHECK_ZIP_ENTRY_ERR(errNum);
        errNum = zip_entry_close(zip);
        CHECK_ZIP_ENTRY_ERR(errNum);
    }

    zip_close(zip);

    const auto endTime = std::chrono::high_resolution_clock::now();
    const auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - beginTime);
    printf("Asset baking took %dms\n", i32(duration.count()));
    return EXIT_SUCCESS;
}
