#pragma once

#include <nce/HUDElement.hpp>

namespace nce {
class HUDText : public HUDElement {
public:
    HUDText(AppState* m_appState, std::string_view fontName, std::string_view textString = "", uint32_t charSize = 24);

    virtual void update(const sf::Time& dt, sf::RenderTarget& target) override;
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void setString(std::string_view string);
    void setAnchorPosition(const sf::Vector2f& pos);
    void setPosition(const sf::Vector2u& position);
    sf::Vector2u getPosition() { return sf::Vector2u { m_text->getPosition() }; }
    sf::FloatRect getOnScreenBounds() const;

    void setFillColour(const sf::Color& col);
    [[nodiscard]] auto getFillColour() -> sf::Color
    {
        assert(m_text);
        return m_text->getFillColor();
    }

private:
    std::optional<sf::Text> m_text;
    sf::Vector2f m_anchorPosition;
};
} // nce namespace