#pragma once

#include <nce/NCE.hpp>

namespace nce {
class GameObject;

class Component {
public:
    Component(std::string_view name, u32 tag, GameObject* parentObject) noexcept
        : m_name(name)
        , m_tag(tag)
        , m_parentObject(parentObject)
    {
        assert(parentObject);
    }
    virtual ~Component() = default;

    [[nodiscard]] virtual ErrResult init() { return true; }
    [[nodiscard]] virtual ErrResult onEnterState() { return true; }
    [[nodiscard]] virtual ErrResult onExitState() { return true; }

    virtual void update(sf::Time dt) { NCE_UNUSED(dt); };
    virtual void fixedUpdate(sf::Time dt, sf::Time fixedDT)
    {
        NCE_UNUSED(dt);
        NCE_UNUSED(fixedDT);
    };

    [[nodiscard]] u32 getTag() const noexcept { return m_tag; }
    [[nodiscard]] std::string_view getName() const noexcept { return m_name; }
    [[nodiscard]] auto getParentObject() const -> GameObject* { return m_parentObject; }

private:
    std::string m_name;
    u32 m_tag;
    GameObject* m_parentObject;
};
} // nce namespace