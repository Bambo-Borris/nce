#pragma once

namespace nce {

template <typename EventType>
struct Event {
    EventType type;
    std::any data;
};

template <typename EventType>
class EventQueue {
public:
    using Callback = std::function<void(const Event<EventType>&)>;

    EventQueue() = default;
    ~EventQueue() = default;

    // Add event listener and return a unique ID for the listener
    std::size_t addEventListener(Callback callback);

    // Remove a specific listener using its ID
    void removeEventListener(std::size_t listenerId);

    void processQueue();
    void triggerEvent(const Event<EventType>& event);

private:
    std::queue<Event<EventType>> m_eventQueue;
    std::unordered_map<std::size_t, Callback> m_eventListeners;
    std::size_t m_nextListenerId = 0; // Unique ID for listeners
};

template <typename EventType>
std::size_t EventQueue<EventType>::addEventListener(Callback callback)
{
    std::size_t listenerId = m_nextListenerId++;
    m_eventListeners[listenerId] = std::move(callback);
    return listenerId;
}

template <typename EventType>
void EventQueue<EventType>::removeEventListener(std::size_t listenerId)
{
    m_eventListeners.erase(listenerId);
}

template <typename EventType>
void EventQueue<EventType>::processQueue()
{
    while (!m_eventQueue.empty()) {
        const auto& event = m_eventQueue.front();
        for (const auto& [id, listener] : m_eventListeners) {
            listener(event);
        }
        m_eventQueue.pop();
    }
}

template <typename EventType>
void EventQueue<EventType>::triggerEvent(const Event<EventType>& event)
{
    m_eventQueue.push(event);
}
} // namespace nce
