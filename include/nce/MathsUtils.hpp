#pragma once

#include <nce/NCE.hpp>
#include <numbers>

namespace nce {

// Easings implementations grabbed from : https://easings.net/
template <typename T>
constexpr inline auto EaseOutElastic(const T& v0, const T& v1, float t) -> T
{
    constexpr auto c4 { (2.f * static_cast<f32>(std::numbers::pi)) / 3.f };
    const auto distance { v1 - v0 };
    const auto result { t == 0.f ? 0.f : t == 1.f ? 1.f : std::pow(2.f, -10.f * t) * std::sin((t * 10.f - 0.75f) * c4) + 1.f };
    return v0 + (distance * result);
}

template <typename T>
constexpr inline auto EaseInOutBack(const T& v0, const T& v1, float t) -> T
{
    constexpr auto c1 { 1.70158f };
    constexpr auto c2 { c1 * 1.525f };
    const auto distance { v1 - v0 };
    const auto result { t < 0.5f ? (std::pow(2.f * t, 2.f) * ((c2 + 1.f) * 2.f * t - c2)) / 2.f
                                 : (std::pow(2.f * t - 2.f, 2.f) * ((c2 + 1.f) * (t * 2.f - 2.f) + c2) + 2.f) / 2.f };
    return v0 + (distance * result);
}

template <typename T>
constexpr inline auto Lerp(const T& v0, const T& v1, float t) -> T
{
    return (1.f - t) * v0 + t * v1;
}

// Returns true if x is in range [low..high], else false
template <typename T>
    requires std::integral<T> || std::floating_point<T>
constexpr NCE_FORCE_INLINE auto InRange(T low, T high, T x) -> bool
{
    return ((x - high) * (x - low) <= 0);
}

[[nodiscard]] constexpr auto floatNearlyEqual(f32 a, f32 b, f32 epsilon = 128 * FLT_EPSILON, f32 abs_th = FLT_MIN) -> bool
// those defaults are arbitrary and could be removed
{
    assert(std::numeric_limits<f32>::epsilon() <= epsilon);
    assert(epsilon < 1.f);

    if (a == b)
        return true;

    auto diff = std::abs(a - b);
    auto norm = std::min((std::abs(a) + std::abs(b)), std::numeric_limits<f32>::max());
    // or even faster: std::min(std::abs(a + b), std::numeric_limits<float>::max());
    // keeping this commented out until I update figures below
    return diff < std::max(abs_th, epsilon * norm);
}

} // nce namespace
