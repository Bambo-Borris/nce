#pragma once

#include <nce/MathsUtils.hpp>
#include <nce/TimerController.hpp>

namespace nce {

enum class AnimationCurve { Lerp, EaseOutElastic, EaseInOutBack };
/// <summary>
/// Tweening animation class which can be used to store a tweening animation and retrieve
/// the value at a point in the animation curve controlled by parameter T. Also used a timer
/// through the nce::TimerController to control itself
/// </summary>
/// <typeparam name="T"></typeparam>
template <typename T>
class TweeningAnimation {
public:
    TweeningAnimation(AnimationCurve curve, std::string_view name, const T& start, const T& end, sf::Time duration, TimerController* tc);
    ~TweeningAnimation();

    [[nodiscard]] auto isComplete() const -> bool { return m_isComplete; }
    [[nodiscard]] auto isPlaying() const -> bool { return !m_timerController->isPaused(m_timerName); }
    [[nodiscard]] auto getTweenedValue() -> T;
    [[nodiscard]] auto getStart() const -> T { return m_start; }
    [[nodiscard]] auto getEnd() const -> T { return m_end; }

    void play();
    void stop();

    auto setStart(const T& newStart) { m_start = newStart; }
    auto setEnd(const T& newEnd) { m_end = newEnd; }

private:
    std::string m_name;
    std::string m_timerName;
    T m_start;
    T m_end;
    sf::Time m_duration;
    AnimationCurve m_curve;
    TimerController* m_timerController;
    bool m_isComplete { false };
    bool m_isFloatingPoint { false };
};

template <typename T>
inline TweeningAnimation<T>::TweeningAnimation(
    AnimationCurve curve, std::string_view name, const T& start, const T& end, sf::Time duration, TimerController* tc)
    : m_name(name)
    , m_timerName(fmt::format("{}_TIMER", name))
    , m_start(start)
    , m_end(end)
    , m_duration(duration)
    , m_curve(curve)
    , m_timerController(tc)
{
    static_assert((std::is_floating_point_v<T> || std::is_same_v<sf::Vector2f, T>), "Invalid tweening type");
    m_isFloatingPoint = std::is_floating_point_v<T>;
    m_timerController->addTimer(m_timerName, true);
}

template <typename T>
inline TweeningAnimation<T>::~TweeningAnimation()
{
    m_timerController->removeTimer(m_timerName);
}

template <typename T>
inline void TweeningAnimation<T>::play()
{
    m_timerController->playTimer(m_timerName);
}

template <typename T>
inline void TweeningAnimation<T>::stop()
{
    m_timerController->resetTimer(m_timerName, true);
    m_isComplete = false;
}

template <typename T>
inline T TweeningAnimation<T>::getTweenedValue()
{
    const auto t = std::clamp(m_timerController->getElapsed(m_timerName) / m_duration, 0.f, 1.f);

    if (!m_isComplete) {
        if (t == 1.f) {
            m_isComplete = true;
            m_timerController->pauseTimer(m_timerName);
        }
    }

    T out {};

    switch (m_curve) {
    case nce::AnimationCurve::Lerp:
        out = nce::Lerp(m_start, m_end, t);
        break;
    case nce::AnimationCurve::EaseOutElastic:
        out = nce::EaseOutElastic(m_start, m_end, t);
        break;
    case nce::AnimationCurve::EaseInOutBack:
        out = nce::EaseInOutBack(m_start, m_end, t);
        break;
    default:
        assert(false);
        break;
    }
    return out;
}

using TAFloat = TweeningAnimation<float>;
using TAVec2f = TweeningAnimation<sf::Vector2f>;
}