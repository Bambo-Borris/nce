#pragma once

namespace nce {
class GameObject;
enum class ColliderType { Circle, AABB, OBB, Poly, MAX };
constexpr auto EPSILON { 0.0001f };

struct Manifold {
    sf::Vector2f normal;
    float penetration { 0.f };
};

struct CircleMetadata {
    float radius;
};

struct AABBMetadata {
    sf::FloatRect bounds;
};

struct PolyMetadata {
    std::vector<sf::Vector2f> points;
};

struct OBBMetadata {
    sf::Vector2f halfBounds;
};

struct ColliderInfo {
    ColliderType type = ColliderType::Circle;
    sf::Transformable* transformable { nullptr };
    GameObject* go { nullptr };
    std::variant<CircleMetadata, AABBMetadata, OBBMetadata, PolyMetadata> metadata;
    i32 mask { 1 };
};

// Equality operator for ColliderInfo
[[nodiscard]] bool operator==(const ColliderInfo& rhs, const ColliderInfo& lhs);

[[nodiscard]] inline bool operator!=(const ColliderInfo& rhs, const ColliderInfo& lhs) { return !(rhs == lhs); }

[[nodiscard]] constexpr auto ComputeOBBVertices(const sf::Vector2f& size, sf::Vector2f origin = {}) -> std::array<sf::Vector2f, 4>
{
    if (origin == sf::Vector2f {}) {
        return { sf::Vector2f { 0.f, 0.f }, sf::Vector2f { size.x, 0.f }, sf::Vector2f { size.x, size.y }, sf::Vector2f { 0.f, size.y } };
    }

    return {
        sf::Vector2f { origin.x - size.x / 2.f, origin.y - size.y / 2.f },
        sf::Vector2f { origin.x + size.x / 2.f, origin.y - size.y / 2.f },
        sf::Vector2f { origin.x + size.x / 2.f, origin.y + size.y / 2.f },
        sf::Vector2f { origin.x - size.x / 2.f, origin.y + size.y / 2.f },
    };
}

[[nodiscard]] constexpr auto ComputeAABBWorldVertices(const sf::FloatRect& rect) -> std::array<sf::Vector2f, 4>
{
    return { sf::Vector2f(rect.getPosition()),
             sf::Vector2f(rect.getPosition() + sf::Vector2f { rect.width, 0.f }),
             sf::Vector2f(rect.getPosition() + sf::Vector2f { rect.getSize() }),
             sf::Vector2f(rect.getPosition() + sf::Vector2f { 0.f, rect.height }) };
}

[[nodiscard]] constexpr auto ComputeOBBVerticesTransformed(const sf::Vector2f& size, const sf::Transform& transform, sf::Vector2f origin)
    -> std::array<sf::Vector2f, 4>
{
    auto out { ComputeOBBVertices(size, origin) };
    for (auto& v : out)
        v = transform * v;

    return out;
}

[[nodiscard]] inline auto BoundingBoxFromCollider(const ColliderInfo& colliderInfo) -> sf::FloatRect
{
    const auto& transformMatrix { colliderInfo.transformable->getTransform() };

    sf::Vector2f size;
    sf::FloatRect outRect;

    switch (colliderInfo.type) {
        using namespace nce;
    case ColliderType::Circle: {
        const auto& metadata = std::get<CircleMetadata>(colliderInfo.metadata);
        size = { metadata.radius * 2.f, metadata.radius * 2.f };
        size = size.cwiseMul(colliderInfo.transformable->getScale());
        const auto position = colliderInfo.transformable->getPosition() - (size / 2.f);
        outRect = sf::FloatRect { position, size };
        //        outRect = transformCopy.transformRect({ { -metadata.radius, -metadata.radius }, size });
    } break;
    case ColliderType::AABB: {
        const auto& metadata = std::get<AABBMetadata>(colliderInfo.metadata);
        size = (metadata.bounds.getPosition(), metadata.bounds.getPosition() + metadata.bounds.getSize());
        outRect = transformMatrix.transformRect({ { 0.f, 0.f }, size });
    } break;
    case ColliderType::OBB: {
        // We'll take the untransformed bounds, find the top left position
        // and the size then use the transform to adjust the bounds
        const auto& metadata = std::get<OBBMetadata>(colliderInfo.metadata);
        const auto verts { ComputeOBBVertices(metadata.halfBounds * 2.f, colliderInfo.transformable->getOrigin()) };
        outRect = transformMatrix.transformRect({ verts[0], { metadata.halfBounds * 2.f } });

    } break;
    case ColliderType::Poly: {
        const auto& metadata { std::get<PolyMetadata>(colliderInfo.metadata) };
        auto minX { metadata.points[0].x };
        auto maxX { metadata.points[0].x };
        auto minY { metadata.points[0].y };
        auto maxY { metadata.points[0].y };

        for (size_t i { 0 }; i < metadata.points.size(); ++i) {
            const auto& p { metadata.points[i] };
            minX = std::min(minX, p.x);
            maxX = std::max(maxX, p.x);

            minY = std::min(minY, p.y);
            maxY = std::max(maxY, p.y);
        }

        size = sf::Vector2f { maxX, maxY } - sf::Vector2f { minX, minY };
        outRect = transformMatrix.transformRect({ { minX, minY }, size });

    } break;
    default:
        assert(false);
        break;
    }

    return outRect;
}

// We should rethink this and maybe opt for a pool of colliders if performance becomes a crucial factor
// in here.
[[nodiscard]] auto MakeCircleCollider(float radius, sf::Transformable* transformable, GameObject* go, i32 mask = 1) -> ColliderInfo;
[[nodiscard]] auto MakeAABBCollider(sf::Vector2f halfBounds, sf::Transformable* transformable, GameObject* go, i32 mask = 1) -> ColliderInfo;
[[nodiscard]] auto MakeOBBCollider(sf::Vector2f halfBounds, sf::Transformable* transformable, GameObject* go, i32 mask = 1) -> ColliderInfo;
[[nodiscard]] auto MakePolyCollider(const std::vector<sf::Vector2f>& points, sf::Transformable* transformable, GameObject* go, i32 mask = 1) -> ColliderInfo;

// Some of the implementations below originate from:  https://dyn4j.org/2010/01/sat/ and https://github.com/RandyGaul/ImpulseEngine/

[[nodiscard]] std::optional<Manifold> CircleVsCircle(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> CircleVsAABB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> CircleVsOBB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> CircleVsPoly(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;

[[nodiscard]] std::optional<Manifold> AABBVsCircle(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> AABBVsAABB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> AABBVsOBB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> AABBVsPoly(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;

[[nodiscard]] std::optional<Manifold> OBBVsCircle(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> OBBVsAABB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> OBBVsOBB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> OBBVsPoly(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;

[[nodiscard]] std::optional<Manifold> PolyVsCircle(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> PolyVsAABB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> PolyVsOBB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;
[[nodiscard]] std::optional<Manifold> PolyVsPoly(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept;

/* Raycast related functions */

[[nodiscard]] inline auto LineSegmentIntersectionTest(const sf::Vector2f& p1, const sf::Vector2f& p2, const sf::Vector2f& p3, const sf::Vector2f& p4)
    -> std::optional<sf::Vector2f>
{
    auto denominator = ((p4.y - p3.y) * (p2.x - p1.x)) - ((p4.x - p3.x) * (p2.y - p1.y));

    // Check for parallel lines
    if (denominator == 0) {
        return {};
    }

    const auto ua = (((p4.x - p3.x) * (p1.y - p3.y)) - ((p4.y - p3.y) * (p1.x - p3.x))) / denominator;
    const auto ub = (((p2.x - p1.x) * (p1.y - p3.y)) - ((p2.y - p1.y) * (p1.x - p3.x))) / denominator;

    if (ua >= 0.f && ua <= 1.f && ub >= 0.f && ub <= 1.f) {
        sf::Vector2f intersectionPoint;
        intersectionPoint.x = p1.x + ua * (p2.x - p1.x);
        intersectionPoint.y = p1.y + ua * (p2.y - p1.y);
        return intersectionPoint;
    }

    return {};
}

[[nodiscard]] inline auto CircleLineSegmentIntersection(const sf::Vector2f& rayOrigin,
                                                        const sf::Vector2f& rayDirection,
                                                        float rayLength,
                                                        const sf::Vector2f& circleCenter,
                                                        float circleRadius) -> std::optional<sf::Vector2f>
{
    sf::Vector2f intersection;
    sf::Vector2f oc = rayOrigin - circleCenter;
    auto a = rayDirection.dot(rayDirection);
    auto b = 2.0f * oc.dot(rayDirection);
    auto c = oc.dot(oc) - circleRadius * circleRadius;
    auto discriminant = b * b - 4.f * a * c;

    if (discriminant < 0) {
        return {};
    }
    auto t1 = (-b - std::sqrt(discriminant)) / (2.0f * a);
    auto t2 = (-b + std::sqrt(discriminant)) / (2.0f * a);

    // Check if the intersection points are within the specified ray length
    if (t1 >= 0.0f && t1 <= rayLength) {
        intersection = rayOrigin + t1 * rayDirection;
    } else if (t2 >= 0.0f && t2 <= rayLength) {
        intersection = rayOrigin + t2 * rayDirection;
    }

    if ((intersection - rayOrigin).length() >= rayLength)
        return {};

    return intersection;
}

template <typename T>
    requires std::is_floating_point_v<T> || std::is_integral_v<T>
[[nodiscard]] inline auto IsConvexShape(const std::vector<sf::Vector2<T>>& polygon) -> bool
{
    if (polygon.size() < 3) {
        // A polygon must have at least 3 vertices
        return false;
    }

    int posCount = 0; // Count of positive (clockwise) turns
    int negCount = 0; // Count of negative (counter-clockwise) turns

    for (size_t i = 0; i < polygon.size(); ++i) {
        sf::Vector2<T> v1 = polygon[i];
        sf::Vector2<T> v2 = polygon[(i + 1) % polygon.size()];
        sf::Vector2<T> v3 = polygon[(i + 2) % polygon.size()];

        T crossProduct = (v2.x - v1.x) * (v3.y - v2.y) - (v2.y - v1.y) * (v3.x - v2.x);

        if (crossProduct > 0) {
            posCount++;
        } else if (crossProduct < 0) {
            negCount++;
        }

        // If both types of turns occur, it's a concave polygon
        if (posCount > 0 && negCount > 0) {
            return false; // Concave polygon
        }
    }

    return true; // If it reaches here, the polygon is convex
}
template <typename T>
    requires std::is_floating_point_v<T> || std::is_integral_v<T>
[[nodiscard]] inline auto IsPointInConvexPolygon(const sf::Vector2<T>& point, const std::vector<sf::Vector2<T>>& polygonVerts) -> bool
{
    int32_t winding = 0;
    for (size_t i = 0; i < polygonVerts.size(); ++i) {
        sf::Vector2<T> v1 = polygonVerts[i];
        sf::Vector2<T> v2 = polygonVerts[(i + 1) % polygonVerts.size()];

        if (v1.y <= point.y) {
            if (v2.y > point.y && ((v2 - v1).x * (point.y - v1.y) - (point.x - v1.x) * (v2.y - v1.y)) > 0) {
                winding++;
            }
        } else if (v2.y <= point.y) {
            if ((v2 - v1).x * (point.y - v1.y) - (point.x - v1.x) * (v2.y - v1.y) < 0) {
                winding--;
            }
        }
    }

    return winding != 0;
}

template <typename T>
    requires std::is_floating_point_v<T> || std::is_integral_v<T>
[[nodiscard]] inline auto IsPointInConcavePolygon(sf::Vector2<T> point, const std::vector<sf::Vector2<T>>& polygon) -> bool
{
    auto inside = false;
    size_t j = polygon.size() - 1;

    for (size_t i = 0; i < polygon.size(); i++) {
        if ((polygon[i].y > point.y) != (polygon[j].y > point.y)
            && (point.x < (polygon[j].x - polygon[i].x) * (point.y - polygon[i].y) / (polygon[j].y - polygon[i].y) + polygon[i].x)) {
            inside = !inside;
        }
        j = i;
    }

    return inside;
}

template <typename T>
    requires std::is_floating_point_v<T>
[[nodiscard]] constexpr auto GetIntersectionPointForTwoLines(sf::Vector2<T> p1, sf::Vector2<T> q1, sf::Vector2<T> p2, sf::Vector2<T> q2)
    -> std::optional<sf::Vector2<T>>
{
    // Utility function to find orientation of ordered triplet (p, q, r).
    constexpr auto orientation = [](sf::Vector2<T> p, sf::Vector2<T> q, sf::Vector2<T> r) -> int {
        auto val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);

        if (val == 0.0)
            return 0; // collinear
        return (val > 0.0) ? 1 : 2; // clock or counterclockwise
    };

    // Check if point q lies on line segment 'pr'
    constexpr auto onSegment = [](sf::Vector2<T> p, sf::Vector2<T> q, sf::Vector2<T> r) -> bool {
        return q.x <= std::max(p.x, r.x) && q.x >= std::min(p.x, r.x) && q.y <= std::max(p.y, r.y) && q.y >= std::min(p.y, r.y);
    };

    // Find the four orientations needed for general and special cases
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);

    if (o1 != o2 && o3 != o4) {
        // General case for intersection
        auto a1 = q1.y - p1.y;
        auto b1 = p1.x - q1.x;
        auto c1 = a1 * p1.x + b1 * p1.y;

        auto a2 = q2.y - p2.y;
        auto b2 = p2.x - q2.x;
        auto c2 = a2 * p2.x + b2 * p2.y;

        auto determinant = a1 * b2 - a2 * b1;

        if (determinant == 0) {
            // Lines are parallel, no intersection
            return std::nullopt;
        } else {
            auto x = (b2 * c1 - b1 * c2) / determinant;
            auto y = (a1 * c2 - a2 * c1) / determinant;
            return sf::Vector2<T>(x, y);
        }
    }

    // Special Cases
    if (o1 == 0 && onSegment(p1, p2, q1))
        return p2;
    if (o2 == 0 && onSegment(p1, q2, q1))
        return q2;
    if (o3 == 0 && onSegment(p2, p1, q2))
        return p1;
    if (o4 == 0 && onSegment(p2, q1, q2))
        return q1;

    return std::nullopt; // No intersection
}

template <typename T>
    requires std::is_floating_point_v<T>
[[nodiscard]] constexpr auto ClosestPointOnLine(sf::Vector2<T> lineStart, sf::Vector2<T> lineEnd, sf::Vector2<T> point) -> sf::Vector2<T>
{
    sf::Vector2<T> lineVector = lineEnd - lineStart;
    sf::Vector2<T> pointVector = point - lineStart;

    // Calculate the dot product
    auto dotProduct = pointVector.dot(lineVector);

    // Calculate the length squared of the line vector
    auto lineLengthSquared = lineVector.x * lineVector.x + lineVector.y * lineVector.y;

    // Calculate the parameter of the closest point on the line
    auto t = dotProduct / lineLengthSquared;

    // Clamp 't' to the range [0, 1] to ensure the closest point is on the line segment
    t = std::max(T(0.0), std::min(T(1.0), t));

    // Calculate the closest point
    sf::Vector2<T> closestPoint = lineStart + t * lineVector;

    return closestPoint;
}

} // nce namespace
