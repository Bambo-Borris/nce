#pragma once

#include <nce/AssetJSON.hpp>

namespace nce {
class App;

class AssetHolder {
public:
    static AssetHolder& get()
    {
        static AssetHolder instance {};
        return instance;
    }
    ~AssetHolder();

    [[nodiscard]] sf::Font* getFont(std::string_view id);
    [[nodiscard]] sf::Texture* getTexture(std::string_view id);
    [[nodiscard]] sf::SoundBuffer* getSoundBuffer(std::string_view id);
    [[nodiscard]] sf::Music* getMusic(const std::filesystem::path& path);
    [[nodiscard]] sf::Image* getImage(const std::filesystem::path& path);
    [[nodiscard]] sf::Shader* getShader(std::string_view id, sf::Shader::Type type);

    // Note: This should ** ONLY ** be called on the main thread,
    // GL requires only 1 thread handle the context, this method
    // takes a queue of CPU loaded images and converts them to
    // GL textures. Unsafe to be done on any thread other than main.
    void processTextureQueue();

    // Note: This should ** ONLY ** be called on the main thread,
    // GL requires only 1 thread handle the context, this method
    // takes a queue of CPU loaded shader file contents, and compiles
    // them on the main thread. Unsafe to be done on any thread other
    // than main.
    void processShaderQueue();

    void unloadAssets();

    void setAppInstance(App* app) { m_app = app; }

private:
    AssetHolder();

    struct ShaderQueueEntry {
        std::string shaderSource;
        sf::Shader::Type type;
    };

    struct FontAsset {
        sf::Font font;
        std::vector<std::byte> fileContents;
    };

    std::unordered_map<std::string, FontAsset> m_fontMap;
    std::unordered_map<std::string, sf::Texture> m_textureMap;
    std::unordered_map<std::string, sf::SoundBuffer> m_soundBufferMap;
    std::unordered_map<std::string, sf::Music> m_musicMap;
    std::unordered_map<std::string, sf::Image> m_imageMap;
    std::unordered_map<std::string, sf::Shader> m_shaderMap;

    std::queue<std::pair<std::string, sf::Image>> m_imageQueue;
    std::queue<std::pair<std::string, ShaderQueueEntry>> m_shaderQueue;

    std::mutex m_imageQueueMutex;
    std::mutex m_textureMapMutex;

    std::mutex m_shaderSourceMutex;
    std::mutex m_shaderMapMutex;

    std::string m_packedAssetPath;
    std::optional<AssetsInfo> m_assetInfo;
    App* m_app { nullptr };
};
} // nce namespace

#define NCE_ASSETS() nce::AssetHolder::get()
