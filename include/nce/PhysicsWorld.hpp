#pragma once

#include <SFML/System/Time.hpp>
#include <vector>

namespace nce {
class PhysicsObject;

/// <summary>
/// The physics world is the class responsible
/// for ensuring all PhysicsObjects are ticked
/// on the fixed tick, as well as keeping track
/// of which objects are part of the active
/// simulation.
/// </summary>
class PhysicsWorld {
public:
    /// <summary>
    /// Advance the physics simulation by 1 frame, where DT
    /// represents the fixed step time value for the update.
    /// </summary>
    /// <param name="dt">Fixed target frame time value</param>
    void step(const sf::Time& dt);

    /// <summary>
    /// Add an existing PhysicsObject to the scene,
    /// note that PhysicsWorld takes no ownership for
    /// the lifetime of this object.
    /// </summary>
    /// <param name="obj">Pointer to the PhysicsObject to add</param>
    void addObject(PhysicsObject* obj);

    /// <summary>
    /// Remove a PhysicsObject from being tracked in the
    /// PhysicsWorld simulation.
    /// </summary>
    /// <param name="obj">Pointer to the PhysicsObject to remove</param>
    void removeObject(PhysicsObject* obj);

    /// <summary>
    /// Set the world scale of the simulation. Or in other words,
    /// how many pixels are there per metre.
    /// </summary>
    /// <param name="worldScale"></param>
    void setWorldScale(float worldScale) { m_worldScale = worldScale; }

    /// <summary>
    /// Get the currently applied world scale
    /// </summary>
    /// <returns>worldScale, how many pixels per metre</returns>
    [[nodiscard]] auto getWorldScale() const { return m_worldScale; }

private:
    std::vector<PhysicsObject*> m_objects;
    float m_worldScale { 1.f }; // How many pixels in 1 Metre
};
} // nce namespace
