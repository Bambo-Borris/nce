#pragma once

namespace nce {
class Renderable;

class Renderer {
public:
    void update();
    void render();

    void setClearColour(const sf::Color& col) { m_clearColour = col; }
    void setPostProcessingShader(sf::Shader* shader) { m_postProcessingShader = shader; }

private:
    sf::Color m_clearColour { sf::Color::Black };
    sf::Shader* m_postProcessingShader { nullptr };
    sf::RectangleShape m_loadingSplash {};
    sf::Texture m_loadingScreenTexture {};
    std::vector<Renderable*> m_renderQueueObjects;
};
} // nce namespace