#pragma once

#include "GameObject.hpp"

#include <nce/ButtonActionChecker.hpp>
#include <nce/CollisionSolver.hpp>
#include <nce/Components/Renderable.hpp>
#include <nce/NCE.hpp>
#include <nce/PhysicsWorld.hpp>
#include <nce/TimerController.hpp>
#include <utility>

namespace nce {

class GameObject;
class HUDElement;
class App;

class AppState {
public:
    AppState(std::string_view identifier);
    virtual ~AppState();

    /// <summary>
    /// Called after construction, this method should be used to initialise anything where
    /// exceptions are not the preferred route of error handling.
    /// </summary>
    /// <returns></returns>
    [[nodiscard]] virtual ErrResult init();

    /// <summary>
    /// Invoked once before the first update. Invokes onEnterState for any GameObject
    /// preallocated at init time. GameObjects spawned after this point will have onEnterState
    /// invoked prior to their first update() or fixedUpdate call.
    /// </summary>
    /// <returns></returns>
    [[nodiscard]] virtual ErrResult onStateBegin();

    /// <summary>
    /// Invoked once before the the state is terminated. Invokes onExitState for any GameObject
    /// still active in the state. GameObjects terminated earlier than this have onExitState called
    /// before destruction.
    /// </summary>
    /// <returns></returns>
    [[nodiscard]] virtual ErrResult onStateEnd();

    /// <summary>
    /// Invoked once-per-draw to perform update related tasks.
    /// </summary>
    /// <param name="dt">Delta Time</param>
    virtual void update(sf::Time dt);

    /// <summary>
    /// Called per fixed step update, to perform update related tasks.
    /// </summary>
    /// <param name="dt">Delta Time</param>
    /// <param name="fixedDT">Fixed Step Time</param>
    virtual void fixedUpdate(sf::Time dt, sf::Time fixedDT);

    /// <summary>
    /// Query whether the AppState has completed its role and is
    /// ready to yield to the next AppState
    /// </summary>
    /// <returns>True if the AppState is ready to yield, false otherwise.</returns>
    [[nodiscard]] auto isStateComplete() const -> bool { return m_isStateComplete; }

    /// <summary>
    /// Get the string ID of the current AppState.
    /// </summary>
    /// <returns>AppState ID</returns>
    [[nodiscard]] auto getStateID() const -> std::string_view { return m_stateID; }

    /// <summary>
    /// Get the ID of the state to transition to when this one
    /// is ready to yield.
    /// </summary>
    /// <returns>String ID of the next state</returns>
    [[nodiscard]] auto getNextStateID() const { return m_nextStateID; }

    /// <summary>
    /// Add a new GameObject to be tracked by the AppState.
    /// (This operation hands ownership to the AppState)
    /// </summary>
    /// <param name="object">Pointer to the GameObject to insert</param>
    /// <returns>True if the operation was successful, false otherwise</returns>
    [[nodiscard]] auto addGameObject(GameObject* object) -> bool;

    /// <summary>
    /// Adds all of the GameObject instances, present in the std::vector objects,
    /// to be tracked by AppState. (This operation hands ownership to the AppState)
    /// </summary>
    /// <param name="objects">Vector of game objects to add</param>
    /// <returns>True if the object was added, false if there was a problem</returns>
    [[nodiscard]] auto addGameObjects(std::vector<GameObject*> objects) -> bool;

    /// <summary>
    /// Remove a GameObject from the app state.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="object"></param>
    template <typename T>
        requires std::is_same_v<GameObject, T>
    void removeGameObject(const T* object);

    /// <summary>
    /// Remove a GameObject from the app state.
    /// </summary>
    /// <param name="identifier">Identifier of the game object being queried</param>
    /// <returns>Pointer to the object requested, or nullptr if not found</returns>
    [[nodiscard]] GameObject* getObjectByIdentifier(std::string_view identifier);

    template <typename T>
        requires std::is_base_of_v<Component, T>
    [[nodiscard]] std::vector<GameObject*> getObjectsByComponentType() const;

    // /// <summary>
    // /// Add HUD element to the AppState.
    // /// (This operation hands ownership to the AppState)
    // /// </summary>
    // /// <typeparam name="T">HUDElement derived</typeparam>
    // /// <param name="element">pointer to the element to add</param>
    // /// <returns></returns>
    // template <typename T>
    //     requires std::is_base_of_v<HUDElement, T>
    // [[nodiscard]] auto addHUDElement(T* element) -> bool;
    //
    // /// <summary>
    // /// Remove HUD element to the AppState.
    // /// </summary>
    // /// <typeparam name="T">Type of HUDElement to remove</typeparam>
    // /// <param name="element">Pointer to the instance to remove</param>
    // template <typename T>
    //     requires std::is_base_of_v<HUDElement, T>
    // void removeHUDElement(T* element);
    //
    // template <typename T>
    //     requires std::is_base_of_v<HUDElement, T>
    // [[nodiscard]] auto getHUDElementByType() -> T*;
    //
    // template <typename T>
    //     requires std::is_base_of_v<HUDElement, T>
    // [[nodiscard]] auto getAllHUDElementsOfType() const -> std::vector<T*>;

    [[nodiscard]] auto getButtonActionChecker() -> ButtonActionChecker& { return *m_actionChecker; }
    [[nodiscard]] auto getTimerController() -> TimerController& { return m_timerController; }
    [[nodiscard]] auto getPhysicsWorld() -> PhysicsWorld& { return m_physicsWorld; }

    /// <summary>
    /// Returns a pointer to the CollisionSolver for this AppState, note a solver may not
    /// always be present on the AppState, in that case this pointer will be null. Always
    /// check before using.
    /// </summary>
    /// <returns>Pointer to the currently active solver</returns>
    [[nodiscard]] auto getSolver() -> nce::CollisionSolver* { return m_solver.get(); }
    [[nodiscard]] auto getNextStateData() -> std::any { return m_nextStateData; }

    /// Pausing a state prevents GameObjects and timers being updateed, and halts all
    /// physics/game object fixed step updates. This does NOT stop objects being rendered
    /// however. This functionality is intended to make it eaiser to implement pause menues
    /// and level complete mechanisms.
    void pauseState() { m_isPaused = true; }
    void resumeState() { m_isPaused = false; }
    [[nodiscard]] auto isStatePaused() const -> bool { return m_isPaused; }

    /// <summary>
    /// Construct a list of all GameObjects with a nce::Renderable component
    /// </summary>
    /// <returns>List of pointers to nce::Renderable instances to be used by the renderer</returns>
    [[nodiscard]] auto assembleDrawableObjectList() -> std::vector<Renderable*>;

protected:
    void setStateComplete(std::any nextStateData = {})
    {
        m_isStateComplete = true;
        m_nextStateData = std::move(nextStateData);
    }
    [[nodiscard]] auto getPlayAreaSize() const -> const sf::Vector2f& { return m_playAreSize; }
    void setNextStateID(std::string_view nextID) { m_nextStateID = nextID; }
    void addSolver(std::unique_ptr<nce::CollisionSolver> solver) { m_solver = std::move(solver); }

private:
    const std::string m_stateID;
    std::string m_nextStateID;

    std::unique_ptr<ButtonActionChecker> m_actionChecker;
    std::unique_ptr<nce::CollisionSolver> m_solver;

    std::vector<std::unique_ptr<GameObject>> m_gameObjects;
    // std::vector<std::unique_ptr<HUDElement>> m_hudElements;
    // std::vector<HUDElement*> m_renderQueueHUD;

    TimerController m_timerController;
    PhysicsWorld m_physicsWorld;
    std::any m_nextStateData;

    sf::Vector2f m_playAreSize;
    bool m_isStateComplete { false };
    bool m_isPaused { false };
};

// ------------------------------------------------------------
template <typename T>
    requires std::is_same_v<GameObject, T>
inline void AppState::removeGameObject(const T* object)
{
    std::erase_if(m_gameObjects, [&object](const std::unique_ptr<GameObject>& go) -> bool { return go.get() == object; });
}

template <typename T>
    requires std::is_base_of_v<Component, T>
std::vector<GameObject*> AppState::getObjectsByComponentType() const
{
    std::vector<GameObject*> objectsList;
    auto range = m_gameObjects | std::views::filter([](const std::unique_ptr<GameObject>& goPtr) -> bool { return goPtr->getComponentByType<T>() != nullptr; });
    for (const std::unique_ptr<GameObject>& goPtr : range) {
        objectsList.emplace_back(goPtr.get());
    }
    return objectsList;
}

// // ------------------------------------------------------------
// template <typename T>
//     requires std::is_base_of_v<HUDElement, T>
// inline auto AppState::addHUDElement(T* element) -> bool
// {
//     assert(element);
//     if (!element)
//         return false;
//
//     m_hudElements.emplace_back(element);
//     return true;
// }
//
// // ------------------------------------------------------------
// template <typename T>
//     requires std::is_base_of_v<HUDElement, T>
// inline void AppState::removeHUDElement(T* element)
// {
//     assert(element);
//     auto baseElement { std::static_pointer_cast<HUDElement>(element) };
//     auto removeResult = std::remove(m_hudElements.begin(), m_hudElements.end(), baseElement);
//     m_hudElements.erase(removeResult, m_hudElements.end());
// }
//
// // ------------------------------------------------------------
// template <typename T>
//     requires std::is_base_of_v<HUDElement, T>
// inline auto AppState::getHUDElementByType() -> T*
// {
//     auto result { std::find_if(m_hudElements.begin(), m_hudElements.end(), [](const std::unique_ptr<HUDElement>& element) -> bool {
//         return dynamic_cast<T*>(element.get()) != nullptr;
//     }) };
//
//     if (result == m_hudElements.end())
//         return nullptr;
//
//     return dynamic_cast<T*>((*result).get());
// }
//
// template <typename T>
//     requires std::is_base_of_v<HUDElement, T>
// inline auto AppState::getAllHUDElementsOfType() const -> std::vector<T*>
// {
//     std::vector<T*> output;
//     for (auto& obj : m_hudElements) {
//         auto castedInstance { dynamic_cast<T*>(obj.get()) };
//         if (!castedInstance) {
//             continue;
//         }
//         output.emplace_back(castedInstance);
//     }
//     return output;
// }
} // nce namespace
