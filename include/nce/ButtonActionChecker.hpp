#pragma once

#include <nce/GamePad.hpp>
#include <unordered_map>

namespace nce {
class ButtonActionChecker {
public:
    enum class ActionType { Held, Pressed, Released };
    enum class ActionTrigger { Keyboard, Mouse, GamePad };
    enum class ActionAxisRange { Positive, Negative };

    ButtonActionChecker();
    ~ButtonActionChecker();

    void addKeyboardAction(std::string_view action, ActionType type, sf::Keyboard::Scan key);
    void addMouseAction(std::string_view action, ActionType type, sf::Mouse::Button button);
    void addGamePadAction(std::string_view action, ActionType type, nce::GamePad::Buttons button);
    // [[deprecated]] void addGamePadAxisAsAction(std::string_view action, ActionAxisRange range, nce::GamePad::Axis axis);

    [[nodiscard]] auto checkAction(std::string_view action) const -> std::optional<ActionTrigger>;

private:
    using ActionWrapperKeyboard = std::pair<ActionType, sf::Keyboard::Scan>;
    using ActionWrapperMouse = std::pair<ActionType, sf::Mouse::Button>;
    using ActionWrapperGamePad = std::pair<ActionType, nce::GamePad::Buttons>;
    using ActionWrapperGamePadAxis = std::pair<ActionAxisRange, nce::GamePad::Axis>;

    std::unordered_map<std::string, std::vector<ActionWrapperKeyboard>> m_actionsKeyboard;
    std::unordered_map<std::string, std::vector<ActionWrapperMouse>> m_actionsMouse;
    std::unordered_map<std::string, std::vector<ActionWrapperGamePad>> m_actionsGamePad;
    // std::unordered_map<std::string, std::vector<ActionWrapperGamePadAxis>> m_actionsGamePadAxis;
};
} // nce namespace
