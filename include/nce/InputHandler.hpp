#pragma once

#include <nce/GamePad.hpp>

//
// Example use of this class:
// if (InputHandler::keyPressed(sf::Keyboard::Escape))
//    DoStuff();
//
namespace nce {
enum class PadType { Xbox_Pad, PlayStation_Pad };
enum class JoystickAxis { // Named as though on a PS4 since we only support gamepads
    Left_Joystick_Horizontal,
    Left_Joystick_Vertical,
    Right_Joystick_Horizontal,
    Right_Joystick_Vertical,
    L2,
    R2,
    Directional_Pad_Horizontal,
    Directional_Pad_Vertical,
    Joystick_Axis_Max
};

enum class JoystickButton { // Named as though on PS4 pad since we only support gamepads
    Square,
    Cross,
    Circle,
    Triangle,
    L1,
    R1,
    L2,
    R2,
    Share,
    Options,
    Left_Stick_Click,
    Right_Stick_Click,
    Joystick_Button_Max
};

class InputHandler {
public:
    static void Update();
    static void HandleEvent(const sf::Event& event);
    static void SetMousePosition(const sf::Vector2i& pos, sf::RenderWindow* relativeTo = nullptr);
    static void CleanUp();

    [[nodiscard]] static auto KeyPressed(sf::Keyboard::Scan key) -> bool;
    [[nodiscard]] static auto KeyHeld(sf::Keyboard::Scan key) -> bool;
    [[nodiscard]] static auto KeyReleased(sf::Keyboard::Scan key) -> bool;

    [[nodiscard]] static auto MousePressed(sf::Mouse::Button button) -> bool;
    [[nodiscard]] static auto MouseReleased(sf::Mouse::Button button) -> bool;
    [[nodiscard]] static auto MouseHeld(sf::Mouse::Button button) -> bool;

    [[nodiscard]] static auto GetMouseWorldPosition() -> sf::Vector2f;
    [[nodiscard]] static auto GetMouseScreenPosition() -> sf::Vector2i;
    [[nodiscard]] static auto GetMouseScreenDelta() -> sf::Vector2i;

    [[nodiscard]] static auto IsGamePadConnected() -> bool { return m_gamePad.hasValidConnectedDevice(); }
    [[nodiscard]] static auto GetGamePad() -> const GamePad& { return m_gamePad; }

    [[nodiscard]] static auto AnyKeyPressed() -> bool;

private:
    static void HandleKeyboardAndMouse(const sf::Event& event);

    // void handleXbox(const sf::Event& event);

    static PadType m_padType;
    static sf::Vector2i m_mousePositionScreen;
    static sf::Vector2i m_mouseScreenDelta;

    static std::set<sf::Keyboard::Scan> m_keyboardPressed;
    static std::set<sf::Keyboard::Scan> m_keyboardReleased;
    static std::set<sf::Keyboard::Scan> m_keyboardHeld;

    static std::set<sf::Mouse::Button> m_mouseButtonPressed;
    static std::set<sf::Mouse::Button> m_mouseButtonReleased;
    static std::set<sf::Mouse::Button> m_mouseButtonHeld;

    static GamePad m_gamePad;
    static int32_t m_fileDescriptor; // Used when on Linux
};
} // nce namespace
