#pragma once

#include <ranges>
#include <string>

namespace nce {

[[nodiscard]] NCE_FORCE_INLINE auto StringContainsSubstring(std::string_view string, std::string_view subStr) -> bool
{
    return string.find(subStr) != std::string::npos;
}

[[nodiscard]] NCE_FORCE_INLINE auto LowercaseString(std::string& str)
{
    std::ranges::transform(str, str.begin(), [](char& c) { return static_cast<char>(std::tolower(c)); });
}

[[nodiscard]] NCE_FORCE_INLINE auto UppercaseString(std::string& str)
{
    std::ranges::transform(str, str.begin(), [](char& c) { return static_cast<char>(std::toupper(c)); });
}

} // nce namespace
