#pragma once

#include <nce/Component.hpp>
#include <nce/NCE.hpp>

namespace nce {
class AppState;

class GameObject : public sf::Transformable {
public:
    explicit GameObject(AppState* currentState);
    ~GameObject() override = default;

    [[nodiscard]] ErrResult init();
    [[nodiscard]] ErrResult onEnterState();
    [[nodiscard]] ErrResult onExitState();

    void update(const sf::Time& dt);
    void fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT);

    void setActive(bool active) { m_isActive = active; }
    void setIdentifier(std::string_view id) { m_identifier = id; }

    [[nodiscard]] auto isActive() const -> bool { return m_isActive; }
    [[nodiscard]] auto getIdentifier() const -> std::string_view { return m_identifier; }

    template <typename T, class... Args>
        requires std::is_base_of_v<Component, T>
    void addComponent(Args&&... args) noexcept(noexcept(T(std::forward<Args>(args)...)));

    void removeComponentByTag(u32 tag);

    template <typename T>
        requires std::is_base_of_v<Component, T>
    void removeComponentByType();

    template <typename T>
        requires std::is_base_of_v<Component, T>
    [[nodiscard]] T* getComponentByTag(u32 tag);

    template <typename T>
        requires std::is_base_of_v<Component, T>
    [[nodiscard]] T* getComponentByType();

    template <typename T>
        requires std::is_base_of_v<Component, T>
    [[nodiscard]] std::vector<T*> getComponentsByType();

    [[nodiscard]] AppState* getAppState() const { return m_currentState; }

private:
    bool m_isActive { true };
    AppState* m_currentState;
    std::string m_identifier;
    std::vector<std::unique_ptr<Component>> m_components;
};

template <typename T, class... Args>
    requires std::is_base_of_v<Component, T>
void GameObject::addComponent(Args&&... args) noexcept(noexcept(T(std::forward<Args>(args)...)))
{
    m_components.emplace_back(NCE_NEW T(std::forward<Args>(args)...));
}

template <typename T>
    requires std::is_base_of_v<Component, T>

void GameObject::removeComponentByType()
{
    std::erase_if(m_components, [](std::unique_ptr<Component>& comp) -> bool { return dynamic_cast<T*>(comp.get()) != nullptr; });
}
template <typename T>
    requires std::is_base_of_v<Component, T>

T* GameObject::getComponentByTag(u32 tag)
{
    auto result = std::ranges::find(m_components, tag, &Component::getTag);
    if (result == m_components.end())
        return nullptr;

    auto casted = dynamic_cast<T*>(result->get());
    if (!casted)
        return nullptr;

    return casted;
}

template <typename T>
    requires std::is_base_of_v<Component, T>

T* GameObject::getComponentByType()
{
    auto result = std::ranges::find_if(m_components, [](const std::unique_ptr<Component>& comp) -> bool { return dynamic_cast<T*>(comp.get()) != nullptr; });

    if (result == m_components.end())
        return nullptr;

    return dynamic_cast<T*>(result->get());
}

template <typename T>
    requires std::is_base_of_v<Component, T>
std::vector<T*> GameObject::getComponentsByType()
{
    std::vector<T*> output;
    auto range = m_components | std::views::filter([](const std::unique_ptr<Component>& comp) -> bool { return dynamic_cast<T*>(comp.get()) != nullptr; });
    for (std::unique_ptr<Component>& ptr : range)
        output.emplace_back(dynamic_cast<T*>(ptr.get()));

    return output;
    // auto result = std::ranges::find_if(m_components, [](const std::unique_ptr<Component>& comp) -> bool { return dynamic_cast<T*>(comp.get()) != nullptr; });
    //
    // if (result == m_components.end())
    //     return nullptr;
    //
    // return dynamic_cast<T*>(result->get());
}
} // nce namespace
