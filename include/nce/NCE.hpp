#pragma once

#include <tl/expected.hpp>

#ifdef _MSC_VER
#define NCE_FORCE_INLINE __forceinline
#else
#define NCE_FORCE_INLINE inline
#endif

// No throw form of new
#define NCE_NEW new (std::nothrow)
#define NCE_PLACEMENT_NEW(mem, type) new (mem) type
#define NCE_DEPRECATED __declspec(deprecated("NCE Deprecated method"))
#define NCE_UNUSED(var) (void)var

namespace nce {
/// <summary>
/// General engine utility header
/// </summary>

auto GenerateUUID() -> std::string;

constexpr auto CalculateAspectRatio(const sf::Vector2u& resolution) -> sf::Vector2u
{
    return resolution == sf::Vector2u {} ? resolution : (resolution / std::gcd(resolution.x, resolution.y));
}

auto WorldPosToScreenPos(sf::Vector2f worldPos) -> sf::Vector2i;

using ErrResult = tl::expected<bool, std::string>;
using ErrVal = tl::unexpected<std::string>;
} // nce namespace
