#pragma once

#include <SFML/Window/Joystick.hpp>

namespace nce {
constexpr auto MicrosoftVendorID { 0x45Eu };
constexpr std::array<uint32_t, 11> MicrosoftSupportedProductIDs { 0x2DD, 0x2E3, 0x2EA, 0x2FD, 0x2D1, 0x289, 0x028E, 0x0288F, 0x202, 0x285, 0x0B12 };

constexpr auto SonyVendorID { 0x54Cu };
constexpr std::array<uint32_t, 4> SonySupportedProductIDs { 0x5C4, 0x9CC, 0x0CE6, 0x0DF2 };

class GamePadImpl;
class GamePad {
public:
    /*
    Axes    LT  RT
            ------
            LX  RX
    */
    enum class Axis { // Named as though on a PS4 since we only support gamepads
        LeftStick,
        RightStick,
        LeftTrigger,
        RightTrigger,
        DirectionX,
        DirectionY,
        Max
    };

    /*
    Buttons
                         Y
     Back  Guide       X   B
                         A

    */
    enum class Buttons { // Named as though on PS4 pad since we only support gamepads
        A,
        X,
        Y,
        B,
        Start,
        Back,
        LeftShoulder,
        RightShoulder,
        LeftTrigger,
        RightTrigger,
        DirectionUp,
        DirectionRight,
        DirectionDown,
        DirectionLeft,
        LeftStickClick,
        RightStickClick,

        // The ones below here don't work for pressed events
        LeftStickUp,
        LeftStickUpRight,
        LeftStickUpLeft,
        LeftStickDown,
        LeftStickDownRight,
        LeftStickDownLeft,
        LeftStickLeft,
        LeftStickRight,
        RightStickUp,
        RightStickUpRight,
        RightStickUpLeft,
        RightStickDown,
        RightStickDownRight,
        RightStickDownLeft,
        RightStickLeft,
        RightStickRight,
        UnsupportedButton,
        MAX
    };

    struct JoystickInfo {
        sf::Vector2f direction;
        f32 magnitude;
    };

    GamePad();
    ~GamePad();

    void update();
    void handleEvents(sf::Event event);

    [[nodiscard]] auto buttonPressed(Buttons button) const -> bool;
    [[nodiscard]] auto buttonReleased(Buttons button) const -> bool;
    [[nodiscard]] auto buttonHeld(Buttons button) const -> bool;
    [[nodiscard]] auto getLeftStickInfo() const -> JoystickInfo;
    [[nodiscard]] auto getRightStickInfo() const -> JoystickInfo;
    [[nodiscard]] auto getLeftTriggerValue() const -> f32;
    [[nodiscard]] auto getRightTriggerValue() const -> f32;
    auto hasValidConnectedDevice() const -> bool;

private:
    auto buttonIDToEnum(uint16_t id) const -> Buttons;
    auto getNormalizedStickAxis(const sf::Vector2f& rawAxis, bool isLeftStick) const -> sf::Vector2f;
    auto getNormalizedTriggerAxis(float raw) const -> float;

    std::unique_ptr<GamePadImpl> m_impl;
};
} // nce namespace
