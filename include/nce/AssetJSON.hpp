#pragma once

#include <nce/AssetsInfo.hpp>
#include <nlohmann/json.hpp>
#include <tl/expected.hpp>

namespace nce {
constexpr auto ASSET_DETAILS_FILE_NAME { "assets.json" };

class AssetJSON {
public:
    enum class FileOrigin { Disk, Zip };
    explicit AssetJSON(FileOrigin fileOrigin);

    tl::expected<AssetsInfo, std::string> getJSONData();

private:
    void loadFromJSONNode(const nlohmann::json& jsonRootNode);

    AssetsInfo m_assetInfo;
    FileOrigin m_fileOrigin;
    bool m_loaded { false };
};
} // nce namespace