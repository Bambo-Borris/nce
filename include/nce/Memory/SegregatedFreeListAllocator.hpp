#pragma once

#include <cstddef>
#include <memory>
#include <vector>

namespace nce {
class SegregatedFreeListAllocator {
public:
    explicit SegregatedFreeListAllocator(std::size_t blockSize, std::size_t blockCount);
    ~SegregatedFreeListAllocator();

    void* allocate(std::size_t size);
    void deallocate(void* ptr, std::size_t size);

private:
    struct FreeBlock {
        FreeBlock* next;
    };

    std::vector<FreeBlock*> m_freeLists;
    std::unique_ptr<std::byte[]> m_memoryPool;
    std::size_t m_blockSize;
    std::size_t m_blockCount;
};

inline SegregatedFreeListAllocator::SegregatedFreeListAllocator(std::size_t blockSize, std::size_t blockCount)
    : m_blockSize(blockSize)
    , m_blockCount(blockCount)
{
    m_memoryPool = std::make_unique<std::byte[]>(blockSize * m_blockCount);
    m_freeLists.resize(blockSize + 1, nullptr);

    for (std::size_t i = 0; i < blockCount; ++i) {
        auto* block = reinterpret_cast<FreeBlock*>(m_memoryPool.get() + i * blockSize);
        block->next = m_freeLists[blockSize];
        m_freeLists[blockSize] = block;
    }
}

inline SegregatedFreeListAllocator::~SegregatedFreeListAllocator() = default;

inline void* SegregatedFreeListAllocator::allocate(std::size_t size)
{
    if (size > m_blockSize || m_freeLists[size] == nullptr) {
        return nullptr; // Allocation failed
    }

    FreeBlock* block = m_freeLists[size];
    m_freeLists[size] = block->next;
    return block;
}

inline void SegregatedFreeListAllocator::deallocate(void* ptr, std::size_t size)
{
    if (size > m_blockSize) {
        return; // Invalid block size
    }

    auto* block = static_cast<FreeBlock*>(ptr);
    block->next = m_freeLists[size];
    m_freeLists[size] = block;
}
} // nce namespace