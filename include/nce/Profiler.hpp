#pragma once

#include <chrono>

namespace nce {
struct ProfilerResult {
    std::string tag;
    std::chrono::time_point<std::chrono::high_resolution_clock> start;
};

[[nodiscard]] inline ProfilerResult StartProfilerResultGathering(std::string_view tag)
{
    return { .tag = tag.data(), .start = std::chrono::high_resolution_clock::now() };
}

inline void EndProfilerResultGathering(const ProfilerResult& profilerResult)
{
    const auto duration { std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - profilerResult.start) };
    spdlog::info(
        "NCE Profiler Result: Profiler Result with tag {} completed execution with a duration of {} microseconds", profilerResult.tag, duration.count());
}
} // nce namespace
