#pragma once

#include <SFML/System.hpp>
#include <functional>
#include <nce/Memory/SegregatedFreeListAllocator.hpp>
#include <string>
#include <unordered_map>
#include <vector>

namespace nce {

/// <summary>
/// Enumeration to specify the type of the timer.
/// SingleShot: The timer triggers once and then stops.
/// Repeating: The timer triggers repeatedly at the specified interval.
/// </summary>
enum class TimerType { SingleShot, Repeating };

/// <summary>
/// Structure representing a timer with its properties.
/// </summary>
struct Timer {
    sf::Time interval; // The interval at which the timer triggers.
    sf::Time elapsed; // The elapsed time since the timer started.
    bool isPaused; // Whether the timer is paused.
    TimerType type; // The type of the timer (SingleShot or Repeating).
    std::function<void()> callback; // The callback function to be executed when the timer triggers.
};

/// <summary>
/// A utility class for managing timers. This class allows creating, updating,
/// and managing multiple timers, each with a callback that is triggered upon timer completion.
/// </summary>
class TimerController {
public:
    TimerController();
    ~TimerController();

    /// <summary>
    /// Adds a new timer.
    /// </summary>
    /// <param name="name">The name of the timer.</param>
    /// <param name="interval">The interval at which the timer should trigger.</param>
    /// <param name="type">The type of the timer (SingleShot or Repeating).</param>
    /// <param name="callback">The callback function to be executed when the timer triggers.</param>
    /// <param name="startPaused">Whether the timer should start paused.</param>
    void addTimer(std::string_view name, sf::Time interval, TimerType type, std::function<void()> callback, bool startPaused = false);

    /// <summary>
    /// Updates all timers with the given delta time.
    /// </summary>
    /// <param name="dt">The delta time to update the timers with.</param>
    void update(const sf::Time& dt);

    /// <summary>
    /// Resets the specified timer.
    /// </summary>
    /// <param name="name">The name of the timer to reset.</param>
    /// <param name="pauseOnReset">Whether the timer should be paused after resetting.</param>
    void resetTimer(std::string_view name, bool pauseOnReset = false);

    /// <summary>
    /// Pauses the specified timer.
    /// </summary>
    /// <param name="name">The name of the timer to pause.</param>
    void pauseTimer(std::string_view name);

    /// <summary>
    /// Resumes the specified paused timer.
    /// </summary>
    /// <param name="name">The name of the timer to resume.</param>
    void playTimer(std::string_view name);

    /// <summary>
    /// Removes the specified timer.
    /// </summary>
    /// <param name="name">The name of the timer to remove.</param>
    void removeTimer(std::string_view name);

    /// <summary>
    /// Retrieves the elapsed time for the specified timer.
    /// </summary>
    /// <param name="name">The name of the timer to query.</param>
    /// <returns>The elapsed time for the specified timer.</returns>
    [[nodiscard]] auto getElapsed(std::string_view name) -> sf::Time;

    /// <summary>
    /// Checks if the specified timer is paused.
    /// </summary>
    /// <param name="name">The name of the timer to query.</param>
    /// <returns>True if the timer is paused, false otherwise.</returns>
    [[nodiscard]] auto isPaused(std::string_view name) -> bool;

    /// <summary>
    /// Checks if the specified timer exists.
    /// </summary>
    /// <param name="name">The name of the timer to query.</param>
    /// <returns>True if the timer exists, false otherwise.</returns>
    [[nodiscard]] auto exists(std::string_view name) -> bool;

    /// <summary>
    /// Retrieves the names of all timers.
    /// </summary>
    /// <returns>A vector of timer names.</returns>
    [[nodiscard]] auto getKeys() -> std::vector<std::string>;

private:
    std::unordered_map<std::string, Timer*> m_timers; // Map to store timers by name.
    SegregatedFreeListAllocator allocator; // Custom allocator for managing Timer objects.

    /// <summary>
    /// Executes the callback for the specified timer.
    /// </summary>
    /// <param name="name">The name of the timer whose callback to execute.</param>
    void executeCallback(const std::string& name);
};

} // nce namespace
