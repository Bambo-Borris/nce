#pragma once

namespace nce {
constexpr auto ASSET_MAX_SIZE = 1000000;
constexpr std::array<const char*, 2> SUPPORTED_TEXTURE_FORMATS = {
    ".png",
    ".bmp",
};

constexpr std::array<const char*, 3> SUPPORTED_AUDIO_FORMATS = {
    ".mp3",
    ".ogg",
    ".wav",
};

constexpr std::array<const char*, 2> SUPPORTED_SHADER_FORMATS = {
    ".frag",
    ".vert",
};

constexpr std::array<const char*, 1> SUPPORTED_FONT_FORMATS = {
    ".ttf",
};

enum class AssetType { Font, Texture, Shader, Sound };

struct AssetEntry {
    std::string id;
    std::string path;
};

struct AssetsInfo {
    std::vector<AssetEntry> fonts;
    std::vector<AssetEntry> textures;
    std::vector<AssetEntry> shaders;
    std::vector<AssetEntry> sounds;
};

} // nce namespace
