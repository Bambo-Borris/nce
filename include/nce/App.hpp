#pragma once

#include <nce/AppState.hpp>
#include <nce/Components/Renderable.hpp>
#include <nce/ConfigFile.hpp>
#include <nce/NCE.hpp>
#include <nce/Renderer.hpp>

#include <tl/expected.hpp>

namespace nce {

enum class WindowDisplayModes { Windowed, BordlessWindowed, FullScreen, MAX };
constexpr std::array WindowDisplayModesStrings { "Windowed", "Windowed Borderless", "Fullscreen" };

static_assert(static_cast<size_t>(WindowDisplayModes::MAX) == WindowDisplayModesStrings.size(), "Mismatch in enum size vs string array size");

using LoadingCallback = std::function<nce::AppState*(std::string stateName, std::any metaData)>;

struct AppInitData {
    const sf::Vector2u& targetResolution;
    std::string_view windowTitle;
    unsigned int targetFPS;
    unsigned int targetFixedFPS;
    LoadingCallback loadingCallback;
    std::string_view loadingSplashPath;
    std::string_view startState;
};

class App {
public:
    ~App();

    static App& get() noexcept
    {
        static auto app = NCE_NEW App;
        return *app;
    }

    /// <summary>
    /// Initialise the App singleton to create a new app
    /// </summary>
    /// <returns>Returns a bool with a value of true, or a string containing the error.</returns>
    nce::ErrResult init(const AppInitData& appInitData);

    /// <summary>
    /// Invoked to begin the core loop of the application.
    /// </summary>
    void run();

    /// <summary>
    /// Will set the display mode of the application window. Takes into account aspect
    /// ratio to add black bars to maintain current target aspect ratio. Result achieved by
    /// recreating the window where necessary.
    /// </summary>
    /// <param name="displayMode">Which display mode would be preferred</param>
    /// <param name="resolution">App resolution (unsupported in BorderlessWindowed), if (0, 0) changes nothing.</param>
    void setDisplayModeAndReconstruct(WindowDisplayModes displayMode, sf::Vector2u resolution = {}, bool saveConfig = true);

    /// <summary>
    /// Get the display mode of the app.
    /// </summary>
    /// <returns>Current display mode</returns>
    [[nodiscard]] auto getDisplayMode() const -> WindowDisplayModes { return m_displayMode; }

    /// <summary>
    /// Set the master volume on the users app config, calls to this will ensure that
    /// this is serialised to disk if the user closes the application.
    /// </summary>
    /// <param name="masterVolume">The volume level to set</param>
    void setMasterVolumeConfig(int32_t masterVolume);

    [[nodiscard]] auto getMasterVolume() const -> i32;

    auto getRenderTexture() -> sf::RenderTexture* { return &m_renderTexture; }

    auto getRenderWindow() -> sf::RenderWindow* { return &m_renderWindow; }

    auto getTotalElapsedTime() const -> sf::Time { return m_totalElapsedTime; }

    [[nodiscard]] auto getContextSettings() const -> sf::ContextSettings { return m_contextSettings; }

    [[nodiscard]] auto isLoading() const -> bool { return m_isLoading; }

    [[nodiscard]] auto getFixedStepAsTime() const -> sf::Time { return sf::seconds(1.f / static_cast<f32>(m_targetFixedFPS)); }
    [[nodiscard]] auto getFixedStepAsFrameRate() const -> u32 { return m_targetFixedFPS; }

    [[nodiscard]] auto getStepAsTime() const -> sf::Time { return sf::seconds(1.f / static_cast<f32>(m_targetFPS)); }
    [[nodiscard]] auto getStepAsFrameRate() const -> u32 { return m_targetFPS; }

    [[nodiscard]] auto getRenderer() -> Renderer& { return m_renderer; }
    [[nodiscard]] auto getActiveState() -> AppState* { return m_activeState.get(); }

    /// <summary>
    /// Triggers shutdown sequence by requesting the window be closed.
    /// </summary>
    void closeWindow();

private:
    App() = default;

    /// <summary>
    /// Configure spdlog to write to disk and the console window (where applicable).
    /// Also ensures that the verbosity level is different between debug & release
    /// </summary>
    void setupSpdlog();

    /// <summary>
    /// Log the necessary hardware configuration information to aid with debugging.
    /// </summary>
    static void logHardwareInfo();

    /// <summary>
    /// Log the frame rate of the application to the game window.
    /// </summary>
    /// <param name="dt">Frame time</param>
    void logFPS(const sf::Time& dt);

    /// <summary>
    /// Checks if the users hardware configuration meets the necessary
    /// requirements to run the Nebulous Creation Engine.
    /// </summary>
    /// <returns>True if the users hardware is capable of running NCE, otherwise returns false. </returns>
    [[nodiscard]] static bool checkGraphicsCapabilities();

    /// <summary>
    /// Setup the async loading task and marks the app as in a loading
    /// state.
    /// </summary>
    /// <param name="stateName">Name of the state we are loading</param>
    void setupLoadingScreen(std::string_view stateName, std::any data);

    /// <summary>
    /// Called when an nce::AppState is marked as completed and
    /// we need to transition to the next state.
    /// </summary>
    /// <param name="nextStateID">Name of the next state to load</param>
    void changeState(std::string_view nextStateID = "");

    /// <summary>
    /// Update the application
    /// </summary>
    /// <param name="dt">Frame time</param>
    void update(const sf::Time& dt);

    void render();

    /// <summary>
    /// Load the volume configuration setting from the users
    /// app config and set the master audio volume to this value.
    /// </summary>
    // void handleVolumeConfig();

    sf::Vector2u m_targetResolution;
    sf::Vector2u m_targetAspectRatio;
    std::string m_windowTitle;
    LoadingCallback m_loadingCallback;

    // Fixed & set by development team
    unsigned int m_targetFixedFPS;
    sf::Time m_dtFixed;

    // Can be user changed
    unsigned int m_targetFPS;

    sf::RenderWindow m_renderWindow;
    sf::RenderTexture m_renderTexture;
    sf::Texture m_loadingScreenTexture;
    sf::Image m_appIcon;
    std::string m_startState;
    sf::Time m_totalElapsedTime;
    Renderer m_renderer;

    std::unique_ptr<AppState> m_activeState;
    std::unique_ptr<ConfigFile> m_appConfig;
    std::shared_ptr<spdlog::logger> m_logger;

    WindowDisplayModes m_displayMode;
    std::future<tl::expected<AppState*, std::string>> m_appStateFuture;
    bool m_isLoading { true };

    sf::RectangleShape m_loadingSplash;
    sf::ContextSettings m_contextSettings;
};

} // nce namespace

#define NCE_APP() nce::App::get()
