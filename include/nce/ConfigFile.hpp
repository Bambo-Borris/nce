#pragma once

namespace nce {
class ConfigFile {
public:
    enum class ConfigFieldType { Int, Float, String };
    struct ConfigFieldEntry {
        std::any value;
        ConfigFieldType type;
    };

    using ConfigFileSpec = std::map<std::string, ConfigFieldType>;

    ConfigFile(const std::filesystem::path& path, const ConfigFileSpec& specification);
    [[nodiscard]] bool load();
    void save();

    template <typename T>
    [[nodiscard]] std::optional<T> getValue(std::string_view key) const;

    template <typename T>
    void setValue(std::string_view key, const T& value);

private:
    std::filesystem::path m_path;
    ConfigFileSpec m_specification;
    std::map<std::string, ConfigFieldEntry> m_data;
};

template <typename T>
constexpr auto isSupportedType() -> bool
{
    return std::is_same_v<T, int> || std::is_same_v<T, float> || std::is_same_v<T, std::string>;
};

template <typename T>
constexpr auto typeToConfigFileType() -> ConfigFile::ConfigFieldType
{
    static_assert(isSupportedType<T>(), "Unsupported type  passed to typeToConfigFileType");

    ConfigFile::ConfigFieldType type { ConfigFile::ConfigFieldType::Int };
    if constexpr (std::is_same_v<T, int>) {
        type = ConfigFile::ConfigFieldType::Int;
    } else if constexpr (std::is_same_v<T, float>) {
        type = ConfigFile::ConfigFieldType::Float;
    } else if constexpr (std::is_same_v<T, std::string>) {
        type = ConfigFile::ConfigFieldType::String;
    }
    return type;
}

template <typename T>
inline std::optional<T> ConfigFile::getValue(std::string_view key) const
{
    static_assert(isSupportedType<T>(), "Unsupported type passed to ConfigFile::getValue");
    const auto type { typeToConfigFileType<T>() };
    const auto findResult = m_data.find(key.data());
    if (findResult == m_data.end()) {
        return std::nullopt;
    }

    if (findResult->second.type != type)
        return std::nullopt;

    return { std::any_cast<const T&>(findResult->second.value) };
}

template <typename T>
inline void ConfigFile::setValue(std::string_view key, const T& value)
{
    static_assert(isSupportedType<T>(), "Unsupported type passed to ConfigFile::setValue");

    auto findResult = m_data.find(key.data());
    const auto type = typeToConfigFileType<T>();
    if (findResult == m_data.end()) {
        spdlog::error("Unable to set value for key {} because key is not found", key);
        return;
    }

    if (findResult->second.type != type) {
        spdlog::error("Unable to set value for key {} because type is invalid", key);
        return;
    }

    findResult->second.value = value;
}
} // nce namespace
