#pragma once

#include <nce/Collisions.hpp>

namespace nce {
class CollisionSolver {
public:
    using OnCollisionCallback = std::function<void(const Manifold&, GameObject* const)>;
    using OnCollisionExitCallback = std::function<void(GameObject* const object)>;

    struct CallbackInfoOnCollision {
        uint64_t id {};
        OnCollisionCallback cb {};
    };

    struct CallbackInfoCollisionExit {
        uint64_t id {};
        OnCollisionExitCallback cb {};
    };

    // size
    CollisionSolver() = default;
    ~CollisionSolver() = default;

    CollisionSolver(const sf::Vector2u& cellSize, const sf::Vector2u& gridSize, const sf::Vector2f& position = sf::Vector2f { 0.f, 0.f });

    void update();
    void draw(sf::RenderTarget& target) const;

    // returns true if the collider could successfully be added
    [[nodiscard]] auto addCollider(const ColliderInfo& collider) -> std::optional<std::uint64_t>;
    void removeCollider(std::uint64_t id);

    // TODO: The way this system works now is clunky and requires the user to keep track of 2 IDs
    //  the better way would be to have a common interface clients inherit from and subscribe
    //  themselves as the call back listener, and unsub on destructor
    [[nodiscard]] u64 subscribeOnCollisionCallback(const CallbackInfoOnCollision& info);
    void unsubscribeOnCollisionCallBack(u64 subscriberID);

    [[nodiscard]] u64 subscribeCollisionExitCallback(const CallbackInfoCollisionExit& info);
    void unsubscribeCollisionExitCallBack(u64 subscriberID);

    void shouldDebugDraw(bool shouldDebugDraw) { m_enableDebugDrawing = shouldDebugDraw; }

    [[nodiscard]] auto getNumberOfColliders() const -> size_t { return m_colliders.size(); }
    [[nodiscard]] auto getNumberOfSubscribers() const -> size_t { return m_onCollisionCallbackSubscribers.size(); }
    [[nodiscard]] auto getGridSize() const -> sf::Vector2u { return m_gridSize; }
    [[nodiscard]] auto getCellSize() const -> sf::Vector2u { return m_cellSize; }
    [[nodiscard]] auto debugDrawEnabled() const -> bool { return m_enableDebugDrawing; }
    [[nodiscard]] ColliderInfo* getColliderInfo(u64 colliderId) const;
    /// <summary>
    /// To be used when needing to resize cells, grid size or adjust the entire grid positions
    /// </summary>
    /// <param name="cellSize">sf::Vector2u</param>
    /// <param name="gridSize">sf::Vector2u</param>
    /// <param name="position">sf::Vector2u (optional)</param>
    void reconstructGrid(const sf::Vector2u& cellSize, const sf::Vector2u& gridSize, const sf::Vector2f& position = sf::Vector2f { 0.f, 0.f });

    void setDebugDrawScale(sf::Vector2f scale) { m_debugDrawScale = scale; }

    /// <summary>
    /// Casts a ray from the start position specified, in the direction passed, and returns
    /// a pointer to the object hit. The ray will terminate after the distance supplied.
    /// </summary>
    /// <param name="start">Start position of the ray</param>
    /// <param name="direction">Direction the ray will travel in (as an angle)</param>
    /// <param name="distance">The maximum distance the ray can travel</param>
    /// <returns>An optional wrapping over the GameObject hit, and the intersection point of the ray</returns>
    [[nodiscard]] auto castRay(const sf::Vector2f& start, sf::Angle direction, float distance, GameObject* toIgnore = nullptr)
        -> std::optional<std::pair<GameObject*, sf::Vector2f>>;

    // Check if an AABB is intersecting with anything, only returns true or false.
    // This is also an imprecise check as it converts the colliders in the cell occupied by
    // the parameter 'rect' into AABB colliders to perform the intersection test cheaply.
    [[nodiscard]] auto queryBoxForIntersections(const sf::FloatRect& rect) -> bool;

private:
    using CollisionJumpTable
        = std::array<std::array<std::function<std::optional<Manifold>(const ColliderInfo& a, const ColliderInfo& b)>, size_t(ColliderType::MAX)>,
                     size_t(ColliderType::MAX)>;

    // Connects the collider info to a unique ID
    struct ColliderInfoEntry {
        std::unique_ptr<ColliderInfo> colliderInfo;
        uint64_t id;
    };

    struct CollisionPair {
        ColliderInfoEntry* a { nullptr };
        ColliderInfoEntry* b { nullptr };
        Manifold m {};

        bool operator==(const CollisionPair& rhs) const { return a == rhs.a && b == rhs.b; }
    };

    struct Cell {
        std::vector<ColliderInfoEntry*> entries;
    };

    enum class DebugGraphicsLayers : size_t { Grid, Occupied, MAX };

    void updateColliders();
    std::vector<CollisionPair> broadPhase();
    void narrowPhase(std::vector<CollisionPair>& collisionPairs);
    void updateDebugVertices();
    void updateDebugShapes();
    sf::Vector2u convertWorldPositionToGridPosition(const sf::Vector2f& position) const
    {
        return sf::Vector2u { uint32_t(uint32_t(position.x - m_position.x) / m_cellSize.x), uint32_t(uint32_t(position.y - m_position.y) / m_cellSize.y) };
    };

    std::vector<ColliderInfoEntry> m_colliders;
    std::vector<std::shared_ptr<sf::Shape>> m_colliderDebugShapes;
    std::unordered_map<u64, CallbackInfoOnCollision> m_onCollisionCallbackSubscribers;
    std::unordered_map<u64, CallbackInfoCollisionExit> m_collisionExitCallbackSubscribers;
    std::vector<Cell> m_gridCells;
    std::array<sf::VertexArray, size_t(DebugGraphicsLayers::MAX)> m_debugVertices;

    std::vector<CollisionPair> m_currentlyColliding;

    CollisionJumpTable m_jumpTable;
    sf::VertexArray m_debugNormals;

    sf::Vector2u m_gridSize;
    sf::Vector2u m_cellSize;
    sf::Vector2f m_position; // top left origin of the grid
    sf::Vector2f m_debugDrawScale;

    uint64_t m_nextUniqueID { 0 };
    u64 m_onCollisionCallbackSubscribersIDCounter { 0 };
    u64 m_collisionExitCallbackSubscribersIDCounter { 0 };

    bool m_enableDebugDrawing { false };
};
} // nce namespace
