#pragma once

namespace nce {
class AppState;
class HUDElement : public sf::Drawable {
public:
    enum class OriginLocation { TopLeft, Centre };

    HUDElement(AppState* appState);
    virtual ~HUDElement() = default;

    auto setLayer(uint32_t layer) { m_layer = layer; }
    [[nodiscard]] auto getLayer() const -> uint32_t { return m_layer; }

    virtual void update(const sf::Time& dt, sf::RenderTarget& target) = 0;

    auto setOriginLocation(OriginLocation location) { m_originLocation = location; }
    [[nodiscard]] auto getOriginLocation() -> OriginLocation { return m_originLocation; }

    auto setActive(bool active) { m_active = active; }
    [[nodiscard]] auto isActive() -> bool { return m_active; }

    [[nodiscard]] auto getAppState() -> AppState* { return m_appState; }

    [[nodiscard]] auto getIdentifier() -> std::string_view { return m_identifier; }

    void setIdentifier(std::string_view identifier) { m_identifier = identifier; }

private:
    OriginLocation m_originLocation { OriginLocation::TopLeft };
    AppState* m_appState;
    uint32_t m_layer { 0 };
    std::string m_identifier;
    bool m_active { true };
};
} // nce namespace
