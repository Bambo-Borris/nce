#pragma once

#include <nce/Components/ColliderBase.hpp>

namespace nce {
class CircleCollider : public ColliderBase {
public:
    CircleCollider(f32 radius, GameObject* parentObject, i32 mask = 1);

    ErrResult init() override;

private:
    f32 m_radius;
};
} // nce namespace