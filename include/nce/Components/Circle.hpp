#pragma once

#include <nce/Components/Renderable.hpp>
#include <nce/GameObject.hpp>

namespace nce {
class Circle : public Renderable {
public:
    Circle(f32 radius, GameObject* parentObject);

    void setRadius(f32 radius) { m_shape.setRadius(radius); }
    [[nodiscard]] f32 getRadius() const { return m_shape.getRadius(); }

    [[nodiscard]] sf::FloatRect getGlobalBounds() const { return getParentObject()->getTransform().transformRect(m_shape.getLocalBounds()); }
    [[nodiscard]] sf::FloatRect getLocalBounds() const { return m_shape.getLocalBounds(); }

    void setFillColor(const sf::Color& color) { m_shape.setFillColor(color); }

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    sf::CircleShape m_shape;
};
} // nce namespace