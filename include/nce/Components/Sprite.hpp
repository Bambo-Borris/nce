#pragma once

#include <nce/Components/Renderable.hpp>
#include <nce/GameObject.hpp>

namespace nce {
class Sprite : public Renderable {
public:
    Sprite(sf::Texture* texture, GameObject* parentObject);

    ErrResult init() override;
    void setTexture(sf::Texture* texture);
    void setTextureRect(const sf::IntRect& rect);
    void setFillColor(const sf::Color& color);

    [[nodiscard]] sf::FloatRect getLocalBounds() const;
    [[nodiscard]] sf::FloatRect getGlobalBounds() const;

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    std::unique_ptr<sf::Sprite> m_sprite;
    sf::Texture* m_texture;
};
} // nce namespace