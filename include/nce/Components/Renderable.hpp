#pragma once

#include <nce/Component.hpp>

#include <SFML/Graphics/Drawable.hpp>

namespace nce {
class Renderable : public Component, public sf::Drawable {
public:
    Renderable(std::string_view name, u32 tag, GameObject* parentObject)
        : Component(name, tag, parentObject)
    {
    }
    ~Renderable() override = default;

    void setLayer(i32 layer) { m_layer = layer; }
    [[nodiscard]] auto getLayer() const -> i32 { return m_layer; }

private:
    i32 m_layer { 0 };
};
} // nce namespace
