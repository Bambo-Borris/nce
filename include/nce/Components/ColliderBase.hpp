#pragma once

#include <nce/App.hpp>
#include <nce/CollisionSolver.hpp>
#include <nce/Component.hpp>
#include <nce/Components/Tags.hpp>

namespace nce {
class ColliderBase : public Component {
public:
    ColliderBase(u32 tag, GameObject* parentObject, i32 mask = 1)
        : Component(ComponentNames[tag], tag, parentObject)
        , m_collisionMask(mask)
    {
    }

    ErrResult onExitState() override;

    virtual void subscribeCollisionCallback(const CollisionSolver::OnCollisionCallback& info);
    virtual void unsubCollisionCallback();

    virtual void subscribeCollisionExitCallback(const CollisionSolver::OnCollisionExitCallback& info);
    virtual void unsubCollisionExitCallback();

    [[nodiscard]] sf::FloatRect getBoundedBox() const;

protected:
    u64 m_colliderID {};
    std::optional<u64> m_subIDCollision;
    std::optional<u64> m_subIDCollisionExit;
    i32 m_collisionMask;
};
} // nce namespace