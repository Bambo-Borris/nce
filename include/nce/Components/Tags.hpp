#pragma once

#include <array>

namespace nce {

namespace ComponentTags {
// clang-format off
enum : u32 {
    Sprite = 0,
    AnimatedSprite,
    Circle,
    Rectangle,
    AABBCollider,
    CircleCollider,
    BoxCollider,
    PhysicsObject,
    MAX_BUILT_IN_COMPONENTS
};
// clang-format on
}

// clang-format off
constexpr std::array ComponentNames {
    "Sprite",
    "Animated Sprite",
    "Circle",
    "Rectangle",
    "AABB Collider",
    "Circle Collider",
    "Box Collider",
    "Physics Object"
};

static_assert(ComponentNames.size() == ComponentTags::MAX_BUILT_IN_COMPONENTS, "Element count mismatch between string array & component tags enum");
// clang-format on

} // nce namespace