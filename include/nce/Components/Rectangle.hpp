#pragma once

#include <nce/Components/Renderable.hpp>
#include <nce/GameObject.hpp>

namespace nce {
class Rectangle : public Renderable {
public:
    Rectangle(const sf::Vector2f& size, GameObject* parentObject);

    void setSize(const sf::Vector2f& size) { m_shape.setSize(size); }
    const sf::Vector2f& getSize() const { return m_shape.getSize(); }

    [[nodiscard]] sf::FloatRect getGlobalBounds() const { return getParentObject()->getTransform().transformRect(m_shape.getLocalBounds()); }
    [[nodiscard]] sf::FloatRect getLocalBounds() const { return m_shape.getLocalBounds(); }

    void setFillColor(const sf::Color& color) { m_shape.setFillColor(color); }

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    sf::RectangleShape m_shape;
};
} // nce namespace