#pragma once

#include <nce/Components/AABBCollider.hpp>
#include <nce/Components/AnimatedSprite.hpp>
#include <nce/Components/BoxCollider.hpp>
#include <nce/Components/Circle.hpp>
#include <nce/Components/CircleCollider.hpp>
#include <nce/Components/PhysicsObject.hpp>
#include <nce/Components/Rectangle.hpp>
#include <nce/Components/Sprite.hpp>
#include <nce/GameObject.hpp>

namespace nce {

/// <summary>
/// Utility function to make accessing getComponentByTag
/// easier
/// </summary>
/// <typeparam name="T"></typeparam>
/// <returns></returns>
template <typename T>
    requires std::is_base_of_v<Component, T>
constexpr inline T* GetNCEComponentByTag(GameObject* object)
{
    if constexpr (std::is_same_v<T, AABBCollider>) {
        return object->getComponentByTag<T>(ComponentTags::AABBCollider);
    } else if constexpr (std::is_same_v<T, AnimatedSprite>) {
        return object->getComponentByTag<T>(ComponentTags::AnimatedSprite);
    } else if constexpr (std::is_same_v<T, BoxCollider>) {
        return object->getComponentByTag<T>(ComponentTags::BoxCollider);
    } else if constexpr (std::is_same_v<T, Circle>) {
        return object->getComponentByTag<T>(ComponentTags::Circle);
    } else if constexpr (std::is_same_v<T, CircleCollider>) {
        return object->getComponentByTag<T>(ComponentTags::CircleCollider);
    } else if constexpr (std::is_same_v<T, PhysicsObject>) {
        return object->getComponentByTag<T>(ComponentTags::PhysicsObject);
    } else if constexpr (std::is_same_v<T, Rectangle>) {
        return object->getComponentByTag<T>(ComponentTags::Rectangle);
    } else if constexpr (std::is_same_v<T, Sprite>) {
        return object->getComponentByTag<T>(ComponentTags::Sprite);
    } else {
        static_assert(false, "Unknown type");
    }
}
} // nce namespace