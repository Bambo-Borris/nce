#pragma once

#include <nce/Component.hpp>

namespace sf {
class Transformable;
} // sf namespace

namespace nce {
enum class BehaviourType { StaticObject, DynamicObject };
class GameObject;
/// <summary>
/// The PhysicsObject is the key element to the physics simulation.
/// Every force, torque moment, velocity cap etc that must be applied
/// to an object must be done to the PhysicsObject. A PhysicsObject
/// simply enables a GameObject to be able to take part in the simulation.
/// </summary>
class PhysicsObject : public Component {
public:
    /// <summary>
    /// Construct a valid PhysicsObject
    /// </summary>
    /// <param name="transformable">Transformable which the PhysicsObject will influence</param>
    /// <param name="type">Is the object dynamic or static in nature</param>
    PhysicsObject(GameObject* parentObject, BehaviourType type = BehaviourType::DynamicObject);

    ErrResult onEnterState() override;

    ErrResult onExitState() override;

    /// <summary>
    /// Advance the simulation of this object by a fixed step interval.
    /// </summary>
    /// <param name="dt">Fixed frame time to use when integrating</param>
    void fixedUpdate(sf::Time dt, sf::Time fixedDt) override;

    /// <summary>
    /// Apply a linear force to the object
    /// </summary>
    /// <param name="force">Force top apply (as a vector) in N</param>
    void applyForce(const sf::Vector2f& force);

    /// <summary>
    /// Apply a torque to the object
    /// </summary>
    /// <param name="torque">Torque in Nm</param>
    void applyTorque(float torque);

    /// <summary>
    /// Set the activity state of this object, an
    /// inactive object will no longer take part in
    /// the simulation
    /// </summary>
    /// <param name="active">True for active, false for inactive</param>
    void setActivity(bool active);

    /// <summary>
    /// Set the mass of the PhysicsObject
    /// </summary>
    /// <param name="mass">Mass in kg</param>
    void setMass(float mass);

    /// <summary>
    /// Set the mass of the PhysicsObject
    /// </summary>
    /// <param name="mass">Mass in kg</param>
    [[nodiscard]] auto getMass() const -> float { return m_mass; }

    /// <summary>
    /// Set the inertia of the object
    /// </summary>
    /// <param name="inertia">Inertia value to apply in kg.m^2</param>
    void setInertia(float inertia);

    /// <summary>
    /// Get the inertia of the object
    /// </summary>
    /// <returns>Current inertia value in kg.m^2</returns>
    auto getInertia() const -> float { return m_inertia; }

    /// <summary>
    /// Set the linear velocity of the object
    /// </summary>
    /// <param name="velocity">Velocity vector (in m s^-1)</param>
    void setLinearVelocity(const sf::Vector2f& velocity);

    /// <summary>
    /// Get linear linear velocity of the object
    /// </summary>
    /// <returns>Linear velocity in m s^-1</returns>
    [[nodiscard]] auto getLinearVelocity() const -> sf::Vector2f;

    /// <summary>
    /// Set the angular velocity of the object
    /// </summary>
    /// <param name="angularVel">Angular velocity to apply in rad s^-1</param>
    void setAngularVelocity(float angularVel);

    /// <summary>
    /// Get angular velocity of the object
    /// </summary>
    /// <returns>Angular velocity in rad s^-1</returns>
    [[nodiscard]] auto getAngularVelocity() const -> float;

    /// <summary>
    /// Query the object activity state
    /// </summary>
    /// <returns>True if active, false if inactive</returns>
    [[nodiscard]] auto isPhysicsBodyActive() const -> bool;

    void clearForces();
    void clearTorque();

    void setDTMultiplier(f32 dtMultiplier) { m_dtMultiplier = dtMultiplier; }

private:
    sf::Vector2f m_linearVelocity;
    sf::Vector2f m_force;

    f32 m_mass { 0.f };
    f32 m_invMass { 0.f };

    f32 m_inertia { 0.f };
    f32 m_invInertia { 0.f };

    f32 m_angularVelocity { 0.f };
    f32 m_torque { 0.f };

    f32 m_dtMultiplier { 1.f };
    BehaviourType m_type;
    bool m_active { true };
};
} // nce namespace
