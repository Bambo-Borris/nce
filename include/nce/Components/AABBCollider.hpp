#pragma once

#include <nce/Components/ColliderBase.hpp>

namespace nce {
class AABBCollider : public ColliderBase {
public:
    AABBCollider(const sf::Vector2f& size, GameObject* go, i32 mask = 1);

    ErrResult init() override;

private:
    sf::Vector2f m_size;
};
} // nce namespace