////////////////////////////////////////////////////////////
//
// Copyright (C) 2014 Maximilian Wagenbach (aka. Foaly) (foaly.f@web.de)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it freely,
// subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented;
// you must not claim that you wrote the original software.
// If you use this software in a product, an acknowledgment
// in the product documentation would be appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such,
// and must not be misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//
////////////////////////////////////////////////////////////

#pragma once

#include <nce/Components/Animation.hpp>
#include <nce/Components/Renderable.hpp>

namespace nce {
class AnimatedSprite : public Renderable {
public:
    explicit AnimatedSprite(GameObject* parentObject, sf::Time frameTime = sf::seconds(0.2f), bool paused = false, bool looped = true);

    void update(sf::Time deltaTime) override;

    void setAnimation(const Animation& animation);
    void setFrameTime(sf::Time time);

    void play();
    void play(const Animation& animation);

    void pause();
    void stop();

    void setLooped(bool looped);
    void setColor(const sf::Color& color);

    [[nodiscard]] const Animation* getAnimation() const;
    [[nodiscard]] sf::FloatRect getLocalBounds() const;
    [[nodiscard]] sf::FloatRect getGlobalBounds() const;
    [[nodiscard]] bool isLooped() const;
    [[nodiscard]] bool isPlaying() const;
    [[nodiscard]] sf::Time getFrameTime() const;

    void setFrame(std::size_t newFrame, bool resetTime = true);
    [[nodiscard]] auto getCurrentFrameIndex() const -> size_t { return m_currentFrame; }

private:
    const Animation* m_animation { nullptr };
    sf::Time m_frameTime;
    sf::Time m_currentTime { sf::Time::Zero };
    std::size_t m_currentFrame;
    bool m_isPaused;
    bool m_isLooped;
    const sf::Texture* m_texture { nullptr };
    std::array<sf::Vertex, 6> m_vertices;

    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};
} // nce namespace
