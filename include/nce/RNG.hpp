#pragma once
#include <random>
namespace nce {

class RNG {
public:
    template <typename T>
        requires std::is_floating_point_v<T>
    [[nodiscard]] static T realWithinRange(T start, T end);

    template <typename T>
        requires std::is_integral_v<T>
    [[nodiscard]] static T intWithinRange(T start, T end);

private:
    static std::random_device m_randomDevice;
    static std::default_random_engine m_randomEngine;
};

template <typename T>
    requires std::is_floating_point_v<T>
inline T RNG::realWithinRange(T start, T end)
{
    static_assert(std::is_floating_point_v<T>, "Must be float, double or long double!");
    T result { 0 };
    std::uniform_real_distribution<T> dist { start, end };
    result = dist(m_randomEngine);
    return result;
}

template <typename T>
    requires std::is_integral_v<T>
inline T RNG::intWithinRange(T start, T end)
{
    static_assert(std::is_integral_v<T>, "Must be an integral type!");
    T result { 0 };
    std::uniform_int_distribution<T> dist { start, end };
    result = dist(m_randomEngine);
    return result;
}
} // nce namespace
