#pragma once

#include <nce/AppState.hpp>

class BasicState : public nce::AppState {
public:
    BasicState();
    nce::ErrResult init() override;
    nce::ErrResult onStateBegin() override;
    nce::ErrResult onStateEnd() override;

private:
};
