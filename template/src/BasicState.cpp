#include "BasicState.hpp"

#include <nce/AssetHolder.hpp>
#include <nce/ColourUtils.hpp>
#include <nce/Components/AABBCollider.hpp>
#include <nce/Components/AnimatedSprite.hpp>
#include <nce/Components/BoxCollider.hpp>
#include <nce/Components/Circle.hpp>
#include <nce/Components/CircleCollider.hpp>
#include <nce/Components/ComponentUtils.hpp>
#include <nce/Components/PhysicsObject.hpp>
#include <nce/Components/Rectangle.hpp>
#include <nce/Components/Sprite.hpp>
#include <nce/Components/Tags.hpp>
#include <nce/GameObject.hpp>
#include <nce/InputHandler.hpp>

nce::Animation FLAG_ANIMATION;

class GameObjectMover : public nce::Component {
public:
    explicit GameObjectMover(nce::GameObject* parentObject)
        : Component("cirlce mover", nce::ComponentTags::MAX_BUILT_IN_COMPONENTS + 1, parentObject)
    {
    }

    nce::ErrResult onEnterState() override
    {
        auto sprite = nce::GetNCEComponentByTag<nce::AnimatedSprite>(getParentObject());
        sprite->play();
        sprite->setFrameTime(sf::milliseconds(100));
        getParentObject()->setOrigin(sprite->getLocalBounds().getSize() / 2.f);
        getParentObject()->setScale(sf::Vector2f { 64.f, 64.f }.cwiseDiv(sprite->getGlobalBounds().getSize()));
        auto collider = nce::GetNCEComponentByTag<nce::BoxCollider>(getParentObject());
        collider->subscribeCollisionCallback(std::bind(&GameObjectMover::collisionCallback, this, std::placeholders::_1, std::placeholders::_2));
        collider->subscribeCollisionExitCallback(std::bind(&GameObjectMover::collisionExit, this, std::placeholders::_1));
        return true;
    }

    void update(const sf::Time& dt) override
    {
        NCE_UNUSED(dt);
        // getParentObject()->setPosition(nce::InputHandler::GetMouseWorldPosition());

        if (nce::InputHandler::KeyHeld(sf::Keyboard::Scancode::A)) {
            getParentObject()->rotate(dt.asSeconds() * sf::degrees(45.f));
        }

        if (nce::InputHandler::KeyHeld(sf::Keyboard::Scancode::D)) {
            getParentObject()->rotate(dt.asSeconds() * sf::degrees(45.f));
        }

        if (nce::InputHandler::KeyHeld(sf::Keyboard::Scancode::W)) {
            nce::GetNCEComponentByTag<nce::PhysicsObject>(getParentObject())->applyForce({ 2000.f, 0.f });
        }
    }

private:
    void collisionCallback(const nce::Manifold&, nce::GameObject* const)
    {
        auto sprite = nce::GetNCEComponentByTag<nce::AnimatedSprite>(getParentObject());
        assert(sprite);
        sprite->setColor(sf::Color::Red);
    }

    void collisionExit(nce::GameObject* const)
    {
        auto sprite = nce::GetNCEComponentByTag<nce::AnimatedSprite>(getParentObject());
        assert(sprite);
        sprite->setColor(sf::Color::White);
    }
};

BasicState::BasicState()
    : nce::AppState("basic_state")
{
    addSolver(std::unique_ptr<nce::CollisionSolver> { NCE_NEW nce::CollisionSolver(sf::Vector2u { 192, 108 }, sf::Vector2u { 10, 10 }) });
}

nce::ErrResult BasicState::init()
{
    auto testObject = NCE_NEW nce::GameObject(this);
    assert(testObject);

    if (!addGameObject(testObject))
        return nce::ErrVal("Unable to create circle game object");

    auto texture = NCE_ASSETS().getTexture("flag");
    assert(texture);
    FLAG_ANIMATION.setSpriteSheet(*texture);
    FLAG_ANIMATION.addFrame(sf::IntRect { { 0, 0 }, { 64, 64 } });
    FLAG_ANIMATION.addFrame(sf::IntRect { { 64, 0 }, { 64, 64 } });
    FLAG_ANIMATION.addFrame(sf::IntRect { { 128, 0 }, { 64, 64 } });

    testObject->addComponent<nce::AnimatedSprite>(testObject);
    nce::GetNCEComponentByTag<nce::AnimatedSprite>(testObject)->setAnimation(FLAG_ANIMATION);

    testObject->addComponent<GameObjectMover>(testObject);
    testObject->addComponent<nce::BoxCollider>(sf::Vector2f { 32.f, 32.f }, testObject, 0);

    testObject->addComponent<nce::PhysicsObject>(testObject);
    nce::GetNCEComponentByTag<nce::PhysicsObject>(testObject)->setMass(100.f);
    for (size_t i { 0 }; i < 10; ++i) {
        auto go = NCE_NEW nce::GameObject(this);
        assert(go);
        if (!addGameObject(go))
            return nce::ErrVal("Unable to allocate game object in BasicState");

        go->addComponent<nce::Rectangle>(sf::Vector2f { 64.f, 64.f }, go);
        go->setOrigin({ 32.f, 32.f });
        auto rect = go->getComponentByTag<nce::Rectangle>(nce::ComponentTags::Rectangle);
        rect->setFillColor(nce::GenerateRandomColour());
        go->setPosition({ nce::RNG::realWithinRange(0.f, 500.f), nce::RNG::realWithinRange(0.f, 500.f) });

        go->addComponent<nce::BoxCollider>(sf::Vector2f { 64.f, 64.f }, go, 1);
    }
    return AppState::init();
}

nce::ErrResult BasicState::onStateBegin() { return AppState::onStateBegin(); }

nce::ErrResult BasicState::onStateEnd() { return AppState::onStateEnd(); }