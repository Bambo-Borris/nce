#include "BasicState.hpp"

#include <SFML/GpuPreference.hpp>
#include <nce/App.hpp>

#ifdef _MSC_VER
#include <Windows.h>
#include <minidumpapiset.h>
#include <tchar.h>
#endif

#include <SFML/System/Vector2.hpp>
#include <imgui-SFML.h>
#include <imgui.h>

SFML_DEFINE_DISCRETE_GPU_PREFERENCE

// For generating crash dump stuff we need some horrific code
// so here goes nothing!
#ifdef _MSC_VER

typedef BOOL(WINAPI* MINIDUMPWRITEDUMP)(HANDLE hProcess,
                                        DWORD dwPid,
                                        HANDLE hFile,
                                        MINIDUMP_TYPE DumpType,
                                        CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
                                        PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
                                        CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam);

LONG WINAPI HandleException(struct _EXCEPTION_POINTERS* apExceptionInfo)
{
    HMODULE mhLib = ::LoadLibrary(_T("dbghelp.dll"));
    if (!mhLib)
        return EXCEPTION_EXECUTE_HANDLER;
    MINIDUMPWRITEDUMP pDump = (MINIDUMPWRITEDUMP)::GetProcAddress(mhLib, "MiniDumpWriteDump");

    HANDLE hFile = ::CreateFile(_T("nce-template.dmp"), GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    _MINIDUMP_EXCEPTION_INFORMATION ExInfo;
    ExInfo.ThreadId = ::GetCurrentThreadId();
    ExInfo.ExceptionPointers = apExceptionInfo;
    ExInfo.ClientPointers = FALSE;

    pDump(GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &ExInfo, NULL, NULL);
    ::CloseHandle(hFile);

    return EXCEPTION_EXECUTE_HANDLER;
}
#endif

constexpr auto WINDOW_TITLE { "NCE Template" };
constexpr auto TARGET_FPS { 60u };
constexpr auto TARGET_FIXED_FPS { 60u };

nce::AppState* loadStateAsync(std::string stateName, std::any metaData)
{
    NCE_UNUSED(stateName);
    NCE_UNUSED(metaData);

    try {
        nce::AppState* state = NCE_NEW BasicState();
        return state;
    } catch (const std::runtime_error& error) {
        spdlog::critical("Unable to load state: {}", error.what());
    }

    return nullptr;
}

int main(int argc, char* argv[])
{
    NCE_UNUSED(argc);
    NCE_UNUSED(argv);

#if _MSC_VER
    SetUnhandledExceptionFilter(HandleException);
#endif
    try {
        const nce::AppInitData appInitData
            = { { 1920, 1080 }, WINDOW_TITLE, TARGET_FPS, TARGET_FIXED_FPS, { loadStateAsync }, "bin/textures/loading_screen.png", "basic_state" };

        auto& app = nce::App::get();
        if (auto result = app.init(appInitData); !result) {
            spdlog::error("NCE App Init Error: {}", result.error());
            spdlog::shutdown();
#ifdef NCE_DEBUG
            std::abort();
#else
            return 1;
#endif
        }
        ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
#ifndef NCE_TEMPLATE_DEBUG
        // Load debug initial state
#else
        // Load release initial state
#endif
        app.run();

        delete &app;
    } catch (const std::runtime_error& e) {
        NCE_UNUSED(e);
        spdlog::shutdown();
        std::abort();
    }

    return 0;
}