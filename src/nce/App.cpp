#include "../stdafx.h"

#include "nce/App.hpp"
#include "nce/AssetHolder.hpp"
#include "nce/InputHandler.hpp"
#include "nce/NCE.hpp"

#include <imgui-SFML.h>
#include <imgui.h>

#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/spdlog.h>
#ifndef _WIN32
#include <spdlog/sinks/stdout_color_sinks.h>
#else
#include <Windows.h>
#include <spdlog/sinks/wincolor_sink.h>
#endif

#include <hwinfo/hwinfo.h>

#include <utility>

[[nodiscard]] sf::View GetLetterboxView(sf::View view, const sf::Vector2u& targetAspectRatio, const sf::Vector2u& newWindowSize)
{
    // Compares the aspect ratio of the window to the aspect ratio of the view,
    // and sets the view's viewport accordingly in order to archive a letterbox effect.
    // A new view (with a new viewport set) is returned.
    auto windowRatio = static_cast<f32>(newWindowSize.x) / static_cast<f32>(newWindowSize.y);
    auto viewRatio = static_cast<f32>(targetAspectRatio.x) / static_cast<f32>(targetAspectRatio.y);
    auto sizeX = 1.f;
    auto sizeY = 1.f;
    auto posX = 0.f;
    auto posY = 0.f;

    auto horizontalSpacing = true;
    if (windowRatio < viewRatio)
        horizontalSpacing = false;

    // If horizontalSpacing is true, the black bars will appear on the left and right side.
    // Otherwise, the black bars will appear on the top and bottom.

    if (horizontalSpacing) {
        sizeX = viewRatio / windowRatio;
        posX = (1 - sizeX) / 2.f;
    } else {
        sizeY = windowRatio / viewRatio;
        posY = (1 - sizeY) / 2.f;
    }

    view.setViewport(sf::FloatRect({ posX, posY }, { sizeX, sizeY }));

    return view;
}

namespace nce {
constexpr auto APP_CONFIG_FILE_NAME { "settings.cfg" };

const nce::ConfigFile::ConfigFileSpec APP_CONFIG_FILE_SPEC { { "window_width", nce::ConfigFile::ConfigFieldType::Int },
                                                             { "window_height", nce::ConfigFile::ConfigFieldType::Int },
                                                             { "window_mode", nce::ConfigFile::ConfigFieldType::String },
                                                             /*{ "anti_aliasing", nce::ConfigFile::ConfigFieldType::Int },*/
                                                             { "master_volume", nce::ConfigFile::ConfigFieldType::Int } };

App::~App()
{
    m_activeState.reset();
    InputHandler::CleanUp();
    ImGui::SFML::Shutdown(m_renderWindow);
}

tl::expected<bool, std::string> App::init(const AppInitData& appInitData)
{
    m_targetResolution = appInitData.targetResolution;
    m_targetAspectRatio = CalculateAspectRatio(m_targetResolution);
    m_windowTitle = appInitData.windowTitle;
    m_loadingCallback = appInitData.loadingCallback;
    m_targetFixedFPS = appInitData.targetFixedFPS;
    m_dtFixed = sf::seconds(1.f / float(appInitData.targetFixedFPS));
    m_targetFPS = appInitData.targetFPS;
    m_startState = appInitData.startState;
    m_displayMode = WindowDisplayModes::BordlessWindowed;

    NCE_UNUSED(m_targetFixedFPS);
    setupSpdlog();
    logHardwareInfo();

    if (!m_appIcon.loadFromFile("bin/textures/icon.png"))
        spdlog::error("No application icon found");

    if (!checkGraphicsCapabilities()) {
        return ErrVal("Graphics capabilities not met, application cannot start");
    }

    const auto setupReasonableDefaults = [this]() -> tl::expected<bool, std::string> {
        const auto& bestFsMode = sf::VideoMode::getDesktopMode();
        if (bestFsMode.size.x < uint32_t(m_targetResolution.x) || bestFsMode.size.y < uint32_t(m_targetResolution.y))
            return nce::ErrVal("Resolution too small");
        setDisplayModeAndReconstruct(m_displayMode, bestFsMode.size);
        m_appConfig->setValue("master_volume", 100);
        m_appConfig->save();
        return true;
    };

    m_appConfig = std::make_unique<nce::ConfigFile>(APP_CONFIG_FILE_NAME, APP_CONFIG_FILE_SPEC);
    assert(m_appConfig);
    bool loadResult { false };
    if (std::filesystem::exists(APP_CONFIG_FILE_NAME)) {
        loadResult = m_appConfig->load();
    }

    // If we fail to load the config file we'll setup a reasonable
    // new default
    if (!loadResult) {
        if (auto result = setupReasonableDefaults(); !result.has_value())
            return result;
    } else {
        // Verify all parameters first
        const auto windowWidth { m_appConfig->getValue<int>("window_width") };
        const auto windowHeight { m_appConfig->getValue<int>("window_height") };
        const auto windowMode { m_appConfig->getValue<std::string>("window_mode") };

        const auto validWidth { windowWidth.has_value() && windowWidth != 0 };
        const auto validHeight { windowHeight.has_value() && windowHeight != 0 };
        auto windowModeEnum { WindowDisplayModes::BordlessWindowed };
        // It's more complex to verify the window mode since it's a string, but we're essentially
        // confirming the following assumptions:
        // -> We have an entry in the optional returned
        // -> Our returned entry isn't empty
        // -> Our returned entry is present in the supported window modes
        // -> The mode enum we find is valid.
        auto validMode { false };
        if (windowMode) {
            if (!windowMode.value().empty()) {
                auto it = std::find(WindowDisplayModesStrings.begin(), WindowDisplayModesStrings.end(), windowMode.value());
                if (it != WindowDisplayModesStrings.end()) {
                    const auto distance = std::distance(WindowDisplayModesStrings.begin(), it);
                    if (int(distance) < int(WindowDisplayModes::MAX)) {
                        windowModeEnum = WindowDisplayModes(distance);
                        validMode = true;
                    }
                }
            }
        }

        // If we found an invalid config option we'll just
        // reset everything.
        if (!validMode || !validWidth || !validHeight) {
            setupReasonableDefaults();
        } else {
            const sf::Vector2u res { uint32_t(windowWidth.value()), uint32_t(windowHeight.value()) };
            setDisplayModeAndReconstruct(windowModeEnum, res, false);
            // handleVolumeConfig();
        }
    }

    m_renderWindow.setIcon(m_appIcon);

    try {
        nce::AssetHolder::get().setAppInstance(this);
    } catch (const std::runtime_error& err) {
        return nce::ErrVal(std::format("Asset Holder init error:\n{}", err.what()));
    }

    if (!ImGui::SFML::Init(m_renderWindow))
        return nce::ErrVal("Unable to initialise ImGui SFML");

    // We can't use the asset holder to handle this as this gets
    // invoked on startup on the main thread, and all textures
    // loaded beyond this point are done on a separate thread
    if (!m_loadingScreenTexture.loadFromFile(appInitData.loadingSplashPath))
        return ErrVal(std::format("Unable to loading splash load texture {}", appInitData.loadingSplashPath));

    m_loadingSplash.setTexture(&m_loadingScreenTexture, true);

    return true;
}

void App::run()
{
    sf::Clock loopClock;
    sf::Time accumulator;

    changeState(m_startState);

    while (m_renderWindow.isOpen()) {
        auto dt = loopClock.restart();
        if (dt > sf::seconds(0.25f)) {
            dt = sf::seconds(0.25f);
        }
        if (!m_isLoading) {
            if (m_activeState->isStateComplete()) {
                changeState();
            }
        }

        // We need to update the handler so it can erase
        // any keys that are considered only pressed events
        nce::InputHandler::Update();

        while (auto event = m_renderWindow.pollEvent()) {
            ImGui::SFML::ProcessEvent(m_renderWindow, event);
            nce::InputHandler::HandleEvent(event);
            if (event.is<sf::Event::Closed>())
                m_renderWindow.close();
        }

        ImGui::SFML::Update(m_renderWindow, dt);

        update(dt);
        m_renderer.update();

        accumulator += dt;
        while (accumulator >= dt) {
            if (!m_isLoading) {
                // TODO: set previous state here
                m_activeState->fixedUpdate(dt, m_dtFixed);
            }
            // TODO: Update interpolation T
            accumulator -= m_dtFixed;
        }

        logFPS(dt);
        // TODO: Interpolation alpha stuff here (for fixed step)
        render();

        // Async state loading, we need to verify that the async call has completed by polling the future every frame.
        // Once there's a value on the future, we then need to verify if the async state failed to init, or returned a nullpointer
        // and report errors where required then abort. If the values are all correct the previous state must have its onStateEnd()
        // method called. If this returns an error we also abort. When successful the previous state will be cleaned up, then the next
        // state will be tracked in the m_activeState unique_ptr. Then the new state will have its on onStateBegin method called, if this
        // fails there is also an abort. If all is successful then loading is set to false and we continue with the new state.
        if (m_isLoading) {
            using namespace std::chrono_literals;
            if (auto s = m_appStateFuture.wait_for(0s); s == std::future_status::ready) {
                m_isLoading = false;
                const auto asyncLoadResult = m_appStateFuture.get();
                if (asyncLoadResult && asyncLoadResult.value()) {
                    if (m_activeState) {
                        // On first state entry we don't have an active state
                        if (auto errResult = m_activeState->onStateEnd(); errResult.has_value()) {
                            spdlog::error("Encountered error when cleaning up state {}", m_activeState->getStateID());
                            spdlog::default_logger()->flush();
                            std::abort();
                        }
                    }

                    m_activeState.reset();
                    m_activeState.reset(asyncLoadResult.value());
                    m_isLoading = false;

                    if (!m_activeState->onStateBegin()) {
                        spdlog::error("Unable to enter state {}", m_activeState->getStateID());
                        spdlog::default_logger()->flush();
                        std::abort();
                    }
                } else {
                    if (!asyncLoadResult.has_value()) // If we have don't a value then the async function returned an error string
                        spdlog::error("Unable to init state, reason: {}", asyncLoadResult.error());
                    else // otherwise there must be a nullpointer for the asyncLoadResult
                        spdlog::error("Unable to swap to new state, state instance is null", asyncLoadResult.error());
                    spdlog::default_logger()->flush();
                }
            }

            NCE_ASSETS().processTextureQueue();
            NCE_ASSETS().processShaderQueue();
        }
        m_totalElapsedTime += dt;
    }

    // When we break out of the loop to exit we should guarantee the active state gets a call to this
    if (m_activeState) {
        auto endResult = m_activeState->onStateEnd();
        if (!endResult) {
            spdlog::error("Unable to exit state {}", endResult.error());
            spdlog::default_logger()->flush();
            std::abort();
        }
    }
}

void App::setDisplayModeAndReconstruct(WindowDisplayModes displayMode, sf::Vector2u resolution, bool saveConfig)
{
    sf::ContextSettings contextSettings;
    u32 windowStyle {};
    sf::State windowState {};

    switch (displayMode) {
        using enum WindowDisplayModes;
    case Windowed:
        windowStyle = sf::Style::Titlebar | sf::Style::Close;
        windowState = sf::State::Windowed;
        break;
    case BordlessWindowed:
        windowStyle = 0;
        windowState = sf::State::Windowed;
        break;
    case FullScreen:
        windowStyle = 0;
        windowState = sf::State::Fullscreen;
        break;
    default:
        assert(false);
    }

    contextSettings.antialiasingLevel = 8;
    contextSettings.majorVersion = 3;
    contextSettings.minorVersion = 2;

    if (!m_renderTexture.create(m_targetResolution, contextSettings)) {
        spdlog::error("Unable to create render texture for application");
        std::exit(1);
    }

    m_renderWindow.create(sf::VideoMode(resolution, 32), m_windowTitle, windowStyle, windowState, contextSettings);
    m_renderWindow.setPosition({ 0, 0 });
    m_renderWindow.setFramerateLimit(m_targetFPS);
    m_renderWindow.setKeyRepeatEnabled(false);
    m_displayMode = displayMode;

    const auto newView = GetLetterboxView(m_renderTexture.getView(), m_targetAspectRatio, resolution);
    m_renderWindow.setView(newView);

    if (saveConfig) {
        m_appConfig->setValue<int>("window_width", int32_t(resolution.x));
        m_appConfig->setValue<int>("window_height", int32_t(resolution.y));
        m_appConfig->setValue<std::string>("window_mode", WindowDisplayModesStrings[size_t(displayMode)]);
        m_appConfig->save();
    }

    m_loadingSplash.setSize(sf::Vector2f { m_renderTexture.getSize() });
    m_contextSettings = contextSettings;
}

void App::closeWindow() { m_renderWindow.close(); }

void App::setupSpdlog()
{
    std::vector<spdlog::sink_ptr> sinks;
#ifdef _WIN32
    sinks.push_back(std::make_shared<spdlog::sinks::wincolor_stdout_sink_mt>());
#else
    sinks.push_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
#endif

    sinks.push_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>("logs/log.txt", true));
    m_logger = std::make_shared<spdlog::logger>("NCE", begin(sinks), end(sinks));
    spdlog::register_logger(m_logger);
    spdlog::set_default_logger(m_logger);

#ifdef NCE_DEBUG
    spdlog::set_level(spdlog::level::debug);
#else
    spdlog::set_level(spdlog::level::info);
#endif
}

void App::logHardwareInfo()
{
    spdlog::info("---------------------------------------------------------------------------");
    spdlog::info("                      Detected Hardware Configuration                      ");
    spdlog::info("----------------------------------- OS ------------------------------------");
    spdlog::info("Name:               {}", hwinfo::OS::getFullName());

    spdlog::info("----------------------------------- CPU -----------------------------------");
    spdlog::info("Vendor:             {}", hwinfo::CPU::getVendor());
    spdlog::info("Model:              {}", hwinfo::CPU::getModelName());
    spdlog::info("Base Clock (GHz):   {}", static_cast<double>(hwinfo::CPU::getRegularClockSpeed_kHz()) / 1e6);
    spdlog::info("Max Clock (GHz):    {}", static_cast<double>(hwinfo::CPU::getMaxClockSpeed_kHz()) / 1e6);

    spdlog::info("----------------------------------- GPU -----------------------------------");
    spdlog::info("Vendor:             {}", hwinfo::GPU::getVendor());
    spdlog::info("Name:               {}", hwinfo::GPU::getName());

    spdlog::info("----------------------------------- RAM -----------------------------------");
    spdlog::info("Total Size (Bytes)  {}", hwinfo::RAM::getTotalSize_Bytes());
    spdlog::info("Total Size (GBytes) {}", hwinfo::RAM::getTotalSize_Bytes() / static_cast<int>(1e9));
    spdlog::info("---------------------------------------------------------------------------");
}

void App::logFPS(const sf::Time& dt)
{
    static int counter = 0;
    static auto sum = sf::Time::Zero;

    if (counter < 10) {
        sum += dt;
        ++counter;
    } else {
        const auto fps = std::round(1.f / (sum.asSeconds() / static_cast<float>(counter)));
        const auto newTitle = fmt::format("{} - FPS {}", m_windowTitle, static_cast<std::uint32_t>(fps));
        m_renderWindow.setTitle(newTitle);
        sum = sf::Time::Zero;
        counter = 0;
    }
}

void App::setMasterVolumeConfig(int32_t masterVolume)
{
    m_appConfig->setValue("master_volume", masterVolume);
    m_appConfig->save();
}

auto App::getMasterVolume() const -> i32 { return *m_appConfig->getValue<i32>("master_volume"); }

bool App::checkGraphicsCapabilities()
{
    if (sf::RenderTexture::getMaximumAntialiasingLevel() < 4) {
        spdlog::error("Invalid maximum AA level supported by system");
        return false;
    }

    if (!sf::Shader::isAvailable()) {
        spdlog::error("Shader support unavailable");
        return false;
    }

    if (!sf::VertexBuffer::isAvailable()) {
        spdlog::error("Vertex buffer support unavailable");
        return false;
    }

    return true;
}

void App::setupLoadingScreen(std::string_view stateName, std::any data)
{
    const auto stateCreator = [this](std::string stateName, std::any metaData) -> tl::expected<nce::AppState*, std::string> {
        auto loadingCallbackResult = m_loadingCallback(stateName, metaData);
        assert(loadingCallbackResult);
        if (loadingCallbackResult) {
            const auto initResult = loadingCallbackResult->init();
            if (!initResult)
                return ErrVal(initResult.error());
            return loadingCallbackResult;
        }

        return ErrVal(std::format("Null state instance encountered when attempting to load {}", stateName));
    };
    m_appStateFuture = std::async(std::launch::async, stateCreator, std::string(stateName.data()), data);
    m_isLoading = true;
}

void App::changeState(std::string_view nextStateID)
{
    try {
        std::string stateID;
        if (nextStateID.empty()) {
            stateID = m_activeState->getNextStateID();
        } else {
            stateID = nextStateID;
        }

        if (stateID.empty())
            throw std::runtime_error("No valid next state ID found");

        std::any data;
        if (m_activeState)
            data = m_activeState->getNextStateData();
        m_activeState.reset(nullptr);
        setupLoadingScreen(stateID, data);

    } catch (const std::runtime_error& e) {
        spdlog::error("{}", e.what());
        throw;
    }
}

void App::update(const sf::Time& dt)
{
    if (!m_isLoading)
        m_activeState->update(dt);
}

void App::render() { m_renderer.render(); }

// void App::handleVolumeConfig()
//{
//     const auto v { m_appConfig->getValue<int>("master_volume").value_or(100) };
//     nce::SoundEventManager::SetMasterVolume(v);
// }

} // nce namespace