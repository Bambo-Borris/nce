#include "../stdafx.h"

#include "nce/App.hpp"
#include "nce/AppState.hpp"
#include "nce/AssetHolder.hpp"
#include "nce/GameObject.hpp"
#include "nce/HUDElement.hpp"

namespace nce {
AppState::AppState(std::string_view identifier)
    : m_stateID(identifier)
    , m_actionChecker(NCE_NEW ButtonActionChecker)
{
    assert(m_actionChecker);
}

AppState::~AppState()
{
    m_gameObjects.clear();
    // m_hudElements.clear();
    AssetHolder::get().unloadAssets();
}

ErrResult AppState::init()
{
    for (auto& obj : m_gameObjects) {
        if (auto errReturn = obj->init(); !errReturn) {
            spdlog::error("Unable to invoke init for GameObject {} in state {}, reason: {}", obj->getIdentifier(), m_stateID, errReturn.error());
            return errReturn;
        }
    }
    return true;
}

ErrResult AppState::onStateBegin()
{
    for (auto& obj : m_gameObjects) {
        if (auto errReturn = obj->onEnterState(); !errReturn) {
            spdlog::error("Unable to invoke onEnterState for GameObject {} in state {}, reason: {}", obj->getIdentifier(), m_stateID, errReturn.error());
            return errReturn;
        }
    }
    return true;
}

ErrResult AppState::onStateEnd()
{
    for (auto& obj : m_gameObjects) {
        if (auto errReturn = obj->onExitState(); !errReturn) {
            spdlog::error("Unable to invoke onExitState for GameObject {} in state {}, reason: {}", obj->getIdentifier(), m_stateID, errReturn.error());
            return errReturn;
        }
    }
    return true;
}

void AppState::update(sf::Time dt)
{
    // m_renderQueueObjects.clear();
    // m_renderQueueHUD.clear();

    if (!m_isPaused) {
        m_timerController.update(dt);
    }

    for (auto& obj : m_gameObjects) {
        if (!obj->isActive())
            continue;
        if (!m_isPaused) {
            obj->update(dt);
        }
    }

    //
    // std::sort(m_renderQueueObjects.begin(), m_renderQueueObjects.end(), [](const GameObject* const a, const GameObject* const b) -> bool {
    //     assert(a);
    //     assert(b);
    //     return a->getRenderLayer() < b->getRenderLayer();
    // });

    // for (auto& element : m_hudElements) {
    //     if (!element->isActive())
    //         continue;
    //     element->update(dt, *NCE_APP().getRenderTexture());
    //     m_renderQueueHUD.push_back(element.get());
    // }
    //
    // std::sort(m_renderQueueHUD.begin(), m_renderQueueHUD.end(), [](const HUDElement* const elementA, const HUDElement* const elementB) -> bool {
    //     // We shouldn't encounter expired objects here
    //     assert(elementA);
    //     assert(elementB);
    //
    //     return elementA->getLayer() < elementB->getLayer();
    // });
}

void AppState::fixedUpdate(sf::Time dt, sf::Time fixedDT)
{
    if (m_isPaused)
        return;
    m_physicsWorld.step(fixedDT);

    if (m_solver)
        m_solver->update();

    for (auto& obj : m_gameObjects) {
        if (!obj->isActive())
            continue;
        obj->fixedUpdate(dt, fixedDT);
    }
}

auto AppState::addGameObject(GameObject* object) -> bool
{
    assert(object);
    if (!object)
        return false;

    m_gameObjects.emplace_back(object);

    return true;
}

auto AppState::addGameObjects(std::vector<GameObject*> objects) -> bool
{
    assert(!objects.empty() && "Trying to pass an empy list of objects to be appended");
    if (objects.empty())
        return false;

    const auto oldSize = m_gameObjects.size();
    m_gameObjects.resize(oldSize + objects.size());
    for (u64 i { 0 }; i < objects.size(); ++i) {
        m_gameObjects[i + oldSize].reset(objects[i]);
    }

    return true;
}

GameObject* AppState::getObjectByIdentifier(std::string_view identifier)
{
    auto findResult = std::ranges::find(m_gameObjects, identifier, &GameObject::getIdentifier);
    if (findResult == m_gameObjects.end())
        return nullptr;

    return findResult->get();
}

auto AppState::assembleDrawableObjectList() -> std::vector<Renderable*>
{
    std::vector<Renderable*> output;

    for (auto& go : m_gameObjects) {
        if (!go->isActive())
            continue;

        auto renderable = go->getComponentsByType<Renderable>();

        if (renderable.empty())
            continue;

        for (auto& ptr : renderable)
            output.emplace_back(ptr);
    }

    return output;
}
} // nce namespace
