#include "nce/TimerController.hpp"
#include "nce/NCE.hpp"

#include <spdlog/spdlog.h>

namespace nce {
constexpr auto MAX_TIMERS { 100 };
TimerController::TimerController()
    : allocator(sizeof(Timer), MAX_TIMERS)
{ // Initialize the allocator with block size and count
}

TimerController::~TimerController()
{
    for (auto& kv : m_timers) {
        allocator.deallocate(kv.second, sizeof(Timer));
    }
}

void TimerController::addTimer(std::string_view name, sf::Time interval, TimerType type, std::function<void()> callback, bool startPaused)
{
    void* mem = allocator.allocate(sizeof(Timer));
    if (mem == nullptr) {
        spdlog::warn("Failed to allocate memory for timer {}", name);
        return;
    }

    Timer* timer = NCE_PLACEMENT_NEW(mem, Timer)(interval, sf::Time::Zero, startPaused, type, std::move(callback));
    m_timers[name.data()] = timer;
}

void TimerController::update(const sf::Time& dt)
{
    for (auto it = m_timers.begin(); it != m_timers.end();) {
        Timer* timer = it->second;

        if (timer->isPaused) {
            ++it;
            continue;
        }

        timer->elapsed += dt;
        if (timer->elapsed >= timer->interval) {
            executeCallback(it->first);
            if (timer->type == TimerType::SingleShot) {
                allocator.deallocate(timer, sizeof(Timer)); // Deallocate memory
                it = m_timers.erase(it); // Remove single-shot timer after execution
            } else {
                timer->elapsed = sf::Time::Zero;
                ++it;
            }
        } else {
            ++it;
        }
    }
}

void TimerController::resetTimer(std::string_view name, bool pauseOnReset)
{
    auto it = m_timers.find(name.data());
    if (it == m_timers.end()) {
        spdlog::warn("Unable to reset timer {} that wasn't found in TimerController", name);
        return;
    }

    it->second->elapsed = sf::Time::Zero;
    if (pauseOnReset) {
        it->second->isPaused = true;
    }
}

void TimerController::pauseTimer(std::string_view name)
{
    auto it = m_timers.find(name.data());
    if (it == m_timers.end()) {
        spdlog::warn("Unable to pause timer {} that wasn't found in TimerController", name);
        return;
    }
    it->second->isPaused = true;
}

void TimerController::playTimer(std::string_view name)
{
    auto it = m_timers.find(name.data());
    if (it == m_timers.end()) {
        return;
    }

    it->second->isPaused = false;
}

void TimerController::removeTimer(std::string_view name)
{
    auto it = m_timers.find(name.data());
    if (it == m_timers.end()) {
        spdlog::warn("Unable to remove timer {} that wasn't found in TimerController", name);
        return;
    }

    allocator.deallocate(it->second, sizeof(Timer));
    m_timers.erase(it);
}

auto TimerController::getElapsed(std::string_view name) -> sf::Time
{
    auto it = m_timers.find(name.data());
    if (it == m_timers.end()) {
        spdlog::warn("Unable to retrieve elapsed for timer {} in TimerController", name);
        return sf::Time::Zero;
    }

    return it->second->elapsed;
}

auto TimerController::isPaused(std::string_view name) -> bool
{
    auto it = m_timers.find(name.data());
    if (it == m_timers.end()) {
        spdlog::warn("Unable to pause timer {} in TimerController", name);
        return false;
    }

    return it->second->isPaused;
}

auto TimerController::exists(std::string_view name) -> bool { return m_timers.find(name.data()) != m_timers.end(); }

auto TimerController::getKeys() -> std::vector<std::string>
{
    std::vector<std::string> keys;
    for (const auto& kv : m_timers) {
        keys.push_back(kv.first);
    }

    return keys;
}

void TimerController::executeCallback(const std::string& name)
{
    auto it = m_timers.find(name);
    if (it != m_timers.end() && it->second->callback) {
        it->second->callback();
    }
}

} // nce namespace
