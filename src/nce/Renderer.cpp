#include "../stdafx.h"

#include "nce/Renderer.hpp"

#include "nce/App.hpp"
#include "nce/Components/Renderable.hpp"

#include <imgui-SFML.h>

namespace nce {

void Renderer::update()
{
    // We shouldn't assemble a render queue during loading
    if (NCE_APP().isLoading())
        return;

    // for (auto& obj : m_gameObjects) {
    //     if (!obj->isActive())
    //         continue;
    //     if (!m_isPaused) {
    //         obj->update(dt);
    //     }
    //     m_renderQueueObjects.push_back(obj.get());
    // }
    m_renderQueueObjects = NCE_APP().getActiveState()->assembleDrawableObjectList();

    std::ranges::stable_sort(m_renderQueueObjects, [](const Renderable* const a, const Renderable* const b) -> bool {
        assert(a);
        assert(b);
        return a->getLayer() < b->getLayer();
    });
}

void Renderer::render()
{
    auto renderTexture = NCE_APP().getRenderTexture();
    auto renderWindow = NCE_APP().getRenderWindow();

    renderTexture->clear(m_clearColour);
    if (!NCE_APP().isLoading()) {
        for (auto& renderable : m_renderQueueObjects) {
            renderTexture->draw(*renderable);
        }

        // We want to copy this because we want
        // to retain the view state prior to setting
        // default so we can re-apply it.

        // (TODO: Readd the below logic when doing HUD elements
        // logic again)
        // const auto cachedView = renderTexture->getView();
        // renderTexture->setView(renderTexture->getDefaultView());
        // for (const auto& element : m_renderQueueHUD) {
        //     assert(element);
        //     renderTexture->draw(*element);
        // }
        // renderTexture->setView(cachedView);
    }
    renderTexture->display();

    if (m_postProcessingShader) {
        const auto& t = renderTexture->getTexture();
        // Cache the current view
        const auto v = renderTexture->getView();
        renderTexture->setView(renderTexture->getDefaultView());
        const sf::Sprite postProcSprite(t);

        renderTexture->clear();

        renderTexture->draw(postProcSprite, m_postProcessingShader);

        renderTexture->display();

        // Restore original view
        renderTexture->setView(v);
    }

    const sf::Sprite temp(renderTexture->getTexture());
    renderWindow->clear();
    renderWindow->draw(temp);
    ImGui::SFML::Render(*renderWindow);
    if (NCE_APP().isLoading())
        renderWindow->draw(m_loadingSplash);
    renderWindow->display();
}
} // nce namespace
