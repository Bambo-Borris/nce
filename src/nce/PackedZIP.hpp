#pragma once

#include "nce/NCE.hpp"

#include <format>
#include <nce/AssetsInfo.hpp>
#include <tl/expected.hpp>
#include <zip.h>

#define ZIP_ERROR_TO_STRING(errNum) std::format("Unable to open zip!\nReason: {}", zip_strerror(errNum))
#define ZIP_ENTRY_ERROR_TO_STRING(errNum) std::format("Unable to open zip entry!\nReason: {}", zip_strerror(errNum))

namespace nce {
inline tl::expected<std::vector<std::byte>, std::string> GetDataFromPackedZIP(const AssetsInfo& info, std::string_view id, AssetType type)
{
    i32 errNum;
    auto zip = zip_openwitherror("bin/packed.dat", 6, 'r', &errNum);

    if (errNum != 0)
        return ErrVal(ZIP_ERROR_TO_STRING(errNum));

    std::vector<std::byte> outputBuffer;
    std::string path;

    switch (type) {
    case AssetType::Font: {
        auto findResult = std::ranges::find(info.fonts, id, &AssetEntry::id);
        if (findResult == info.fonts.end())
            return ErrVal(std::format("No font found in asset info list with id {}", id));

        path = findResult->path;
    } break;
    case AssetType::Texture: {
        auto findResult = std::ranges::find(info.textures, id, &AssetEntry::id);
        if (findResult == info.textures.end())
            return ErrVal(std::format("No texture found in asset info list with id {}", id));
        path = findResult->path;
    } break;
    case AssetType::Shader: {
        auto findResult = std::ranges::find(info.shaders, id, &AssetEntry::id);
        if (findResult == info.shaders.end())
            return ErrVal(std::format("No shader found in asset info list with id {}", id));
        path = findResult->path;
    } break;
    case AssetType::Sound: {
        auto findResult = std::ranges::find(info.sounds, id, &AssetEntry::id);
        if (findResult == info.sounds.end())
            return ErrVal(std::format("No sound found in asset info list with id {}", id));
        path = findResult->path;
    } break;
    }

    errNum = zip_entry_open(zip, path.data());
    if (errNum != 0)
        return ErrVal(std::format("Unable to load asset ID {}, zip error {}", id, ZIP_ENTRY_ERROR_TO_STRING(errNum)));

    void* buf = nullptr;
    size_t bufsize;

    errNum = static_cast<i32>(zip_entry_read(zip, &buf, &bufsize));
    if (errNum < 0)
        return ErrVal(std::format("Unable to load asset ID {}, zip error {}", id, ZIP_ENTRY_ERROR_TO_STRING(errNum)));

    outputBuffer.resize(bufsize);
    memcpy(outputBuffer.data(), buf, bufsize);
    free(buf);
    zip_close(zip);

    return outputBuffer;
}
} // nce namespace