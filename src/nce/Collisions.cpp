#include "../stdafx.h"
#include "nce/NCE.hpp"
// #define CUTE_C2_IMPLEMENTATION
// #pragma warning(push, 0)
// #pragma clang diagnostic push
// #pragma clang diagnostic ignored "-Wunused-function"
#include "nce/Collisions.hpp"
// #pragma clang diagnostic pop
// #pragma warning(pop)

namespace nce {

struct Projection {
    float minimum { 0.f };
    float maximum { 0.f };
};

struct Orientation {

    /*float m[2][2];
    float v[4];*/

    Orientation(sf::Angle angle)
    {
        m00 = std::cos(angle.asRadians());
        m01 = -std::sin(angle.asRadians());
        m10 = std::sin(angle.asRadians());
        m11 = std::cos(angle.asRadians());
    }
    Orientation(float mat00, float mat01, float mat10, float mat11)
        : m00(mat00)
        , m01(mat01)
        , m10(mat10)
        , m11(mat11)
    {
    }

    float m00, m01;
    float m10, m11;
    Orientation transposed() { return Orientation(m00, m10, m01, m11); }
    sf::Vector2f operator*(const sf::Vector2f& rhs) const { return { m00 * rhs.x + m01 * rhs.y, m10 * rhs.x + m11 * rhs.y }; }
};

Projection projectVertices(const std::array<sf::Vector2f, 4>& vertices, const sf::Vector2f& normal)
{
    Projection p { std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity() };

    for (std::size_t i { 0 }; i < vertices.size(); ++i) {
        const auto dp { vertices[i].dot(normal) };
        p.minimum = std::min(p.minimum, dp);
        p.maximum = std::max(p.maximum, dp);
    }
    return p;
}

constexpr std::array<sf::Vector2f, 4> ComputeOBBFromAABB(const sf::FloatRect& rect)
{
    return {
        sf::Vector2f { rect.left, rect.top },
        sf::Vector2f { rect.left + rect.width, rect.top },
        sf::Vector2f { rect.left + rect.width, rect.top + rect.height },
        sf::Vector2f { rect.left, rect.top + rect.height },
    };
}

inline std::array<sf::Vector2f, 4> ComputeOBBNormals(const sf::Vector2f size, const sf::Transform& transform, sf::Vector2f origin)
{
    // Note this is a recompute for any function that has already called this,
    // when we move to caching points/normals this will be more sensible
    const auto points { ComputeOBBVerticesTransformed(size, transform, origin) };
    return {
        (points[1] - points[0]).normalized().rotatedBy(sf::degrees(-90.f)),
        (points[2] - points[1]).normalized().rotatedBy(sf::degrees(-90.f)),
        (points[3] - points[2]).normalized().rotatedBy(sf::degrees(-90.f)),
        (points[0] - points[3]).normalized().rotatedBy(sf::degrees(-90.f)),

    };
}

inline std::array<sf::Vector2f, 4> ComputeOBBNormals(const std::array<sf::Vector2f, 4>& points)
{
    // clang-format off
    std::array<sf::Vector2f, 4> out {
        (points[1] - points[0]).normalized(),
        (points[2] - points[1]).normalized(),
        (points[3] - points[2]).normalized(),
        (points[0] - points[3]).normalized()
    };
    // clang-format on

    out[0] = { out[0].y, -out[0].x };
    out[1] = { out[1].y, -out[1].x };
    out[2] = { out[2].y, -out[2].x };
    out[3] = { out[3].y, -out[3].x };
    return out;
}

bool operator==(const CircleMetadata& lhs, const CircleMetadata& rhs) { return lhs.radius == rhs.radius; }

bool operator==(const AABBMetadata& lhs, const AABBMetadata& rhs) { return lhs.bounds == rhs.bounds; }

bool operator==(const OBBMetadata& lhs, const OBBMetadata& rhs) { return lhs.halfBounds == rhs.halfBounds; }

bool operator==(const ColliderInfo& rhs, const ColliderInfo& lhs)
{
    if (lhs.type != rhs.type)
        return false;

    if (lhs.transformable != rhs.transformable)
        return false;

    if (lhs.go != rhs.go)
        return false;

    // Check c2 colliders match
    bool metaDataMatch { false };
    bool c2ColliderMatch { false };

    switch (lhs.type) {
    case ColliderType::Circle:
        metaDataMatch = std::get<CircleMetadata>(lhs.metadata) == std::get<CircleMetadata>(rhs.metadata);
        break;
    case ColliderType::AABB:
        metaDataMatch = std::get<AABBMetadata>(lhs.metadata) == std::get<AABBMetadata>(rhs.metadata);
        break;
    case ColliderType::OBB:
        metaDataMatch = std::get<OBBMetadata>(lhs.metadata) == std::get<OBBMetadata>(rhs.metadata);
        break;
    case ColliderType::Poly: // TODO: implement
    default:
        assert(false);
    }

    return metaDataMatch && c2ColliderMatch;
}

auto MakeCircleCollider(float radius, sf::Transformable* transformable, GameObject* go, i32 mask) -> ColliderInfo
{
    ColliderInfo out {};
    out.type = ColliderType::Circle;
    out.transformable = transformable;
    out.go = go;
    out.metadata = CircleMetadata { radius };
    out.mask = mask;

    return out;
}

auto MakeAABBCollider(sf::Vector2f halfBounds, sf::Transformable* transformable, GameObject* go, i32 mask) -> ColliderInfo
{
    ColliderInfo out {};
    out.type = ColliderType::AABB;
    out.transformable = transformable;
    out.go = go;
    out.metadata = AABBMetadata { sf::FloatRect { { 0.f, 0.f }, halfBounds * 2.f } };
    out.mask = mask;
    return out;
}

auto MakeOBBCollider(sf::Vector2f halfBounds, sf::Transformable* transformable, GameObject* go, i32 mask) -> ColliderInfo
{
    ColliderInfo out {};
    out.type = ColliderType::OBB;
    out.transformable = transformable;
    out.go = go;
    out.metadata = OBBMetadata {};
    std::get<OBBMetadata>(out.metadata).halfBounds = halfBounds;
    out.mask = mask;
    return out;
}

auto MakePolyCollider(const std::vector<sf::Vector2f>& points, sf::Transformable* transformable, GameObject* go, i32 mask) -> ColliderInfo
{
    assert(!points.empty());
    ColliderInfo out {};

    out.type = ColliderType::Poly;
    out.transformable = transformable;
    out.go = go;

    out.metadata = PolyMetadata {};
    auto& metadata = std::get<PolyMetadata>(out.metadata);

    metadata.points = points;

    // c2Poly polyColliderC2;
    // polyColliderC2.count = int32_t(points.size());

    // for (size_t i { 0 }; i < points.size(); ++i)
    //     polyColliderC2.verts[i] = Vector2fToc2v(points[i]);

    // out->c2Collider = polyColliderC2;
    out.mask = mask;
    return out;
}

std::optional<Manifold> CircleVsCircle(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    // We really shouldn't reach this, there should be a check external to this
    // to check the colliders we want to use match this
    assert(colliderA.type == ColliderType::Circle && colliderB.type == ColliderType::Circle);
    if (colliderA.type != ColliderType::Circle || colliderB.type != ColliderType::Circle)
        return std::nullopt;

    assert(colliderA.transformable);
    assert(colliderB.transformable);

    const auto direction = colliderB.transformable->getPosition() - colliderA.transformable->getPosition();

    // We assume a uniform scale on circle collided objects. If not uniform then the radius is only scaled by the
    // x. This is an assumption that's safe for One Hit Wonder.
    const auto& radiusA = std::get<CircleMetadata>(colliderA.metadata).radius * colliderA.transformable->getScale().x;
    const auto& radiusB = std::get<CircleMetadata>(colliderB.metadata).radius * colliderB.transformable->getScale().x;

    const auto radiiSumSquared = std::pow(radiusA + radiusB, 2.f);
    if (direction.lengthSq() > radiiSumSquared) {
        return {};
    }

    const auto distance = direction.length();
    Manifold m;

    if (distance != 0.f) {
        m.penetration = (radiusA + radiusB) - distance;
        m.normal = -direction / distance;
    } else {
        m.penetration = radiusA;
        m.normal = sf::Vector2f(1.f, 0.f);
    }
    return { m };
}

std::optional<Manifold> OBBVsOBB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    // This is obviously stupid to constantly compute
    // on every check of the OBBVsOBB so...
    // TODO We need to iterate over collider behaviour again
    // so that we can implement a system to cache the local vertices
    // of the OBB, then only transform it when we need to check
    // collisions
    // TODO also we should really support containment
    // read this for more information : https://dyn4j.org/2010/01/sat/

    assert(colliderA.type == ColliderType::OBB && colliderB.type == ColliderType::OBB);
    if (colliderA.type != ColliderType::OBB && colliderB.type != ColliderType::OBB)
        return std::nullopt;

    constexpr auto doProjectionsOverlap = [](const Projection& p1, const Projection& p2) -> bool {
        return !(p1.minimum > p2.maximum || p2.minimum > p1.maximum); //{ p1.maximum >= p2.minimum && p2.maximum >= p1.minimum };
    };

    constexpr auto getOverlap = [](const Projection& p1, const Projection& p2) { return std::min(p2.maximum - p1.minimum, p1.maximum - p2.minimum); };

    const auto& halfSizeA = std::get<OBBMetadata>(colliderA.metadata).halfBounds;
    const auto& halfSizeB = std::get<OBBMetadata>(colliderB.metadata).halfBounds;

    const auto verticesA = ComputeOBBVerticesTransformed(halfSizeA * 2.f, colliderA.transformable->getTransform(), colliderA.transformable->getOrigin());
    const auto verticesB = ComputeOBBVerticesTransformed(halfSizeB * 2.f, colliderB.transformable->getTransform(), colliderB.transformable->getOrigin());

    // we'll mark these as static
    const auto normalsA = ComputeOBBNormals(verticesA);
    const auto normalsB = ComputeOBBNormals(verticesB);

    Manifold m;
    m.penetration = std::numeric_limits<float>::max();

    // Iterate over shape A
    for (std::size_t i = 0; i < normalsA.size(); ++i) {
        const auto p1 = projectVertices(verticesA, normalsA[i]);
        const auto p2 = projectVertices(verticesB, normalsA[i]);
        if (!doProjectionsOverlap(p1, p2)) {
            return std::nullopt;
        } else {
            const auto o = getOverlap(p1, p2);
            if (o < m.penetration) {
                m.penetration = o;
                m.normal = normalsA[i];
            }
        }
    }

    // Now iterate over shape B
    for (std::size_t i = 0; i < normalsB.size(); ++i) {
        const auto p1 = projectVertices(verticesA, normalsB[i]);
        const auto p2 = projectVertices(verticesB, normalsB[i]);
        if (!doProjectionsOverlap(p1, p2)) {
            return std::nullopt;
        } else {
            const auto o = getOverlap(p1, p2);
            if (o < m.penetration) {
                m.penetration = o;
                m.normal = normalsB[i];
            }
        }
    }

    const auto& shape1Position { colliderA.transformable->getPosition() };
    const auto& shape2Position { colliderB.transformable->getPosition() };

    const auto direction { shape2Position - shape1Position };

    if (direction.dot(m.normal) > 0.f) {
        m.normal = -m.normal;
    }

    return { m };
}

std::optional<Manifold> CircleVsAABB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    assert(colliderA.type == ColliderType::Circle && colliderB.type == ColliderType::AABB);
    if (colliderA.type != ColliderType::Circle && colliderB.type != ColliderType::AABB)
        return std::nullopt;

    const auto result = AABBVsCircle(colliderB, colliderA);
    if (!result)
        return {};

    return { { -result->normal, result->penetration } };
}

std::optional<Manifold> CircleVsOBB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    assert(colliderA.type == ColliderType::Circle && colliderB.type == ColliderType::OBB);
    if (colliderA.type != ColliderType::Circle && colliderB.type != ColliderType::OBB)
        return std::nullopt;

    const auto& halfBounds { std::get<OBBMetadata>(colliderB.metadata).halfBounds };

    // Transform circle centre to Polygon model space
    auto centre = colliderA.transformable->getPosition();
    Orientation o(colliderB.transformable->getRotation());

    centre = o.transposed() * (centre - colliderB.transformable->getPosition());

    auto vertices { ComputeOBBVertices(2.f * halfBounds, colliderB.transformable->getOrigin()) };
    for (auto& v : vertices)
        v -= colliderB.transformable->getOrigin();
    const auto normals { ComputeOBBNormals(vertices) };
    const auto& radius { std::get<CircleMetadata>(colliderA.metadata).radius * colliderA.transformable->getScale().x };

    // Find edge with minimum penetration
    // Exact concept as using support points in Polygon vs Polygon
    auto separation = -std::numeric_limits<float>::max();
    size_t faceNormal = 0;

    for (size_t i = 0; i < 4; ++i) {
        auto s { normals[i].dot((centre - vertices[i])) };
        if (s > radius)
            return std::nullopt;

        if (s > separation) {
            separation = s;
            faceNormal = i;
        }
    }

    // Grab face's vertices
    auto v1 = vertices[faceNormal];
    size_t i2 = faceNormal + 1 < vertices.size() ? faceNormal + 1 : 0;
    auto v2 = vertices[i2];
    std::optional<Manifold> m;

    // Check to see if center is within polygon
    if (separation < EPSILON) {
        m.emplace();
        m->normal = o * normals[faceNormal];
        m->penetration = radius;
        return m;
    }

    // Determine which voronoi region of the edge center of circle lies within
    auto dot1 = (centre - v1).dot(v2 - v1);
    auto dot2 = (centre - v2).dot(v1 - v2);

    m.emplace();
    m->penetration = radius - separation;

    // Closest to v1
    if (dot1 <= 0.0f) {
        if ((v1 - centre).lengthSq() > (radius * radius))
            return std::nullopt;

        auto n = v1 - centre;
        n = -(o * n);
        m->normal = n.normalized();
    }

    // Closest to v2
    else if (dot2 <= 0.0f) {
        if ((centre - v2).lengthSq() > (radius * radius))
            return std::nullopt;

        auto n = v2 - centre;
        n = -(o * n);
        m->normal = n.normalized();
    }
    // Closest to face
    else {
        auto n = normals[faceNormal];
        if ((centre - v1).dot(n) > radius)
            return std::nullopt;

        n = -(o * n);
        m->normal = -n;
    }

    return m;
}

std::optional<Manifold> CircleVsPoly(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    NCE_UNUSED(colliderA);
    NCE_UNUSED(colliderB);
    return {};
}

std::optional<Manifold> AABBVsCircle(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    std::optional<Manifold> m;

    // Setup a couple pointers to each object
    assert(colliderA.type == ColliderType::AABB && colliderB.type == ColliderType::Circle);
    if (colliderA.type != ColliderType::AABB && colliderB.type != ColliderType::Circle)
        return std::nullopt;

    // Vector from A to B
    auto n { colliderB.transformable->getPosition() - colliderA.transformable->getPosition() };

    // Closest point on A to center of B
    auto closest = n;

    // Calculate half extents along each axis
    const auto aabbMetadataRect { std::get<AABBMetadata>(colliderA.metadata).bounds };
    const auto transformedAABB { colliderA.transformable->getTransform().transformRect(aabbMetadataRect) };
    const auto x_extent = transformedAABB.getSize().x / 2.f;
    const auto y_extent = transformedAABB.getSize().y / 2.f;

    // Clamp point to edges of the AABB
    closest.x = std::clamp(closest.x, -x_extent, x_extent);
    closest.y = std::clamp(closest.y, -y_extent, y_extent);

    bool inside = false;

    // Circle is inside the AABB, so we need to clamp the circle's center
    // to the closest edge
    if (n == closest) {
        inside = true;

        // Find closest axis
        if (std::abs(n.x) > std::abs(n.y)) {
            // Clamp to closest extent
            if (closest.x > 0)
                closest.x = x_extent;
            else
                closest.x = -x_extent;
        }
        // y axis is shorter
        else {
            // Clamp to closest extent
            if (closest.y > 0)
                closest.y = y_extent;
            else
                closest.y = -y_extent;
        }
    }

    auto normal { n - closest };
    auto d = normal.lengthSq();
    auto r = std::get<CircleMetadata>(colliderB.metadata).radius;

    // Early out of the radius is shorter than distance to closest point and
    // Circle not inside the AABB
    if (d > r * r && !inside)
        return {};

    // Avoided sqrt until we needed
    d = std::sqrt(d);

    // Collision normal needs to be flipped to point outside if circle was
    // inside the AABB
    m.emplace();
    if (inside) {
        m->normal = n;
        m->penetration = r - d;
    } else {
        m->normal = -n;
        m->penetration = r - d;
    }

    if (m->normal != sf::Vector2f {})
        m->normal = m->normal.normalized();
    else
        sf::Vector2f { 1.f, 0.f }; // This is just what? Idk, it fixes it tho.

    return m;
}

std::optional<Manifold> AABBVsAABB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    // We really shouldn't reach this, there should be a check external to this
    // to check the colliders we want to use match this
    assert(colliderA.type == ColliderType::AABB || colliderB.type == ColliderType::AABB);
    if (colliderA.type != ColliderType::AABB || colliderB.type != ColliderType::AABB)
        return std::nullopt;

    assert(colliderA.transformable);
    assert(colliderB.transformable);

    const auto direction { colliderB.transformable->getPosition() - colliderA.transformable->getPosition() };

    const auto updatedBoundsA { colliderA.transformable->getTransform().transformRect(std::get<AABBMetadata>(colliderA.metadata).bounds) };
    const auto updatedBoundsB { colliderB.transformable->getTransform().transformRect(std::get<AABBMetadata>(colliderB.metadata).bounds) };

    const auto halfSizeA { updatedBoundsA.getSize() * 0.5f };
    const auto halfSizeB { updatedBoundsB.getSize() * 0.5f };

    const auto xOverlap = (halfSizeA.x + halfSizeB.x) - std::abs(direction.x);

    if (xOverlap > 0.f) {
        const auto yOverlap = (halfSizeA.y + halfSizeB.y) - std::abs(direction.y);

        if (yOverlap > 0) {
            if (xOverlap < yOverlap) {
                Manifold m;

                if (direction.x > 0.f)
                    m.normal = sf::Vector2f { -1.f, 0.f };
                else
                    m.normal = sf::Vector2f { 1.f, 0.f };

                m.penetration = xOverlap;
                return { m };
            } else {
                Manifold m;
                if (direction.y > 0.f)
                    m.normal = sf::Vector2f(0.f, -1.f);
                else
                    m.normal = sf::Vector2f(0.f, 1.0f);

                m.penetration = yOverlap;
                return { m };
            }
        }
    }

    return {};
}

std::optional<Manifold> AABBVsOBB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    assert(colliderA.type == ColliderType::AABB && colliderB.type == ColliderType::OBB);
    if (colliderA.type != ColliderType::AABB && colliderB.type != ColliderType::OBB)
        return std::nullopt;

    constexpr auto doProjectionsOverlap = [](const Projection& p1, const Projection& p2) -> bool {
        return !(p1.minimum > p2.maximum || p2.minimum > p1.maximum); //{ p1.maximum >= p2.minimum && p2.maximum >= p1.minimum };
    };

    constexpr auto getOverlap = [](const Projection& p1, const Projection& p2) { return std::min(p2.maximum - p1.minimum, p1.maximum - p2.minimum); };

    const auto& boundsA = std::get<AABBMetadata>(colliderA.metadata).bounds;
    const auto& halfSizeB = std::get<OBBMetadata>(colliderB.metadata).halfBounds;

    const auto verticesA = ComputeOBBFromAABB(colliderA.transformable->getTransform().transformRect(boundsA));
    const auto verticesB = ComputeOBBVerticesTransformed(halfSizeB * 2.f, colliderB.transformable->getTransform(), colliderB.transformable->getOrigin());

    // we'll mark these as static
    const auto normalsA = ComputeOBBNormals(verticesA);
    const auto normalsB = ComputeOBBNormals(verticesB);

    Manifold m;
    m.penetration = std::numeric_limits<float>::max();

    // Iterate over shape A
    for (std::size_t i = 0; i < normalsA.size(); ++i) {
        const auto p1 = projectVertices(verticesA, normalsA[i]);
        const auto p2 = projectVertices(verticesB, normalsA[i]);
        if (!doProjectionsOverlap(p1, p2)) {
            return std::nullopt;
        } else {
            const auto o = getOverlap(p1, p2);
            if (o < m.penetration) {
                m.penetration = o;
                m.normal = normalsA[i];
            }
        }
    }

    // Now iterate over shape B
    for (std::size_t i = 0; i < normalsB.size(); ++i) {
        const auto p1 = projectVertices(verticesA, normalsB[i]);
        const auto p2 = projectVertices(verticesB, normalsB[i]);
        if (!doProjectionsOverlap(p1, p2)) {
            return std::nullopt;
        } else {
            const auto o = getOverlap(p1, p2);
            if (o < m.penetration) {
                m.penetration = o;
                m.normal = normalsB[i];
            }
        }
    }

    const auto& shape1Position { colliderA.transformable->getPosition() };
    const auto& shape2Position { colliderB.transformable->getPosition() };

    const auto direction { shape2Position - shape1Position };

    if (direction.dot(m.normal) > 0.f) {
        m.normal = -m.normal;
    }

    return { m };
}

std::optional<Manifold> AABBVsPoly(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    NCE_UNUSED(colliderA);
    NCE_UNUSED(colliderB);
    return {};
}

std::optional<Manifold> OBBVsCircle(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    assert(colliderA.type == ColliderType::OBB && colliderB.type == ColliderType::Circle);
    if (colliderA.type != ColliderType::OBB && colliderB.type != ColliderType::Circle)
        return std::nullopt;

    auto m = CircleVsOBB(colliderB, colliderA);
    if (!m)
        return {};
    return { { -m->normal, m->penetration } };
}

std::optional<Manifold> OBBVsAABB(const ColliderInfo& colliderA, const ColliderInfo& colliderB) noexcept
{
    assert(colliderA.type == ColliderType::OBB && colliderB.type == ColliderType::AABB);
    if (colliderA.type != ColliderType::OBB && colliderB.type != ColliderType::AABB)
        return std::nullopt;

    auto m = AABBVsOBB(colliderB, colliderA);
    if (!m)
        return {};

    return { { -m->normal, m->penetration } };
}
} // nce namespace