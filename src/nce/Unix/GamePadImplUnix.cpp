#include "../../stdafx.h"

#include "../GamePadImpl.hpp"
#include "nce/NCE.hpp"

namespace nce {

enum DPADSential { UP = -10, RIGHT = -11, DOWN = -12, LEFT = -13 };

constexpr auto SONY_VENDOR_ID { 1356 };
constexpr auto MICROSOFT_VENDOR_ID { 1118 };
constexpr auto JOYSTICK_ID = 0;

// Values taken from XInput header
constexpr f32 LEFT_STICK_DEAD_ZONE { 23.f };
constexpr f32 RIGHT_STICK_DEAD_ZONE { 26.f };
constexpr f32 TRIGGER_DEAD_ZONE { 30 };

// DS4 PAD
constexpr std::array<i32, static_cast<size_t>(GamePad::Buttons::MAX)> DS4_SF_JOYSTICK_BUTTON_TO_GAMEPAD_BUTTON_MAPPING = {
    0, // A
    3, // X
    2, // Y
    1, // B
    9, // Start
    8, // Back
    4, // Left Shoulder
    5, // Right Shoulder
    6, // Left Trigger
    7, // Right Trigger
    DPADSential::UP, // DPAD Up
    DPADSential::RIGHT, // DPad Right
    DPADSential::DOWN, // DPad Down
    DPADSential::LEFT, // DPAD Left
    11, // Left Analogue Click
    12, // Right Analogue Click
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
};

// XBOX PAD
constexpr std::array<i32, static_cast<size_t>(GamePad::Buttons::MAX)> XBOX_SF_JOYSTICK_BUTTON_TO_GAMEPAD_BUTTON_MAPPING = {
    0, // A
    2, // X
    3, // Y
    1, // B
    7, // Start
    6, // Back
    4, // Left Shoulder
    5, // Right Shoulder
    -1, // Left Trigger (Unsupported as buttons, emulated as buttons on Linux)
    -1, // Right Trigger (Unsupported as buttons, emulated as buttons on Linux)
    DPADSential::UP, // DPAD Up
    DPADSential::RIGHT, // DPad Right
    DPADSential::DOWN, // DPad Down
    DPADSential::LEFT, // DPAD Left
    9, // Left Analogue Click
    10, // Right Analogue Click
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
    -1, // Unsupported on Linux
};

constexpr auto SFAxisToGamePadAxis(sf::Joystick::Axis axis, bool isSony) -> std::optional<GamePad::Axis>
{
    if (!isSony) {
        switch (axis) {
        case sf::Joystick::Axis::X:
        case sf::Joystick::Axis::Y:
            return GamePad::Axis::LeftStick;
            break;

        case sf::Joystick::Axis::U:
        case sf::Joystick::Axis::V:
            return GamePad::Axis::RightStick;
            break;

        case sf::Joystick::Axis::Z:
            return GamePad::Axis::LeftTrigger;
            break;
        case sf::Joystick::Axis::R:
            return GamePad::Axis::RightTrigger;
            break;
        default:;
        }
    } else {
        switch (axis) {
        case sf::Joystick::Axis::X:
        case sf::Joystick::Axis::Y:
            return GamePad::Axis::LeftStick;
            break;

        case sf::Joystick::Axis::U:
        case sf::Joystick::Axis::V:
            return GamePad::Axis::RightStick;
            break;

        case sf::Joystick::Axis::Z:
            return GamePad::Axis::LeftTrigger;
            break;
        case sf::Joystick::Axis::R:
            return GamePad::Axis::RightTrigger;
            break;
        default:;
        }
    }
    return std::nullopt;
}

[[nodiscard]] auto GamePadAxisToSFAxisList(GamePad::Axis axis, bool isSony) -> std::vector<sf::Joystick::Axis>
{
    std::vector<sf::Joystick::Axis> axisList;
    if (!isSony) {
        switch (axis) {
        case GamePad::Axis::LeftStick:
            axisList.emplace_back(sf::Joystick::Axis::X);
            axisList.emplace_back(sf::Joystick::Axis::Y);
            break;
        case GamePad::Axis::RightStick:
            axisList.emplace_back(sf::Joystick::Axis::U);
            axisList.emplace_back(sf::Joystick::Axis::V);
            break;
        case GamePad::Axis::LeftTrigger:
            axisList.emplace_back(sf::Joystick::Axis::Z);
            break;
        case GamePad::Axis::RightTrigger:
            axisList.emplace_back(sf::Joystick::Axis::R);
            break;
        default:;
        }
    } else {
        switch (axis) {
        case GamePad::Axis::LeftStick:
            axisList.emplace_back(sf::Joystick::Axis::X);
            axisList.emplace_back(sf::Joystick::Axis::Y);
            break;
        case GamePad::Axis::RightStick:
            axisList.emplace_back(sf::Joystick::Axis::U);
            axisList.emplace_back(sf::Joystick::Axis::V);
            break;
        case GamePad::Axis::LeftTrigger:
            axisList.emplace_back(sf::Joystick::Axis::Z);
            break;
        case GamePad::Axis::RightTrigger:
            axisList.emplace_back(sf::Joystick::Axis::R);
            break;
        default:;
        }
    }

    return axisList;
}

[[nodiscard]] constexpr std::optional<GamePad::Buttons> SFMLButtonIDToGamePadButton(i32 buttonID, bool isSony)
{
    const int* converted { nullptr };
    if (isSony) {
        converted = std::ranges::find(DS4_SF_JOYSTICK_BUTTON_TO_GAMEPAD_BUTTON_MAPPING, buttonID);
        if (converted == DS4_SF_JOYSTICK_BUTTON_TO_GAMEPAD_BUTTON_MAPPING.end()) {
            return std::nullopt;
        }
    } else {
        converted = std::ranges::find(XBOX_SF_JOYSTICK_BUTTON_TO_GAMEPAD_BUTTON_MAPPING, buttonID);
        if (converted == XBOX_SF_JOYSTICK_BUTTON_TO_GAMEPAD_BUTTON_MAPPING.end()) {
            return std::nullopt;
        }
    }

    // -1 indicates unsupported keys
    if (*converted == -1)
        return std::nullopt;

    GamePad::Buttons button;
    if (isSony) {
        button = static_cast<GamePad::Buttons>(std::distance(DS4_SF_JOYSTICK_BUTTON_TO_GAMEPAD_BUTTON_MAPPING.begin(), converted));
    } else {
        button = static_cast<GamePad::Buttons>(std::distance(XBOX_SF_JOYSTICK_BUTTON_TO_GAMEPAD_BUTTON_MAPPING.begin(), converted));
    }

    if (button == GamePad::Buttons::UnsupportedButton)
        return std::nullopt;

    return button;
}

void GamePadImpl::update()
{
    m_pressedButtons.clear();
    m_releasedButtons.clear();
    sf::Vector2f leftJoystick;
    const auto vendorID = sf::Joystick::getIdentification(JOYSTICK_ID).vendorId;
    const auto leftStickAxisList = GamePadAxisToSFAxisList(GamePad::Axis::LeftStick, vendorID == SONY_VENDOR_ID);
    leftJoystick.x = sf::Joystick::getAxisPosition(JOYSTICK_ID, leftStickAxisList[0]);
    leftJoystick.y = -sf::Joystick::getAxisPosition(JOYSTICK_ID, leftStickAxisList[1]);
    m_2dAxisValues[GamePad::Axis::LeftStick] = getStickAxis(leftJoystick, true);

    const auto rightStickAxisList = GamePadAxisToSFAxisList(GamePad::Axis::RightStick, vendorID == SONY_VENDOR_ID);
    leftJoystick.x = sf::Joystick::getAxisPosition(JOYSTICK_ID, rightStickAxisList[0]);
    leftJoystick.y = -sf::Joystick::getAxisPosition(JOYSTICK_ID, rightStickAxisList[1]);
    m_2dAxisValues[GamePad::Axis::RightStick] = getStickAxis(leftJoystick, false);
}

struct GamePadImpl::PrivData { };

GamePadImpl::GamePadImpl() { m_privData.reset(NCE_NEW PrivData); }

GamePadImpl::~GamePadImpl() = default;

auto GamePadImpl::buttonPressed(GamePad::Buttons button) const -> bool { return m_pressedButtons.contains(button); }

auto GamePadImpl::buttonReleased(GamePad::Buttons button) const -> bool { return m_releasedButtons.contains(button); }

auto GamePadImpl::buttonHeld(GamePad::Buttons button) const -> bool { return m_heldButtons.contains(button); }

auto GamePadImpl::get2DAxisInfo(GamePad::Axis axis) const -> GamePad::JoystickInfo
{
    assert(axis == GamePad::Axis::LeftStick || axis == GamePad::Axis::RightStick);
    assert(m_2dAxisValues.contains(axis));
    return m_2dAxisValues.at(axis);
}

auto GamePadImpl::get1DAxisInfo(GamePad::Axis axis) const -> f32
{
    assert(axis != GamePad::Axis::LeftStick && axis != GamePad::Axis::RightStick);
    assert(m_1dAxisValues.contains(axis));
    return m_1dAxisValues.at(axis);
}

auto GamePadImpl::hasValidConnectedDevice() const -> bool
{
    return sf::Joystick::isConnected(JOYSTICK_ID)
        && (sf::Joystick::getIdentification(JOYSTICK_ID).vendorId == SONY_VENDOR_ID
            || sf::Joystick::getIdentification(JOYSTICK_ID).vendorId == MICROSOFT_VENDOR_ID);
}

void GamePadImpl::handleEvents(sf::Event event)
{
    const auto vendorID = sf::Joystick::getIdentification(JOYSTICK_ID).vendorId;

    // We only support Xbox/PS controllers, ignore any other vendor
    if (vendorID != MICROSOFT_VENDOR_ID && vendorID != SONY_VENDOR_ID)
        return;

    if (auto joystickButtonPressed = event.getIf<sf::Event::JoystickButtonPressed>()) {
        if (joystickButtonPressed->joystickId == JOYSTICK_ID) {
            const auto gamePadButton = SFMLButtonIDToGamePadButton(i32(joystickButtonPressed->button), vendorID == SONY_VENDOR_ID);
            if (gamePadButton.has_value()) {
                m_pressedButtons.emplace(*gamePadButton);
                m_heldButtons.emplace(*gamePadButton);
            }
        }
    } else if (auto joystickButtonReleased = event.getIf<sf::Event::JoystickButtonReleased>()) {
        if (joystickButtonReleased->joystickId) {
            const auto gamePadButton = SFMLButtonIDToGamePadButton(i32(joystickButtonReleased->button), vendorID == SONY_VENDOR_ID);
            if (gamePadButton.has_value()) {
                m_releasedButtons.emplace(*gamePadButton);
                m_heldButtons.erase(*gamePadButton);
            }
        }
    } else if (auto joystickMoved = event.getIf<sf::Event::JoystickMoved>()) {
        if (joystickMoved->joystickId) {
            const auto axis = joystickMoved->axis;
            if (const auto gamePadAxis = SFAxisToGamePadAxis(axis, vendorID == SONY_VENDOR_ID)) {
                if (*gamePadAxis == GamePad::Axis::LeftTrigger || *gamePadAxis == GamePad::Axis::RightTrigger) {
                    m_1dAxisValues[*gamePadAxis] = getNormalizedTriggerAxis(joystickMoved->position);
                }
            }
        }
    }

    // On linux the dpad cannot be queried as a button, but can via XInput
    // so we need to emulate this behaviour
    static bool dPadUpPressed { false };
    static bool dPadRightPressed { false };
    static bool dPadDownPressed { false };
    static bool dPadLeftPressed { false };

    constexpr auto dPadHorizontalAxis = sf::Joystick::Axis::PovX;
    constexpr auto dPadVerticalAxis = sf::Joystick::Axis::PovY;

    const auto sfAxisXPosition = sf::Joystick::getAxisPosition(JOYSTICK_ID, dPadHorizontalAxis);
    const auto sfAxisYPosition = sf::Joystick::getAxisPosition(JOYSTICK_ID, dPadVerticalAxis);

    if (i32(sfAxisXPosition) == 100) {
        if (!dPadRightPressed) {
            dPadRightPressed = true;
            m_pressedButtons.emplace(GamePad::Buttons::DirectionRight);
            m_heldButtons.emplace(GamePad::Buttons::DirectionRight);
        }
    } else if (i32(sfAxisXPosition) == -100) {
        if (!dPadLeftPressed) {
            dPadLeftPressed = true;
            m_pressedButtons.emplace(GamePad::Buttons::DirectionLeft);
            m_heldButtons.emplace(GamePad::Buttons::DirectionLeft);
        }
    } else {
        if (dPadRightPressed) {
            dPadRightPressed = false;
            m_releasedButtons.emplace(GamePad::Buttons::DirectionRight);
            m_heldButtons.erase(GamePad::Buttons::DirectionRight);
        }

        if (dPadLeftPressed) {
            dPadLeftPressed = false;
            m_releasedButtons.emplace(GamePad::Buttons::DirectionLeft);
            m_heldButtons.erase(GamePad::Buttons::DirectionLeft);
        }
    }

    if (i32(sfAxisYPosition) == 100) {
        if (!dPadDownPressed) {
            dPadDownPressed = true;
            m_pressedButtons.emplace(GamePad::Buttons::DirectionDown);
            m_heldButtons.emplace(GamePad::Buttons::DirectionDown);
        }
    } else if (i32(sfAxisYPosition) == -100) {
        if (!dPadUpPressed) {
            dPadUpPressed = true;
            m_pressedButtons.emplace(GamePad::Buttons::DirectionUp);
            m_heldButtons.emplace(GamePad::Buttons::DirectionUp);
        }
    } else {
        if (dPadDownPressed) {
            dPadDownPressed = false;
            m_releasedButtons.emplace(GamePad::Buttons::DirectionDown);
            m_heldButtons.erase(GamePad::Buttons::DirectionDown);
        }

        if (dPadUpPressed) {
            dPadUpPressed = false;
            m_releasedButtons.emplace(GamePad::Buttons::DirectionUp);
            m_heldButtons.erase(GamePad::Buttons::DirectionUp);
        }
    }

    // On linux the triggers for an Xbox pad cannot be treated as a button
    // so this must be emulated to keep parity with Windows
    if (vendorID == MICROSOFT_VENDOR_ID) {
        static bool leftTriggerPressed = false;
        static bool rightTriggerPressed = false;
        const auto leftTriggerPos = sf::Joystick::getAxisPosition(JOYSTICK_ID, GamePadAxisToSFAxisList(GamePad::Axis::LeftTrigger, false)[0]);
        const auto rightTriggerPos = sf::Joystick::getAxisPosition(JOYSTICK_ID, GamePadAxisToSFAxisList(GamePad::Axis::RightTrigger, false)[0]);

        if (i32(std::round(leftTriggerPos)) == 100) {
            if (!leftTriggerPressed) {
                leftTriggerPressed = true;
                m_pressedButtons.emplace(GamePad::Buttons::LeftTrigger);
                m_heldButtons.emplace(GamePad::Buttons::LeftTrigger);
            }
        } else {
            if (leftTriggerPressed) {
                leftTriggerPressed = false;
                m_releasedButtons.emplace(GamePad::Buttons::LeftTrigger);
                m_heldButtons.erase(GamePad::Buttons::LeftTrigger);
            }
        }

        if (i32(std::round(rightTriggerPos)) == 100) {
            if (!rightTriggerPressed) {
                rightTriggerPressed = true;
                m_pressedButtons.emplace(GamePad::Buttons::RightTrigger);
                m_heldButtons.emplace(GamePad::Buttons::RightTrigger);
            }
        } else {
            if (rightTriggerPressed) {
                rightTriggerPressed = false;
                m_releasedButtons.emplace(GamePad::Buttons::RightTrigger);
                m_heldButtons.erase(GamePad::Buttons::RightTrigger);
            }
        }
    }
}

auto GamePadImpl::getNormalizedTriggerAxis(f32 raw) const -> f32
{
    auto out { 0.f };

    if (SONY_VENDOR_ID == sf::Joystick::getIdentification(JOYSTICK_ID).vendorId) {
        raw += 100.f;
        raw /= 100.f;
        raw *= 255.f;
    }

    if (raw > TRIGGER_DEAD_ZONE) {
        if (raw > 255)
            raw = 255.f;
        raw -= TRIGGER_DEAD_ZONE;
        out = raw / (255 - TRIGGER_DEAD_ZONE);
    }

    return out;
}

auto GamePadImpl::getStickAxis(const sf::Vector2f& rawAxis, bool isLeftStick) const -> GamePad::JoystickInfo
{
    const auto deadzone { isLeftStick ? LEFT_STICK_DEAD_ZONE : RIGHT_STICK_DEAD_ZONE };
    f32 normalizedMagnitude = 0;
    f32 magnitude = rawAxis.length();
    auto normalizedRawAxis = rawAxis;
    if (magnitude != 0.f) {
        normalizedRawAxis /= magnitude;
        if (magnitude > deadzone) {
            if (magnitude > 100.f)
                magnitude = 100.f;

            magnitude -= deadzone;
            normalizedMagnitude = magnitude / (100.f - deadzone);
        } else {
            magnitude = 0.f;
            normalizedMagnitude = 0.f;
        }
    }
    return { .direction = normalizedRawAxis, .magnitude = normalizedMagnitude };
}
} // nce namespace
