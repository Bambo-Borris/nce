#pragma once

#include <nce/GamePad.hpp>

namespace nce {
class GamePadImpl {
public:
    GamePadImpl();
    ~GamePadImpl();

    void update();
    void handleEvents(sf::Event event);
    [[nodiscard]] auto buttonPressed(GamePad::Buttons button) const -> bool;
    [[nodiscard]] auto buttonReleased(GamePad::Buttons button) const -> bool;
    [[nodiscard]] auto buttonHeld(GamePad::Buttons button) const -> bool;
    [[nodiscard]] auto get2DAxisInfo(GamePad::Axis axis) const -> GamePad::JoystickInfo;
    [[nodiscard]] auto get1DAxisInfo(GamePad::Axis axis) const -> f32;
    [[nodiscard]] auto hasValidConnectedDevice() const -> bool;

private:
    [[nodiscard]] static auto buttonIDToEnum(uint16_t id) -> GamePad::Buttons;
    [[nodiscard]] auto getNormalizedTriggerAxis(float raw) const -> float;
    [[nodiscard]] auto getStickAxis(const sf::Vector2f& rawAxis, bool isLeftStick) const -> GamePad::JoystickInfo;

    struct PrivData;
    std::unique_ptr<PrivData> m_privData;
    std::set<GamePad::Buttons> m_pressedButtons;
    std::set<GamePad::Buttons> m_releasedButtons;
    std::set<GamePad::Buttons> m_heldButtons;
    std::unordered_map<GamePad::Axis, GamePad::JoystickInfo> m_2dAxisValues;
    std::unordered_map<GamePad::Axis, f32> m_1dAxisValues;
    std::optional<unsigned long> m_userIndex;
};
} // nce namespace