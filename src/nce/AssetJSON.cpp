#include "../stdafx.h"

#include "PackedZIP.hpp"
#include "nce/AssetJSON.hpp"
#include "nce/NCE.hpp"

#include <zip.h>
namespace nce {
AssetJSON::AssetJSON(FileOrigin fileOrigin)
    : m_fileOrigin(fileOrigin)
{
}

tl::expected<AssetsInfo, std::string> AssetJSON::getJSONData()
{
    if (m_loaded)
        return m_assetInfo;

    nlohmann::json jsonRootNode;

    switch (m_fileOrigin) {
    case FileOrigin::Disk: {
        // Verify the rootDirectory that contains unpacked assets exists
        if (!std::filesystem::exists(std::filesystem::path("assets.json")))
            return ErrVal("No assets json found in current directory");

        std::ifstream detailsFile;
        detailsFile.open("assets.json", std::ios::in);

        if (detailsFile.fail())
            return ErrVal("Unable to open asset details JSON file\n");

        try {
            jsonRootNode = nlohmann::json::parse(detailsFile);
        } catch (const nlohmann::json::exception& e) {
            return ErrVal(std::format("Unable to load JSON file, exception reported:\n{}\n", e.what()));
        }
    } break;

    case FileOrigin::Zip: {
        i32 errNum;
        auto zip = zip_openwitherror("bin/packed.dat", 6, 'r', &errNum);
        if (errNum != 0)
            return ErrVal(ZIP_ERROR_TO_STRING(errNum));

        errNum = zip_entry_open(zip, ASSET_DETAILS_FILE_NAME);
        if (errNum != 0)
            return ErrVal(ZIP_ENTRY_ERROR_TO_STRING(errNum));

        void* buf = nullptr;
        size_t bufsize;

        errNum = static_cast<i32>(zip_entry_read(zip, &buf, &bufsize));
        if (errNum < 0)
            return ErrVal(ZIP_ENTRY_ERROR_TO_STRING(errNum));

        std::string assetsFile;
        assetsFile.resize(bufsize);
        memcpy(assetsFile.data(), buf, bufsize);
        free(buf);
        zip_close(zip);

        try {
            jsonRootNode = nlohmann::json::parse(assetsFile);
        } catch (const nlohmann::json::exception& e) {
            return ErrVal(std::format("Unable to load JSON file, exception reported:\n{}\n", e.what()));
        }
    } break;
    }

    loadFromJSONNode(jsonRootNode);

    return m_assetInfo;
}

void AssetJSON::loadFromJSONNode(const nlohmann::json& jsonRootNode)
{
    auto fontsArray = jsonRootNode["fonts"];
    auto texturesArray = jsonRootNode["textures"];
    auto shadersArray = jsonRootNode["shaders"];
    auto soundsArray = jsonRootNode["sounds"];

    for (auto& font : fontsArray) {
        printf("Identified font entry [ID: %s, Path: %s]\n", font["id"].get<std::string>().data(), font["path"].get<std::string>().data());
        m_assetInfo.fonts.emplace_back(font["id"], font["path"]);
    }

    for (auto& texture : texturesArray) {
        printf("Identified texture entry [ID: %s, Path: %s]\n", texture["id"].get<std::string>().data(), texture["path"].get<std::string>().data());
        m_assetInfo.textures.emplace_back(texture["id"], texture["path"]);
    }

    for (auto& shader : shadersArray) {
        printf("Identified shader entry [ID: %s, Path: %s]\n", shader["id"].get<std::string>().data(), shader["path"].get<std::string>().data());
        m_assetInfo.shaders.emplace_back(shader["id"], shader["path"]);
    }

    for (auto& sound : soundsArray) {
        printf("Identified sound entry [ID: %s, Path: %s]\n", sound["id"].get<std::string>().data(), sound["path"].get<std::string>().data());
        m_assetInfo.sounds.emplace_back(sound["id"], sound["path"]);
    }
    m_loaded = true;
}
} // nce namespace