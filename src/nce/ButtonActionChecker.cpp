#include "../stdafx.h"

#include "nce/ButtonActionChecker.hpp"
#include "nce/InputHandler.hpp"

namespace nce {
ButtonActionChecker::ButtonActionChecker() = default;

ButtonActionChecker::~ButtonActionChecker() = default;

void ButtonActionChecker::addKeyboardAction(std::string_view action, ActionType type, sf::Keyboard::Scan key)
{
    m_actionsKeyboard[action.data()].push_back({ type, key });
}

void ButtonActionChecker::addMouseAction(std::string_view action, ActionType type, sf::Mouse::Button button)
{
    m_actionsMouse[action.data()].push_back({ type, button });
}

void ButtonActionChecker::addGamePadAction(std::string_view action, ActionType type, nce::GamePad::Buttons button)
{
    m_actionsGamePad[action.data()].push_back({ type, button });
}

// void ButtonActionChecker::addGamePadAxisAsAction(std::string_view action, ActionAxisRange range, nce::GamePad::Axis axis)
// {
//     m_actionsGamePadAxis[action.data()].push_back({ range, axis });
// }

auto ButtonActionChecker::checkAction(std::string_view action) const -> std::optional<ActionTrigger>
{
    auto keyboardFind { m_actionsKeyboard.find(action.data()) };
    auto mouseFind { m_actionsMouse.find(action.data()) };
    auto gamePadFind { m_actionsGamePad.find(action.data()) };
    // auto gamePadAxisFind { m_actionsGamePadAxis.find(action.data()) };

    // If we have no actions, do nothing
    if (keyboardFind == m_actionsKeyboard.end() && mouseFind == m_actionsMouse.end() && gamePadFind == m_actionsGamePad.end()
        /*&& gamePadAxisFind == m_actionsGamePadAxis.end()*/)
        return {};

    auto triggered { false };

    if (keyboardFind != m_actionsKeyboard.end()) {
        for (const auto& pair : (*keyboardFind).second) {
            auto before { triggered };
            switch (pair.first) {
            case ActionType::Held:
                triggered = nce::InputHandler::KeyHeld(pair.second);
                break;
            case ActionType::Pressed:
                triggered = nce::InputHandler::KeyPressed(pair.second);
                break;
            case ActionType::Released:
                triggered = nce::InputHandler::KeyReleased(pair.second);
                break;
            }
            if (before != triggered)
                return { ActionTrigger ::Keyboard };
        }
    }

    if (mouseFind != m_actionsMouse.end()) {
        for (const auto& pair : (*mouseFind).second) {
            auto before { triggered };
            switch (pair.first) {
            case ActionType::Held:
                triggered = nce::InputHandler::MouseHeld(pair.second);
                break;
            case ActionType::Pressed:
                triggered = nce::InputHandler::MousePressed(pair.second);
                break;
            case ActionType::Released:
                triggered = nce::InputHandler::MouseReleased(pair.second);
                break;
            }
            if (before != triggered)
                return { ActionTrigger ::Mouse };
        }
    }

    const auto& gamePad { nce::InputHandler::GetGamePad() };

    if (gamePad.hasValidConnectedDevice()) {
        if (gamePadFind != m_actionsGamePad.end()) {
            for (const auto& pair : (*gamePadFind).second) {
                auto before { triggered };

                switch (pair.first) {
                case ActionType::Held:
                    triggered = gamePad.buttonHeld(pair.second);
                    break;
                case ActionType::Pressed:
                    triggered = gamePad.buttonPressed(pair.second);
                    break;
                case ActionType::Released:
                    triggered = gamePad.buttonReleased(pair.second);
                    break;
                }
                if (before != triggered)
                    return { ActionTrigger::GamePad };
            }
        }

        // if (gamePadAxisFind != m_actionsGamePadAxis.end()) {
        //
        //     for (const auto& pair : (*gamePadAxisFind).second) {
        //         const auto axisValue { gamePad.getAxisNormalizedPosition(pair.second) };
        //
        //         if (axisValue == 0.f)
        //             continue;
        //
        //         if (pair.first == ActionAxisRange::Positive && axisValue > 0.f)
        //             return { ActionTrigger::GamePad };
        //         else if (pair.first == ActionAxisRange::Negative && axisValue < 0.f)
        //             return { ActionTrigger::GamePad };
        //     }
        // }
    }

    return {};
}
} // nce namespace
