#include "../stdafx.h"

#include "nce/AppState.hpp"
#include "nce/HUDElement.hpp"

namespace nce {
HUDElement::HUDElement(AppState* appState)
    : m_appState(appState)
{
}
} // nce namespace
