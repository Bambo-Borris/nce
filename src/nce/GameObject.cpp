#include "../stdafx.h"

#include "nce/GameObject.hpp"
#include "nce/NCE.hpp"

namespace nce {
GameObject::GameObject(AppState* currentState)
    : m_currentState(currentState)
{
    m_identifier = GenerateUUID();
}

ErrResult GameObject::init()
{
    for (auto i { 0u }; i < m_components.size(); ++i) {
        const auto initResult = m_components[i]->init();
        if (!initResult) {
            return initResult;
        }
    }

    return true;
}

ErrResult GameObject::onEnterState()
{
    for (auto i { 0u }; i < m_components.size(); ++i) {
        const auto enterResult = m_components[i]->onEnterState();
        if (!enterResult) {
            return enterResult;
        }
    }

    return true;
}

ErrResult GameObject::onExitState()
{
    for (auto i { 0u }; i < m_components.size(); ++i) {
        const auto exitResult = m_components[i]->onExitState();
        if (!exitResult) {
            return exitResult;
        }
    }

    return true;
}

void GameObject::update(const sf::Time& dt)
{
    for (auto i { 0u }; i < m_components.size(); ++i)
        m_components[i]->update(dt);
}

void GameObject::fixedUpdate(const sf::Time& dt, const sf::Time& fixedDT)
{
    for (auto i { 0u }; i < m_components.size(); ++i)
        m_components[i]->fixedUpdate(dt, fixedDT);
}

void GameObject::removeComponentByTag(u32 tag)
{
    std::erase_if(m_components, [tag](const std::unique_ptr<Component>& comp) -> bool { return comp->getTag() == tag; });
}
}
