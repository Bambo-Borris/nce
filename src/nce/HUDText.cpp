#include "../stdafx.h"

#include "nce/App.hpp"
#include "nce/AppState.hpp"
#include "nce/AssetHolder.hpp"
#include "nce/HUDElement.hpp"
#include "nce/HUDText.hpp"
#include "nce/TextUtils.hpp"

namespace nce {
HUDText::HUDText(AppState* appState, std::string_view fontName, std::string_view textString, uint32_t charSize)
    : HUDElement(appState)
{
    auto font = AssetHolder::get().getFont(fontName);
    m_text.emplace(*font);
    m_text->setCharacterSize(charSize);
    m_text->setString(textString.data());
    switch (getOriginLocation()) {
    // Only centre requires some special treatment
    case OriginLocation::Centre:
        CentreTextOrigin(*m_text);
        break;
    default:
        break;
    }
    auto position = sf::Vector2f { NCE_APP().getRenderTexture()->getSize() }.cwiseMul(m_anchorPosition);
    m_text->setPosition(position);
}

void HUDText::update(const sf::Time& dt, sf::RenderTarget& target)
{
    NCE_UNUSED(dt);
    NCE_UNUSED(target);
    switch (getOriginLocation()) {
    // Only centre requires some special treatment
    case OriginLocation::Centre:
        CentreTextOrigin(*m_text);
        break;
    default:
        break;
    }

    //    auto position = sf::Vector2f { target.getSize() }.cwiseMul(m_anchorPosition);
    //    m_text->setPosition(position);
}

void HUDText::draw(sf::RenderTarget& target, sf::RenderStates states) const { target.draw(*m_text, states); }

void HUDText::setString(std::string_view string)
{
    m_text->setString(string.data());
    // Only centre requires some special treatment
    switch (getOriginLocation()) {
    // Only centre requires some special treatment
    case OriginLocation::Centre:
        CentreTextOrigin(*m_text);
        break;
    default:
        break;
    }
}

void HUDText::setAnchorPosition(const sf::Vector2f& pos)
{
    m_anchorPosition.x = std::clamp(pos.x, 0.f, 1.f);
    m_anchorPosition.y = std::clamp(pos.y, 0.f, 1.f);

    auto position = sf::Vector2f { NCE_APP().getRenderTexture()->getSize() }.cwiseMul(m_anchorPosition);
    m_text->setPosition(position);
}
void HUDText::setFillColour(const sf::Color& col) { m_text->setFillColor(col); }

sf::FloatRect HUDText::getOnScreenBounds() const { return m_text->getGlobalBounds(); }

void HUDText::setPosition(const sf::Vector2u& position) { m_text->setPosition(sf::Vector2f { position }); }
} // nce namespace
