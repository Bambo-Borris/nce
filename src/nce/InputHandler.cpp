#include "../stdafx.h"

#include "nce/InputHandler.hpp"

#if defined NCE_PLATFORM_LINUX
#include <fcntl.h>
#include <linux/uinput.h>
#endif

#include "nce/App.hpp"

#include <imgui.h>

// Necessary operator overload for GamePad logic
namespace sf {
namespace Joystick {
inline bool operator==(const Identification& lhs, const Identification& rhs)
{
    return lhs.name == rhs.name && lhs.productId == rhs.productId && lhs.vendorId == rhs.vendorId;
}
}
}

#if defined NCE_PLATFORM_LINUX
void emit(int fd, int type, int code, int val)
{
    struct input_event ie;

    ie.type = static_cast<uint16_t>(type);
    ie.code = static_cast<uint16_t>(code);
    ie.value = val;
    /* timestamp values below are ignored */
    ie.time.tv_sec = 0;
    ie.time.tv_usec = 0;

    auto capture = write(fd, &ie, sizeof(ie));
    NCE_UNUSED(capture);
}

int32_t CreateFileDescriptor()
{
    struct uinput_setup usetup;

    int fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
    if (fd == -1) {
        return fd;
    }
    /* enable mouse button left and relative events */
    ioctl(fd, UI_SET_EVBIT, EV_KEY);
    ioctl(fd, UI_SET_KEYBIT, BTN_LEFT);

    ioctl(fd, UI_SET_EVBIT, EV_REL);
    ioctl(fd, UI_SET_RELBIT, REL_X);
    ioctl(fd, UI_SET_RELBIT, REL_Y);

    memset(&usetup, 0, sizeof(usetup));
    usetup.id.bustype = BUS_USB;
    usetup.id.vendor = 0x1234; /* sample vendor */
    usetup.id.product = 0x5678; /* sample product */
    strcpy(usetup.name, "Example device");

    ioctl(fd, UI_DEV_SETUP, &usetup);
    ioctl(fd, UI_DEV_CREATE);

    return fd;
}
#endif

namespace nce {
constexpr auto AXIS_DEADZONE_LOWER = 7.5f;
constexpr auto AXIS_DEADZONE_UPPER = 90.0f;

PadType InputHandler::m_padType { PadType::Xbox_Pad };
sf::Vector2i InputHandler::m_mousePositionScreen;
sf::Vector2i InputHandler::m_mouseScreenDelta;

std::set<sf::Keyboard::Scan> InputHandler::m_keyboardPressed;
std::set<sf::Keyboard::Scan> InputHandler::m_keyboardReleased;
std::set<sf::Keyboard::Scan> InputHandler::m_keyboardHeld;

std::set<sf::Mouse::Button> InputHandler::m_mouseButtonPressed;
std::set<sf::Mouse::Button> InputHandler::m_mouseButtonReleased;
std::set<sf::Mouse::Button> InputHandler::m_mouseButtonHeld;

GamePad InputHandler::m_gamePad = {};
int32_t InputHandler::m_fileDescriptor { -1 };

// Operator overloads

float get_normalized_axis_value(float pos)
{
    float pos_abs = fabsf(pos);
    const float sign = pos_abs / pos;
    if (pos_abs > AXIS_DEADZONE_LOWER) {
        pos_abs = std::min(AXIS_DEADZONE_UPPER, pos_abs);
        pos_abs += (100.0f - AXIS_DEADZONE_UPPER);
        pos_abs /= 100.0f;
        pos_abs *= sign;
        return pos_abs;
    }
    return 0.0f;
}

JoystickButton id_to_joystick_button(std::uint32_t id)
{
    switch (id) {
    case 0:
        return JoystickButton::Square;
        break;
    case 1:
        return JoystickButton::Cross;
        break;
    case 2:
        return JoystickButton::Circle;
        break;
    case 3:
        return JoystickButton::Triangle;
        break;
    case 4:
        return JoystickButton::L1;
        break;
    case 5:
        return JoystickButton::R1;
        break;
    case 6:
        return JoystickButton::L2;
        break;
    case 7:
        return JoystickButton::R2;
        break;
    case 8:
        return JoystickButton::Share;
        break;
    case 9:
        return JoystickButton::Options;
        break;
    case 10:
        return JoystickButton::Left_Stick_Click;
        break;
    case 11:
        return JoystickButton::Right_Stick_Click;
        break;
    case 12:
        // ps4 button but we don't use it yet
        break;
    case 13:
        // Trackpad on PS4 but we don't use it (yet)
        break;
    }

    return JoystickButton::Joystick_Button_Max;
}

void InputHandler::Update()
{
    m_mouseScreenDelta = {};
    m_keyboardPressed.clear();
    m_keyboardReleased.clear();
    m_mouseButtonPressed.clear();
    m_mouseButtonReleased.clear();
    m_gamePad.update();
}

void InputHandler::HandleEvent(const sf::Event& event)
{
    if (event.getIf<sf::Event::FocusLost>()) {
        m_keyboardHeld.clear();
        m_mouseButtonHeld.clear();
    }

    HandleKeyboardAndMouse(event);
    m_gamePad.handleEvents(event);
}

void InputHandler::SetMousePosition(const sf::Vector2i& pos, sf::RenderWindow* relativeTo)
{
#if defined NCE_PLATFORM_LINUX
    static const bool isX11 { std::string(getenv("XDG_SESSION_TYPE")) == "x11" };
    static const bool isWayland { std::string(getenv("XDG_SESSION_TYPE")) == "wayland" };

    if (isX11) {
        if (relativeTo)
            sf::Mouse::setPosition(pos, *relativeTo);
        else
            sf::Mouse::setPosition(pos, *relativeTo);
    } else if (isWayland) {
        if (m_fileDescriptor == -1) {
            m_fileDescriptor = CreateFileDescriptor();
            assert(m_fileDescriptor != -1);
            if (m_fileDescriptor == -1)
                throw std::runtime_error(std::format("Invalid file descriptor in {}", m_fileDescriptor));
        }
        if (relativeTo) {
            const auto delta = (pos + relativeTo->getPosition()) - sf::Mouse::getPosition();
            emit(m_fileDescriptor, EV_REL, REL_X, delta.x);
            emit(m_fileDescriptor, EV_REL, REL_Y, delta.y);
            emit(m_fileDescriptor, EV_SYN, SYN_REPORT, 0);
            spdlog::warn("In {}: Mouse Delta is [{}, {}] and Mouse Pos [{}, {}]",
                         __FUNCTION__,
                         delta.x,
                         delta.y,
                         sf::Mouse::getPosition(*relativeTo).x,
                         sf::Mouse::getPosition(*relativeTo).y);
        } else {
            emit(m_fileDescriptor, EV_REL, ABS_X, pos.x);
            emit(m_fileDescriptor, EV_REL, ABS_Y, pos.y);
            emit(m_fileDescriptor, EV_SYN, SYN_REPORT, 0);
        }

    } else {
        throw std::runtime_error("Unknown windowing system found for device, didn't detect x11 or wayland");
    }
    NCE_UNUSED(pos);
    NCE_UNUSED(relativeTo);
#elif defined NCE_PLATFORM_WINDOWS
    if (relativeTo)
        sf::Mouse::setPosition(pos, *relativeTo);
    else
        sf::Mouse::setPosition(pos);
#endif
}

void InputHandler::CleanUp()
{
#if defined NCE_PLATFORM_LINUX
    if (m_fileDescriptor != -1) {
        close(m_fileDescriptor);
    }
#endif
}

auto InputHandler::KeyPressed(sf::Keyboard::Scan key) -> bool
{
    const auto present = m_keyboardPressed.find(key);
    return present != m_keyboardPressed.end();
}

auto InputHandler::KeyHeld(sf::Keyboard::Scan key) -> bool
{
    const auto present = m_keyboardHeld.find(key);
    return present != m_keyboardHeld.end();
}

auto InputHandler::KeyReleased(sf::Keyboard::Scan key) -> bool
{
    const auto present = m_keyboardReleased.find(key);
    return present != m_keyboardReleased.end();
}

auto InputHandler::MousePressed(sf::Mouse::Button button) -> bool
{
    const auto present = m_mouseButtonPressed.find(button);
    return present != m_mouseButtonPressed.end();
}

auto InputHandler::MouseReleased(sf::Mouse::Button button) -> bool
{
    const auto present = m_mouseButtonReleased.find(button);
    return present != m_mouseButtonReleased.end();
}

auto InputHandler::MouseHeld(sf::Mouse::Button button) -> bool
{
    const auto present = m_mouseButtonHeld.find(button);
    return present != m_mouseButtonHeld.end();
}

auto InputHandler::GetMouseWorldPosition() -> sf::Vector2f
{
    const auto normalizedScreenPos = sf::Vector2f { m_mousePositionScreen }.cwiseDiv(sf::Vector2f { NCE_APP().getRenderWindow()->getSize() });
    const auto screenPosRT = sf::Vector2i { normalizedScreenPos.cwiseMul(sf::Vector2f { NCE_APP().getRenderTexture()->getSize() }) };
    return NCE_APP().getRenderTexture()->mapPixelToCoords(screenPosRT);
}

auto InputHandler::GetMouseScreenPosition() -> sf::Vector2i { return m_mousePositionScreen; }

auto InputHandler::GetMouseScreenDelta() -> sf::Vector2i { return m_mouseScreenDelta; }

auto InputHandler::AnyKeyPressed() -> bool { return !m_keyboardPressed.empty() || !m_mouseButtonPressed.empty(); }

void InputHandler::HandleKeyboardAndMouse(const sf::Event& event)
{
    switch (event.getType()) {
    case sf::Event::Type::MouseMoved: {
        const auto before = m_mousePositionScreen;
        const auto mouseMove = event.getIf<sf::Event::MouseMoved>();
        m_mousePositionScreen = sf::Vector2i { mouseMove->position.x, mouseMove->position.y };
        m_mouseScreenDelta = m_mousePositionScreen - before;
    } break;
    case sf::Event::Type::MouseButtonPressed: {
        auto mouseButtonPressed = event.getIf<sf::Event::MouseButtonPressed>();
        if (!ImGui::GetIO().WantCaptureMouse) {
            m_mouseButtonPressed.emplace(mouseButtonPressed->button);
            m_mouseButtonHeld.emplace(mouseButtonPressed->button);
        }
    } break;
    case sf::Event::Type::MouseButtonReleased: {
        auto mouseButtonReleased = event.getIf<sf::Event::MouseButtonReleased>();
        if (!ImGui::GetIO().WantCaptureMouse) {
            auto removeIt = m_mouseButtonHeld.find(mouseButtonReleased->button);
            if (removeIt != m_mouseButtonHeld.end())
                m_mouseButtonHeld.erase(removeIt);

            m_mouseButtonReleased.emplace(mouseButtonReleased->button);
        }
    } break;
    case sf::Event::Type::KeyPressed: {
        auto keyPressed = event.getIf<sf::Event::KeyPressed>();
        m_keyboardPressed.emplace(keyPressed->scancode);
        m_keyboardHeld.emplace(keyPressed->scancode);
    } break;
    case sf::Event::Type::KeyReleased: {
        auto keyReleased = event.getIf<sf::Event::KeyReleased>();
        m_keyboardReleased.emplace(keyReleased->scancode);
        auto removeIt = m_keyboardHeld.find(keyReleased->scancode);
        if (removeIt != m_keyboardHeld.end())
            m_keyboardHeld.erase(removeIt);
    } break;
    default:
        break;
    }
}
} // nce namespace
