#include "../stdafx.h"

#include "nce/Components/PhysicsObject.hpp"
#include "nce/PhysicsWorld.hpp"

namespace nce {
void PhysicsWorld::step(const sf::Time& dt) { NCE_UNUSED(dt); }

void PhysicsWorld::addObject(PhysicsObject* obj)
{
    assert(obj);
    m_objects.push_back(obj);
}

void PhysicsWorld::removeObject(PhysicsObject* obj)
{
    assert(obj);
    auto result = std::find(m_objects.begin(), m_objects.end(), obj);
    if (result != m_objects.end())
        m_objects.erase(result);
}
} // nce namespace
