#include "../stdafx.h"

#include "nce/CollisionSolver.hpp"
#include "nce/ColourUtils.hpp"
#include "nce/GameObject.hpp"

#include <SFML/Graphics/ConvexShape.hpp>
#include <algorithm>
#include <array>
#include <cassert>
#include <spdlog/spdlog.h>
#include <type_traits>

namespace nce {
CollisionSolver::CollisionSolver(const sf::Vector2u& cellSize, const sf::Vector2u& gridSize, const sf::Vector2f& position)
    : m_gridSize(gridSize)
    , m_cellSize(cellSize)
    , m_position(position)
{
    m_gridCells.resize(static_cast<size_t>(gridSize.x * gridSize.y));

    m_debugVertices[size_t(DebugGraphicsLayers::Grid)].setPrimitiveType(sf::PrimitiveType::Lines);
    m_debugVertices[size_t(DebugGraphicsLayers::Occupied)].setPrimitiveType(sf::PrimitiveType::Triangles);
    m_debugNormals.setPrimitiveType(sf::PrimitiveType::Lines);

    /*
     * We use a 2D array as a jump table to use for invoking our collision methods, here we'll bind
     * all of the various functions to the 2D array
     */

    // Circle vs Other
    m_jumpTable[size_t(ColliderType::Circle)][size_t(ColliderType::Circle)] = std::bind(CircleVsCircle, std::placeholders::_1, std::placeholders::_2);
    m_jumpTable[size_t(ColliderType::Circle)][size_t(ColliderType::AABB)] = std::bind(CircleVsAABB, std::placeholders::_1, std::placeholders::_2);
    m_jumpTable[size_t(ColliderType::Circle)][size_t(ColliderType::OBB)] = std::bind(CircleVsOBB, std::placeholders::_1, std::placeholders::_2);
    m_jumpTable[size_t(ColliderType::Circle)][size_t(ColliderType::Poly)] = std::bind(CircleVsPoly, std::placeholders::_1, std::placeholders::_2);

    // AABB vs Other
    m_jumpTable[size_t(ColliderType::AABB)][size_t(ColliderType::Circle)] = std::bind(AABBVsCircle, std::placeholders::_1, std::placeholders::_2);
    m_jumpTable[size_t(ColliderType::AABB)][size_t(ColliderType::AABB)] = std::bind(AABBVsAABB, std::placeholders::_1, std::placeholders::_2);
    m_jumpTable[size_t(ColliderType::AABB)][size_t(ColliderType::OBB)] = std::bind(AABBVsOBB, std::placeholders::_1, std::placeholders::_2);
    m_jumpTable[size_t(ColliderType::AABB)][size_t(ColliderType::Poly)] = std::bind(AABBVsPoly, std::placeholders::_1, std::placeholders::_2);

    // OBB vs Other
    m_jumpTable[size_t(ColliderType::OBB)][size_t(ColliderType::Circle)] = std::bind(OBBVsCircle, std::placeholders::_1, std::placeholders::_2);
    m_jumpTable[size_t(ColliderType::OBB)][size_t(ColliderType::AABB)] = std::bind(OBBVsAABB, std::placeholders::_1, std::placeholders::_2);
    m_jumpTable[size_t(ColliderType::OBB)][size_t(ColliderType::OBB)] = std::bind(OBBVsOBB, std::placeholders::_1, std::placeholders::_2);
    // m_jumpTable[size_t(ColliderType::OBB)][size_t(ColliderType::Poly)] = std::bind(OBBvsPoly, std::placeholders::_1, std::placeholders::_2);

    // Poly vs Other
    // m_jumpTable[size_t(ColliderType::Poly)][size_t(ColliderType::Circle)] = std::bind(PolyvsCircle, std::placeholders::_1, std::placeholders::_2);
    // m_jumpTable[size_t(ColliderType::Poly)][size_t(ColliderType::AABB)] = std::bind(PolyvsAABB, std::placeholders::_1, std::placeholders::_2);
    // m_jumpTable[size_t(ColliderType::Poly)][size_t(ColliderType::OBB)] = std::bind(PolyvsOBB, std::placeholders::_1, std::placeholders::_2);
    // m_jumpTable[size_t(ColliderType::Poly)][size_t(ColliderType::Poly)] = std::bind(PolyvsPoly, std::placeholders::_1, std::placeholders::_2);
}

auto CollisionSolver::addCollider(const ColliderInfo& collider) -> std::optional<std::uint64_t>
{
    assert(collider.go);
    if (!collider.go)
        return std::nullopt;

    m_colliders.push_back(ColliderInfoEntry { .colliderInfo = std::make_unique<ColliderInfo>(collider), .id = m_nextUniqueID++ });

    return { m_colliders.back().id };
}

void CollisionSolver::removeCollider(std::uint64_t id)
{
    auto findResult = std::ranges::find(m_colliders, id, &ColliderInfoEntry::id);
    if (findResult == m_colliders.end())
        return;

    // This is really ineffecient but will do for now, there's a far more intelligent approach we should do here
    // that involves taking the code from CollisionSolver::update() which determines the potential cells that a
    // collider occupies, then iterate over only those cells and ensure we remove the collider ID supplied from those
    // cells
    for (auto& cell : m_gridCells) {
        if (cell.entries.empty())
            continue;

        auto entryFind = std::find_if(cell.entries.begin(), cell.entries.end(), [&id](ColliderInfoEntry* entry) -> bool { return entry->id == id; });
        if (entryFind != cell.entries.end()) {
            cell.entries.erase(entryFind);
        }
    }

    m_colliders.erase(findResult);
}

void CollisionSolver::update()
{
    // We manually handle setting up all collider transforms
    // so first we need to step them all to be up to date
    updateColliders();

    // For now let's not do anything smart, dump the contents
    // of all cells, we'll reappend the entities after
    for (auto& cell : m_gridCells) {
        cell.entries.clear();
    }

    const auto gridPosToIndex = [this](const sf::Vector2u& gridPos) { return size_t(gridPos.x + gridPos.y * m_gridSize.x); };

    const auto cellDiagonalDistanceSq { sf::Vector2f { m_cellSize }.length() };

    // Now let's add everything to the grid
    for (size_t i { 0 }; i < m_colliders.size(); ++i) {
        auto& colliderInfo { m_colliders[i].colliderInfo };
        if (!colliderInfo->go->isActive())
            continue;

        const auto objectAABB { BoundingBoxFromCollider(*colliderInfo) };
        const auto& pos { colliderInfo->transformable->getPosition() };

        // prevent positions outside of the grid
        const sf::Vector2f minWorldPos {
            std::clamp(objectAABB.getPosition().x, m_position.x, m_position.x + float(m_gridSize.x * m_cellSize.x)),
            std::clamp(objectAABB.getPosition().y, m_position.y, m_position.y + float(m_gridSize.y * m_cellSize.y)),
        };

        const sf::Vector2f maxWorldPos {
            std::clamp((objectAABB.getPosition() + objectAABB.getSize()).x, m_position.x, m_position.x + float(m_gridSize.x * m_cellSize.x)),
            std::clamp((objectAABB.getPosition() + objectAABB.getSize()).y, m_position.y, m_position.y + float(m_gridSize.y * m_cellSize.y)),
        };

        const auto tempGridMin { convertWorldPositionToGridPosition(minWorldPos) };
        const auto tempGridMax { convertWorldPositionToGridPosition(maxWorldPos) };

        const sf::Vector2u minGridPos { std::clamp(tempGridMin.x, 0u, m_gridSize.x - 1), std::clamp(tempGridMin.y, 0u, m_gridSize.y - 1) };
        const sf::Vector2u maxGridPos { std::clamp(tempGridMax.x, 0u, m_gridSize.x - 1), std::clamp(tempGridMax.y, 0u, m_gridSize.y - 1) };

        for (auto x { minGridPos.x }; x <= maxGridPos.x; ++x) {
            for (auto y { minGridPos.y }; y <= maxGridPos.y; ++y) {
                const sf::Vector2f cellCentre { m_position.x + (float(x * m_cellSize.x) + float(m_cellSize.x) / 2.f),
                                                m_position.y + (float(y * m_cellSize.y) + float(m_cellSize.y) / 2.f) };
                const sf::FloatRect cellAABB { m_position + sf::Vector2f { sf::Vector2u { x * m_cellSize.x, y * m_cellSize.y } }, sf::Vector2f { m_cellSize } };
                const auto cellIndex = gridPosToIndex({ x, y });
                NCE_UNUSED(cellDiagonalDistanceSq);
                NCE_UNUSED(pos);

                if (objectAABB.findIntersection(cellAABB)) {
                    m_gridCells[cellIndex].entries.push_back(&m_colliders[i]);
                }
            }
        }
    }

    // Do broadphase
    auto potentialCollisions = broadPhase();
    if (!potentialCollisions.empty())
        narrowPhase(potentialCollisions);

    if (m_enableDebugDrawing) {
        updateDebugVertices();
        updateDebugShapes();
    }
}

void CollisionSolver::draw(sf::RenderTarget& target) const
{
    if (!m_enableDebugDrawing)
        return;

    sf::RenderStates s;
    s.transform *= s.transform.scale(m_debugDrawScale);

    // Draw debug grid
    for (auto& va : m_debugVertices)
        target.draw(va, s);

    // Draw debug shapes
    for (auto& shape : m_colliderDebugShapes)
        target.draw(*shape, s);

    target.draw(m_debugNormals, s);
}

u64 CollisionSolver::subscribeOnCollisionCallback(const CallbackInfoOnCollision& info)
{
    const auto id = m_onCollisionCallbackSubscribersIDCounter;
    m_onCollisionCallbackSubscribers[m_onCollisionCallbackSubscribersIDCounter++] = info;
    return id;
}

void CollisionSolver::unsubscribeOnCollisionCallBack(u64 subscriberID)
{
    if (!m_onCollisionCallbackSubscribers.contains(subscriberID))
        return;

    m_onCollisionCallbackSubscribers.erase(subscriberID);
}

u64 CollisionSolver::subscribeCollisionExitCallback(const CollisionSolver::CallbackInfoCollisionExit& info)
{
    const auto id = m_collisionExitCallbackSubscribersIDCounter;
    m_collisionExitCallbackSubscribers[m_collisionExitCallbackSubscribersIDCounter++] = info;
    return id;
}

void CollisionSolver::unsubscribeCollisionExitCallBack(u64 subscriberID)
{
    if (!m_collisionExitCallbackSubscribers.contains(subscriberID))
        return;

    m_collisionExitCallbackSubscribers.erase(subscriberID);
}

ColliderInfo* CollisionSolver::getColliderInfo(u64 colliderId) const
{
    auto findResult = std::ranges::find(m_colliders, colliderId, &ColliderInfoEntry::id);
    if (findResult == m_colliders.end())
        return nullptr;
    return (*findResult).colliderInfo.get();
}

void CollisionSolver::reconstructGrid(const sf::Vector2u& cellSize, const sf::Vector2u& gridSize, const sf::Vector2f& position)
{
    m_gridSize = gridSize;
    m_cellSize = cellSize;
    m_position = position;

    update();
}

auto CollisionSolver::castRay(const sf::Vector2f& start, sf::Angle direction, float distance, GameObject* toIgnore)
    -> std::optional<std::pair<GameObject*, sf::Vector2f>>
{
    std::vector<std::pair<GameObject*, sf::Vector2f>> confirmedIntersections;
    const auto rayEnd { start + sf::Vector2f { distance, direction } };

    sf::Vector2f clampedStart { std::clamp(start.x, 0.f, f32((m_gridSize.x - 1) * m_cellSize.x)),
                                std::clamp(start.y, 0.f, f32((m_gridSize.y - 1) * m_cellSize.y)) };

    sf::Vector2f clampedEnd {
        std::clamp(rayEnd.x, 0.f, f32((m_gridSize.x - 1) * m_cellSize.x)),
        std::clamp(rayEnd.y, 0.f, f32((m_gridSize.y - 1) * m_cellSize.y)),
    };

    auto gridStart { convertWorldPositionToGridPosition(clampedStart) };
    auto gridEnd { convertWorldPositionToGridPosition(clampedEnd) };

    const u64 minX { std::min(gridStart.x, gridEnd.x) };
    const u64 minY { std::min(gridStart.y, gridEnd.y) };

    const u64 maxX { std::max(gridStart.x, gridEnd.x) };
    const u64 maxY { std::max(gridStart.y, gridEnd.y) };

    for (u64 i { minX }; i <= maxX; ++i) {
        for (u64 j { minY }; j <= maxY; ++j) {
            const auto index = i + j * m_gridSize.x;
            assert(index < m_gridCells.size());
            if (m_gridCells[index].entries.empty()) {
                continue;
            }

            for (auto& c : m_gridCells[index].entries) {
                auto& colliderInfo { c->colliderInfo };
                assert(colliderInfo->go);
                if (colliderInfo->go == toIgnore)
                    continue;

                switch (colliderInfo->type) {
                case nce::ColliderType::AABB: {
                    auto& aabbMetadata { std::get<nce::AABBMetadata>(colliderInfo->metadata) };
                    auto floatRect { colliderInfo->transformable->getTransform().transformRect(aabbMetadata.bounds) };
                    auto vertices = ComputeAABBWorldVertices(floatRect);

                    for (size_t k { 0 }; k < vertices.size(); ++k) {
                        auto nextIdx { (k + 1) % 4 };
                        if (auto intersection = LineSegmentIntersectionTest(start, rayEnd, vertices[k], vertices[nextIdx])) {
                            confirmedIntersections.emplace_back(colliderInfo->go, *intersection);
                        }
                    }

                } break;
                case nce::ColliderType::Circle: {
                    auto& circleMetadata { std::get<nce::CircleMetadata>(colliderInfo->metadata) };
                    const auto radius = colliderInfo->transformable->getScale().x * circleMetadata.radius;

                    if (auto intersection = CircleLineSegmentIntersection(
                            start, sf::Vector2f { 1.f, direction }, (start - rayEnd).length(), colliderInfo->transformable->getPosition(), radius)) {
                        confirmedIntersections.emplace_back(colliderInfo->go, *intersection);
                    }
                } break;
                default:
                    assert(false && "Unsupported collider type raycast");
                    break;
                }
            }
        }
    }

    if (confirmedIntersections.empty())
        return {};

    // Implementation will return the closest intersection to the start point, so sort
    // and determine the closest result
    using RaycastIntersectionPair = std::pair<GameObject*, sf::Vector2f>;
    std::sort(
        confirmedIntersections.begin(), confirmedIntersections.end(), [start](const RaycastIntersectionPair& lhs, const RaycastIntersectionPair& rhs) -> bool {
            return (lhs.second - start).lengthSq() < (rhs.second - start).lengthSq();
        });

    return confirmedIntersections[0];
}

void CollisionSolver::updateColliders()
{
    std::vector<ColliderInfo*> active;
    for (auto& ci : m_colliders) {
        auto& colliderInfo { ci.colliderInfo };
        if (!colliderInfo->go->isActive())
            continue;

        active.push_back(colliderInfo.get());

        bool isPlayer = colliderInfo->go->getIdentifier() == "player";
        NCE_UNUSED(isPlayer);

        switch (colliderInfo->type) {
        case ColliderType::Circle: {

        } break;
        case ColliderType::AABB: {
        } break;
        case ColliderType::OBB: {

        } break;
        case ColliderType::Poly: {

        } break;
        default:
            assert(false);
            break;
        }
    }
}

std::vector<CollisionSolver::CollisionPair> CollisionSolver::broadPhase()
{
    std::vector<CollisionPair> pairs;
    const auto pairExists = [&pairs](ColliderInfoEntry* objA, ColliderInfoEntry* objB) -> bool {
        const auto findAB { std::find(pairs.begin(), pairs.end(), CollisionPair { objA, objB, {} }) };
        const auto findBA { std::find(pairs.begin(), pairs.end(), CollisionPair { objA, objB, {} }) };
        return findAB != pairs.end() || findBA != pairs.end();
    };

    for (auto& cell : m_gridCells) {
        // Narrow the search down, exclude empty or singularly occupied cells
        if (cell.entries.empty() || cell.entries.size() == 1)
            continue;

        for (size_t i = 0; i < cell.entries.size(); ++i) {
            for (size_t j = i + 1; j < cell.entries.size(); ++j) {
                // If we have a GO with multiple colliders, we don't want
                // to test self collisions.
                if (cell.entries[i]->colliderInfo->go == cell.entries[j]->colliderInfo->go)
                    continue;

                // Check collision mask works
                if ((cell.entries[i]->colliderInfo->mask & cell.entries[j]->colliderInfo->mask))
                    continue;

                if (!pairExists(cell.entries[i], cell.entries[j])) {
                    CollisionPair p;
                    p.a = cell.entries[i];
                    p.b = cell.entries[j];
                    pairs.push_back(p);
                }
            }
        }
    }

    return pairs;
}

void CollisionSolver::narrowPhase(std::vector<CollisionSolver::CollisionPair>& collisionPairs)
{
    for (auto& pair : collisionPairs) {
        const auto findResult = std::ranges::find(m_currentlyColliding, pair);

        const auto manifold = m_jumpTable[size_t(pair.a->colliderInfo->type)][size_t(pair.b->colliderInfo->type)](*pair.a->colliderInfo, *pair.b->colliderInfo);
        if (!manifold) {
            // If this pair was in the currently colliding list
            // but is no longer colliding, then we can fire a
            // collision exit callback, and remove the pair from
            // the currently colliding list.
            if (findResult != m_currentlyColliding.end()) {
                for (auto& callback : m_collisionExitCallbackSubscribers) {
                    const auto isCallbackForB { callback.second.id == pair.b->id };
                    const auto isCallbackForA { callback.second.id == pair.a->id };
                    if (!isCallbackForA && !isCallbackForB)
                        continue;

                    isCallbackForA ? callback.second.cb(pair.b->colliderInfo->go) : callback.second.cb(pair.a->colliderInfo->go);
                }
                m_currentlyColliding.erase(findResult);
            }

            continue;
        }

        // Callbacks for the collision happening
        for (auto& callback : m_onCollisionCallbackSubscribers) {
            const auto isCallbackForB { callback.second.id == pair.b->id };
            const auto isCallbackForA { callback.second.id == pair.a->id };
            if (!isCallbackForA && !isCallbackForB)
                continue;

            if (isCallbackForA) {
                callback.second.cb(*manifold, pair.b->colliderInfo->go);
            } else {
                auto manifoldCopy { manifold };
                // TODO: Noticed that this -normal inversion breaks physics behaviour
                // consider passing the a/b order of objects done during intersection test too?
                manifoldCopy->normal = -manifoldCopy->normal;
                callback.second.cb(*manifoldCopy, pair.a->colliderInfo->go);
            }
        }

        // Ensure that pairs that are colliding & not currently tracked
        // as colliding are tracked
        if (findResult == m_currentlyColliding.end()) {
            m_currentlyColliding.emplace_back(pair);
        }
    }
}

void CollisionSolver::updateDebugVertices()
{
    // Clear previous vertices
    for (auto& va : m_debugVertices)
        va.clear();

    // Append vertical lines
    auto& gridVA = m_debugVertices[size_t(DebugGraphicsLayers::Grid)];
    auto& occupiedVA = m_debugVertices[size_t(DebugGraphicsLayers::Occupied)];

    for (size_t i { 0 }; i < m_gridSize.x; ++i) {
        std::array<sf::Vertex, 2> line;
        line[0].position = m_position + sf::Vector2f { float(i * m_cellSize.x), 0.f };
        line[1].position = m_position + sf::Vector2f { float(i * m_cellSize.x), float(m_gridSize.y * m_cellSize.x) };

        line[0].color = sf::Color::Red;
        line[1].color = sf::Color::Red;
        gridVA.append(line[0]);
        gridVA.append(line[1]);
    }

    // Append horizontal lines
    for (size_t i { 0 }; i < m_gridSize.y; ++i) {
        std::array<sf::Vertex, 2> line;
        line[0].position = m_position + sf::Vector2f { 0.f, float(i * m_cellSize.y) };
        line[1].position = m_position + sf::Vector2f { float(m_gridSize.x * m_cellSize.x), float(i * m_cellSize.y) };

        line[0].color = sf::Color::Red;
        line[1].color = sf::Color::Red;
        gridVA.append(line[0]);
        gridVA.append(line[1]);
    }

    // Iterate over all grid cells and identify
    // if the cell is occupied or not. If it is
    // then we'll generate a debug shape to be draw
    // to indicate it's occupied
    for (size_t i { 0 }; i < m_gridSize.x; ++i) {
        for (size_t j { 0 }; j < m_gridSize.y; ++j) {
            const auto index = size_t { i + j * m_gridSize.x };
            const auto& cell = m_gridCells[index];

            if (cell.entries.empty())
                continue;

            std::array<sf::Vertex, 6> quad;

            quad[0].position = m_position + sf::Vector2f { float(i * m_cellSize.x), float(j * m_cellSize.y) };
            quad[1].position = m_position + sf::Vector2f { float((i + 1) * m_cellSize.x), float(j * m_cellSize.y) };
            quad[2].position = m_position + sf::Vector2f { float((i + 1) * m_cellSize.x), float((j + 1) * m_cellSize.y) };

            quad[3].position = m_position + sf::Vector2f { float((i + 1) * m_cellSize.x), float((j + 1) * m_cellSize.y) };
            quad[4].position = m_position + sf::Vector2f { float(i * m_cellSize.x), float((j + 1) * m_cellSize.y) };
            quad[5].position = m_position + sf::Vector2f { float(i * m_cellSize.x), float(j * m_cellSize.y) };

            for (size_t k { 0 }; k < 6; ++k) {
                quad[k].color = { 0, 255, 0, 100 };
                occupiedVA.append(quad[k]);
            }
        }
    }
}

void CollisionSolver::updateDebugShapes()
{
    constexpr sf::Color ColliderColor { sf::Color::Magenta.r, sf::Color::Magenta.g, sf::Color::Magenta.b, 150 };

    m_colliderDebugShapes.clear();
    m_debugNormals.clear();

    for (size_t i { 0 }; i < m_colliders.size(); ++i) {
        auto& colliderInfo { m_colliders[i].colliderInfo };
        if (!colliderInfo->go->isActive())
            continue;

        switch (colliderInfo->type) {
        case ColliderType::Circle: {
            const auto radius { colliderInfo->transformable->getScale().x * std::get<CircleMetadata>(colliderInfo->metadata).radius };
            const auto pos { colliderInfo->transformable->getPosition() };
            auto circle = new sf::CircleShape(radius);
            circle->setOrigin({ radius, radius });
            circle->setPosition(pos);
            m_colliderDebugShapes.emplace_back(circle);
        } break;
        case ColliderType::AABB: {
            const auto bounds { colliderInfo->transformable->getTransform().transformRect(std::get<AABBMetadata>(colliderInfo->metadata).bounds) };
            auto rect = new sf::RectangleShape(bounds.getSize());
            rect->setPosition(bounds.getPosition());
            m_colliderDebugShapes.emplace_back(rect);
        } break;
        case ColliderType::OBB: {
            const auto& metadata { std::get<OBBMetadata>(colliderInfo->metadata) };
            auto convexShape = new sf::ConvexShape(4);
            const std::array<sf::Vector2f, 4> vertices { ComputeOBBVerticesTransformed(
                metadata.halfBounds * 2.f, colliderInfo->transformable->getTransform(), colliderInfo->transformable->getOrigin()) };
            convexShape->setPoint(0, vertices[0]);
            convexShape->setPoint(1, vertices[1]);
            convexShape->setPoint(2, vertices[2]);
            convexShape->setPoint(3, vertices[3]);
            m_colliderDebugShapes.emplace_back(convexShape);
        } break;
        case ColliderType::Poly: {
            // auto& c2Collider = std::get<c2Poly>(colliderInfo->c2Collider);
            // auto convexShape = new sf::ConvexShape(c2Collider.count);
            // for (size_t j { 0 }; j < size_t(c2Collider.count); ++j) {
            //     convexShape->setPoint(j, c2vToVector2f(c2Collider.verts[j]));
            // }
            // m_colliderDebugShapes.emplace_back(convexShape);

            // for (size_t j { 0 }; j < size_t(c2Collider.count); ++j) {
            //     const auto k = (j + 1) == size_t(c2Collider.count) ? 0 : (j + 1);
            //     const auto lineMidPoint { c2Div(c2Add(c2Collider.verts[j], c2Collider.verts[k]), 2.f) };

            //    const auto normal = c2vToVector2f(c2Norm(c2Collider.norms[j]));
            //    m_debugNormals.append(sf::Vertex { c2vToVector2f(lineMidPoint), sf::Colour::Yellow, {} });
            //    m_debugNormals.append(sf::Vertex { c2vToVector2f(lineMidPoint) + normal * 20.f, sf::Colour::Yellow, {} });
            //}
        } break;
        default:
            assert(false);
            break;
        }
        m_colliderDebugShapes.back()->setFillColor(ColliderColor);
    }
}

auto CollisionSolver::queryBoxForIntersections(const sf::FloatRect& rect) -> bool
{
    const auto gridPos = sf::Vector2u(rect.getCenter());
    gridPos.cwiseDiv(getCellSize());

    auto& cell = m_gridCells[u64(gridPos.x + gridPos.y * m_gridSize.x)];

    for (auto& cellEntry : cell.entries) {
        const auto cellEntryAABB = BoundingBoxFromCollider(*cellEntry->colliderInfo);
        if (cellEntryAABB.findIntersection(rect)) {
            return true;
        }
    }
    return false;
}
} // nce namespace
