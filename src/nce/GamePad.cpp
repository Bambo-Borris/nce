#include "../stdafx.h"

#include "nce/GamePad.hpp"

#include "GamePadImpl.hpp"

namespace nce {
GamePad::GamePad() { m_impl = std::make_unique<GamePadImpl>(); }
GamePad::~GamePad() = default;

void GamePad::update() { m_impl->update(); }

auto GamePad::buttonPressed(Buttons button) const -> bool { return m_impl->buttonPressed(button); }

auto GamePad::buttonReleased(Buttons button) const -> bool { return m_impl->buttonReleased(button); }

auto GamePad::buttonHeld(Buttons button) const -> bool { return m_impl->buttonHeld(button); }

auto GamePad::getLeftStickInfo() const -> JoystickInfo { return m_impl->get2DAxisInfo(GamePad::Axis::LeftStick); }

auto GamePad::getRightStickInfo() const -> JoystickInfo { return m_impl->get2DAxisInfo(GamePad::Axis::RightStick); }

auto GamePad::getLeftTriggerValue() const -> f32 { return m_impl->get1DAxisInfo(GamePad::Axis::LeftTrigger); }

auto GamePad::getRightTriggerValue() const -> f32 { return m_impl->get1DAxisInfo(GamePad::Axis::RightTrigger); }

auto GamePad::hasValidConnectedDevice() const -> bool { return m_impl->hasValidConnectedDevice(); }

void GamePad::handleEvents(sf::Event event) { m_impl->handleEvents(event); }
} // nce namespace
