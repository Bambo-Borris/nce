#include "../../stdafx.h"

#include "../GamePadImpl.hpp"
#include "nce/NCE.hpp"

#include <GameInput.h>
#include <Windows.h>
#include <Xinput.h>
#include <complex>

namespace nce {

constexpr f32 LEFT_STICK_DEAD_ZONE { XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE };
constexpr f32 RIGHT_STICK_DEAD_ZONE { XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE };
constexpr f32 TRIGGER_DEAD_ZONE { XINPUT_GAMEPAD_TRIGGER_THRESHOLD };

struct GamePadImpl::PrivData {
    PrivData()
    {
        GameInputCreate(&gameInput);
        assert(gameInput);
    }

    ~PrivData()
    {
        if (gamepad)
            gamepad->Release();
        if (gameInput)
            gameInput->Release();
    }

    IGameInput* gameInput = nullptr;
    IGameInputDevice* gamepad = nullptr;
};

GamePadImpl::GamePadImpl() { m_privData.reset(NCE_NEW PrivData); }

GamePadImpl::~GamePadImpl() = default;

void GamePadImpl::update()
{
    m_pressedButtons.clear();
    m_releasedButtons.clear();

    XINPUT_STATE state;
    ZeroMemory(&state, sizeof(XINPUT_STATE));

    if (!m_userIndex) {
        for (DWORD i = 0; i < XUSER_MAX_COUNT; i++) {
            const auto result { XInputGetState(i, &state) };

            if (result != ERROR_SUCCESS) {
                continue;
            }
            m_userIndex = i;
            spdlog::trace("Pad connected in slot {}", *m_userIndex);
        }
    }

    if (m_userIndex) {

        auto result { XInputGetState(*m_userIndex, &state) };
        if (result == ERROR_DEVICE_NOT_CONNECTED) {
            m_userIndex.reset();
            return;
        }

        const sf::Vector2f leftStick { f32(state.Gamepad.sThumbLX), f32(state.Gamepad.sThumbLY) };
        sf::Vector2f rightStick { f32(state.Gamepad.sThumbRX), f32(state.Gamepad.sThumbRY) };

        m_2dAxisValues[GamePad::Axis::LeftStick] = getStickAxis(leftStick, true);
        m_2dAxisValues[GamePad::Axis::RightStick] = getStickAxis(rightStick, false);

        // Handle normalized axis values
        // const auto normalizedLeftStick = getNormalizedStickAxis(leftStick, true);
        // m_axisValuesNormalized[GamePad::Axis::LeftStickX] = normalizedLeftStick.x;
        // m_axisValuesNormalized[GamePad::Axis::LeftStickY] = normalizedLeftStick.y;
        //
        // const auto normalizedRightStick = getNormalizedStickAxis(rightStick, false);
        // m_axisValuesNormalized[GamePad::Axis::RightStickX] = normalizedRightStick.x;
        // m_axisValuesNormalized[GamePad::Axis::RightStickY] = normalizedRightStick.y;

        auto leftTrigger { f32(state.Gamepad.bLeftTrigger) };
        leftTrigger = getNormalizedTriggerAxis(leftTrigger);
        m_1dAxisValues[GamePad::Axis::LeftTrigger] = leftTrigger;

        auto rightTrigger { f32(state.Gamepad.bRightTrigger) };
        rightTrigger = getNormalizedTriggerAxis(rightTrigger);
        m_1dAxisValues[GamePad::Axis::RightTrigger] = rightTrigger;

        // Handle axis values
        // {
        //     const auto normalizedLeftStick = getStickAxis(leftStick, true);
        //     m_axisValues[GamePad::Axis::LeftStickX] = normalizedLeftStick.x;
        //     m_axisValues[GamePad::Axis::LeftStickY] = normalizedLeftStick.y;
        // }

        // Handle button press/release/held events
        XINPUT_KEYSTROKE keystroke;
        ZeroMemory(&keystroke, sizeof(XINPUT_KEYSTROKE));
        result = XInputGetKeystroke((*m_userIndex), 0, &keystroke);

        if (result == ERROR_EMPTY)
            return;

        if (keystroke.UserIndex != (*m_userIndex))
            return;

        if (keystroke.Flags & XINPUT_KEYSTROKE_KEYDOWN && !(keystroke.Flags & XINPUT_KEYSTROKE_REPEAT)) {
            const auto button { buttonIDToEnum(keystroke.VirtualKey) };
            if (button != GamePad::Buttons::UnsupportedButton) {
                m_pressedButtons.emplace(buttonIDToEnum(keystroke.VirtualKey));
                m_heldButtons.emplace(buttonIDToEnum(keystroke.VirtualKey));
            }
        } else if (keystroke.Flags & XINPUT_KEYSTROKE_KEYUP && !(keystroke.Flags & XINPUT_KEYSTROKE_REPEAT)) {
            const auto button { buttonIDToEnum(keystroke.VirtualKey) };
            if (button != GamePad::Buttons::UnsupportedButton) {
                m_releasedButtons.emplace(buttonIDToEnum(keystroke.VirtualKey));

                auto findResult { m_heldButtons.find(buttonIDToEnum(keystroke.VirtualKey)) };
                if (findResult != m_heldButtons.end())
                    m_heldButtons.erase(findResult);
            }
        }
    }
}

auto GamePadImpl::buttonPressed(GamePad::Buttons button) const -> bool { return m_pressedButtons.contains(button); }

auto GamePadImpl::buttonReleased(GamePad::Buttons button) const -> bool { return m_releasedButtons.contains(button); }

auto GamePadImpl::buttonHeld(GamePad::Buttons button) const -> bool { return m_heldButtons.contains(button); }

auto GamePadImpl::get2DAxisInfo(GamePad::Axis axis) const -> GamePad::JoystickInfo
{
    assert(axis == GamePad::Axis::LeftStick || axis == GamePad::Axis::RightStick);
    return m_2dAxisValues.at(axis);
}

auto GamePadImpl::get1DAxisInfo(GamePad::Axis axis) const -> f32
{
    assert(axis != GamePad::Axis::LeftStick && axis != GamePad::Axis::RightStick);
    return m_1dAxisValues.at(axis);
}

// auto GamePadImpl::getAxisNormalizedPosition(GamePad::Axis axis) const -> f32
// {
//     if (!m_axisValuesNormalized.contains(axis))
//         assert(false);
//
//     return m_axisValuesNormalized.at(axis);
// }
//
// auto GamePadImpl::getAxisPosition(GamePad::Axis axis) const -> f32
// {
//     if (!m_axisValues.contains(axis))
//         assert(false);
//
//     return m_axisValues.at(axis);
// }

auto GamePadImpl::buttonIDToEnum(uint16_t id) -> GamePad::Buttons
{
    auto out = GamePad::Buttons::UnsupportedButton;
    switch (id) {
    case VK_PAD_A:
        out = GamePad::Buttons::A;
        break;
    case VK_PAD_B:
        out = GamePad::Buttons::B;
        break;
    case VK_PAD_X:
        out = GamePad::Buttons::X;
        break;
    case VK_PAD_Y:
        out = GamePad::Buttons::Y;
        break;
    case VK_PAD_RSHOULDER:
        out = GamePad::Buttons::RightShoulder;
        break;
    case VK_PAD_LSHOULDER:
        out = GamePad::Buttons::LeftShoulder;
        break;
    case VK_PAD_LTRIGGER:
        out = GamePad::Buttons::LeftTrigger;
        break;
    case VK_PAD_RTRIGGER:
        out = GamePad::Buttons::RightTrigger;
        break;

    case VK_PAD_DPAD_UP:
        out = GamePad::Buttons::DirectionUp;
        break;
    case VK_PAD_DPAD_DOWN:
        out = GamePad::Buttons::DirectionDown;
        break;
    case VK_PAD_DPAD_LEFT:
        out = GamePad::Buttons::DirectionLeft;
        break;
    case VK_PAD_DPAD_RIGHT:
        out = GamePad::Buttons::DirectionRight;
        break;
    case VK_PAD_START:
        out = GamePad::Buttons::Start;
        break;
    case VK_PAD_BACK:
        out = GamePad::Buttons::Back;
        break;
    case VK_PAD_LTHUMB_PRESS:
        out = GamePad::Buttons::LeftStickClick;
        break;
    case VK_PAD_RTHUMB_PRESS:
        out = GamePad::Buttons::RightStickClick;
        break;
    case VK_PAD_LTHUMB_LEFT:
        out = GamePad::Buttons::LeftStickLeft;
        break;
    case VK_PAD_LTHUMB_UP:
        out = GamePad::Buttons::LeftStickUp;
        break;
    case VK_PAD_LTHUMB_DOWN:
        out = GamePad::Buttons::LeftStickDown;
        break;
    case VK_PAD_LTHUMB_RIGHT:
        out = GamePad::Buttons::LeftStickRight;
        break;
    case VK_PAD_LTHUMB_UPLEFT:
        out = GamePad::Buttons::LeftStickUpLeft;
        break;
    case VK_PAD_LTHUMB_UPRIGHT:
        out = GamePad::Buttons::LeftStickUpRight;
        break;
    case VK_PAD_LTHUMB_DOWNRIGHT:
        out = GamePad::Buttons::LeftStickDownRight;
        break;
    case VK_PAD_LTHUMB_DOWNLEFT:
        out = GamePad::Buttons::LeftStickDownLeft;
        break;
    case VK_PAD_RTHUMB_UP:
        out = GamePad::Buttons::RightStickUp;
        break;
    case VK_PAD_RTHUMB_DOWN:
        out = GamePad::Buttons::RightStickDown;
        break;
    case VK_PAD_RTHUMB_RIGHT:
        out = GamePad::Buttons::RightStickRight;
        break;
    case VK_PAD_RTHUMB_LEFT:
        out = GamePad::Buttons::RightStickLeft;
        break;
    case VK_PAD_RTHUMB_UPLEFT:
        out = GamePad::Buttons::RightStickUpLeft;
        break;
    case VK_PAD_RTHUMB_UPRIGHT:
        out = GamePad::Buttons::RightStickUpRight;
        break;
    case VK_PAD_RTHUMB_DOWNRIGHT:
        out = GamePad::Buttons::RightStickDownRight;
        break;
    case VK_PAD_RTHUMB_DOWNLEFT:
        out = GamePad::Buttons::RightStickDownLeft;
        break;
    default:
        break;
    }
    return out;
}

auto GamePadImpl::getNormalizedTriggerAxis(f32 raw) const -> f32
{
    auto out { 0.f };
    if (raw > TRIGGER_DEAD_ZONE) {
        if (raw > 255)
            raw = 255.f;
        raw -= TRIGGER_DEAD_ZONE;
        out = raw / (255 - TRIGGER_DEAD_ZONE);
    }

    return out;
}

auto GamePadImpl::getStickAxis(const sf::Vector2f& rawAxis, bool isLeftStick) const -> GamePad::JoystickInfo
{
    const auto deadzone { isLeftStick ? LEFT_STICK_DEAD_ZONE : RIGHT_STICK_DEAD_ZONE };
    f32 normalizedMagnitude = 0;
    f32 magnitude = rawAxis.length();
    auto normalizedRawAxis = rawAxis;
    if (magnitude != 0.f) {
        normalizedRawAxis /= magnitude;
        if (magnitude > deadzone) {
            if (magnitude > 32767.f)
                magnitude = 32767.f;

            magnitude -= deadzone;
            normalizedMagnitude = magnitude / (32767.f - deadzone);
        } else {
            magnitude = 0.f;
            normalizedMagnitude = 0.f;
        }
    }
    return { .direction = normalizedRawAxis, .magnitude = normalizedMagnitude };
}

auto GamePadImpl::hasValidConnectedDevice() const -> bool { return m_userIndex.has_value(); }

void GamePadImpl::handleEvents(sf::Event event)
{
    // Do nothing cause XInput is handling our input on this device
    NCE_UNUSED(event);
}
} // nce namespace