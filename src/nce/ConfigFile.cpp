#include "../stdafx.h"

#include "nce/ConfigFile.hpp"

#include <fstream>
#include <sstream>

namespace nce {
ConfigFile::ConfigFile(const std::filesystem::path& path, const ConfigFileSpec& specification)
    : m_path(path)
    , m_specification(specification)
{
    for (const auto& [k, v] : specification) {
        m_data[k].type = v;
        switch (v) {
        case ConfigFieldType::Int:
            m_data[k].value = 0;
            break;
        case ConfigFieldType::Float:
            m_data[k].value = 0.f;
            break;
        case ConfigFieldType::String:
            m_data[k].value = std::string();
            break;
        }
    }
}

bool ConfigFile::load()
{
    std::ifstream inputFile;
    inputFile.open(m_path.string(), std::ios::in);

    if (inputFile.fail())
        return false;

    while (!inputFile.eof()) {
        std::array<char, 500> line;
        memset(line.data(), 0, line.size());
        inputFile.getline(line.data(), line.size());

        // Now we've parsed the data from the file into
        // an array, we need to tokenize the string
        std::stringstream ss(line.data());
        std::string intermediate;
        std::vector<std::string> tokens;
        while (std::getline(ss, intermediate, ' ')) {
            tokens.push_back(intermediate);
        }

        // If we found nothing, we'll just continue on
        if (tokens.empty())
            continue;
        // We found a comment marker, we'll skip this line
        if (tokens[0][0] == '#')
            continue;

        if (tokens.size() < 2) {
            spdlog::error("Invalid number of tokens for line contents: \n{}", line.data());
            return false;
        }

        // Now we have our correctly tokenized string
        // we need to verify the key is in our spec
        const auto findResult = m_specification.find(tokens[0]);
        if (findResult == m_specification.end()) {
            spdlog::error("Key entry not found in config file specification, key is: {}", tokens[0]);
            return false;
        }

        switch (findResult->second) {
        case ConfigFieldType::Int:
            m_data[tokens[0]].type = ConfigFieldType::Int;
            m_data[tokens[0]].value = std::atoi(tokens[1].data());
            break;
        case ConfigFieldType::Float:
            m_data[tokens[0]].type = ConfigFieldType::Float;
            m_data[tokens[0]].value = static_cast<float>(std::atof(tokens[1].data()));
            break;
        case ConfigFieldType::String: {
            m_data[tokens[0]].type = ConfigFieldType::String;
            std::string composed;
            for (size_t i { 1 }; i < tokens.size(); ++i) {
                composed += tokens[i];
                if (i != tokens.size() - 1)
                    composed += ' ';
            }
            m_data[tokens[0]].value = composed;
        } break;
        default:
            assert(false);
            break;
        }
    }

    inputFile.close();

    if (m_data.empty())
        return false;

    return true;
}

void ConfigFile::save()
{
    std::ofstream outputFile;
    outputFile.open(m_path.string(), std::ios::out | std::ios::trunc);

    if (outputFile.fail()) {
        spdlog::error("Unable to open output file {} to save to disk", m_path.string());
        return;
    }

    for (const auto& [k, v] : m_data) {
        std::string line;
        line += k;
        line += ' ';
        switch (v.type) {
        case ConfigFieldType::Int:
            line += std::to_string(std::any_cast<const int&>(v.value));
            break;
        case ConfigFieldType::Float:
            line += std::to_string(std::any_cast<const float&>(v.value));
            break;
        case ConfigFieldType::String:
            line += std::any_cast<const std::string&>(v.value);
            break;
        default:
            assert(false);
        }
        line += '\n';
        outputFile << line;
    }
    outputFile.close();
}
} // nce namespace
