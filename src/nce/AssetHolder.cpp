#include "../stdafx.h"

#include "PackedZIP.hpp"
#include "nce/App.hpp"
#include "nce/AssetHolder.hpp"

#include <SFML/System/Err.hpp>
#include <fstream>
#include <mutex>

namespace nce {
AssetHolder::~AssetHolder() { }

sf::Font* AssetHolder::getFont(std::string_view id)
{
    if (m_fontMap.contains(id.data())) {
        return &m_fontMap[id.data()].font;
    }

    auto loadResult = GetDataFromPackedZIP(*m_assetInfo, id, AssetType::Font);
    if (!loadResult.has_value()) {
        throw std::runtime_error(std::format("Font loading failed with error: {}", loadResult.error()));
    }

    sf::Font tempFont;

    if (!tempFont.loadFromMemory(loadResult->data(), loadResult->size()))
        throw std::runtime_error("Unable to load font from data buffer!");

    m_fontMap[id.data()].font = std::move(tempFont);
    m_fontMap[id.data()].fileContents = std::move(loadResult.value());
    m_fontMap[id.data()].font.setSmooth(false);
    return &m_fontMap[id.data()].font;
}

sf::Texture* AssetHolder::getTexture(std::string_view id)
{
    {
        std::scoped_lock scoped { m_textureMapMutex };
        if (m_textureMap.contains(id.data()))
            return &m_textureMap[id.data()];
    }
    auto imageData = GetDataFromPackedZIP(*m_assetInfo, id, AssetType::Texture);
    if (!imageData.has_value()) {
        throw std::runtime_error(std::format("Texture loading failed with error: {}", imageData.error()));
    }

    if (m_app->isLoading()) {

        sf::Image intermediate;

        if (!intermediate.loadFromMemory(imageData->data(), imageData->size()))
            throw std::runtime_error("Unable to load image from data buffer!");
        {
            std::scoped_lock scoped { m_imageQueueMutex };
            m_imageQueue.emplace(id.data(), std::move(intermediate));
        }

        // We don't know how long it will take for the image queue to be loaded
        // so we'll spin around for a while sleeping, and then trying to get our
        // texture. If it's taking too long then we'll report it as a failure
        bool foundTexture = false;
        unsigned int retryCount { 60000 };
        do {

            using namespace std::chrono_literals;
            std::this_thread::sleep_for(1ms);
            std::scoped_lock scoped { m_textureMapMutex };

            foundTexture = m_textureMap.contains(id.data());

            if (m_imageQueue.empty() && !foundTexture)
                break;

            --retryCount;

        } while (!foundTexture && retryCount != 0);
        if (!foundTexture)
            throw std::runtime_error(fmt::format("Failed to load texture at path {}", id.data()));
    } else {
        if (!m_textureMap[id.data()].loadFromMemory(imageData->data(), imageData->size()))
            throw std::runtime_error("Unable to load texture from memory!");
    }

    return &m_textureMap[id.data()];
}

sf::SoundBuffer* AssetHolder::getSoundBuffer(std::string_view id)
{
    if (m_soundBufferMap.contains(id.data())) {
        return &m_soundBufferMap[id.data()];
    }

    auto soundData = GetDataFromPackedZIP(*m_assetInfo, id, AssetType::Sound);
    if (!soundData.has_value()) {
        throw std::runtime_error(std::format("Font loading failed with error: {}", soundData.error()));
    }

    sf::SoundBuffer tempSoundBuffer;
    if (!tempSoundBuffer.loadFromMemory(soundData->data(), soundData->size()))
        throw std::runtime_error("Unable to load sound from data buffer!");

    m_soundBufferMap[id.data()] = tempSoundBuffer;

    return &m_soundBufferMap[id.data()];
}

sf::Music* AssetHolder::getMusic(const std::filesystem::path& path)
{
    const auto pathString = path.string();
    const auto result = m_musicMap.find(pathString);
    if (result != m_musicMap.end())
        return &(*result).second;

    if (!std::filesystem::exists(path))
        throw std::runtime_error(fmt::format("Music at path {} not found", pathString));

    if (!m_musicMap[pathString].openFromFile(pathString))
        throw std::runtime_error(fmt::format("Unable to load music {}", pathString));

    return &m_musicMap[pathString];
}

sf::Image* AssetHolder::getImage(const std::filesystem::path& path)
{
    const auto pathString = path.string();
    const auto result = m_imageMap.find(pathString);
    if (result != m_imageMap.end())
        return &(*result).second;

    if (!std::filesystem::exists(path))
        throw std::runtime_error(fmt::format("Image at path {} not found", pathString));

    if (!m_imageMap[pathString].loadFromFile(pathString))
        throw std::runtime_error(fmt::format("Unable to load image {}", pathString));

    return &m_imageMap[pathString];
}

sf::Shader* AssetHolder::getShader(std::string_view id, sf::Shader::Type type)
{
    {
        std::scoped_lock scoped { m_shaderMapMutex };
        if (m_textureMap.contains(id.data()))
            return &m_shaderMap[id.data()];
    }

    auto shaderData = GetDataFromPackedZIP(*m_assetInfo, id, AssetType::Shader);
    if (!shaderData.has_value()) {
        throw std::runtime_error(std::format("Shader loading failed with error: {}", shaderData.error()));
    }

    {
        std::scoped_lock scoped { m_shaderMapMutex };
        m_shaderQueue.push({ id.data(), { std::string(), type } });
        m_shaderQueue.back().second.shaderSource.resize(shaderData->size());
        static_assert(sizeof(m_shaderQueue.back().second.shaderSource[0]) == sizeof(std::byte), "Sizes of byte & std::string element must match!");
#ifdef _MSC_VER
        memcpy_s(m_shaderQueue.back().second.shaderSource.data(), m_shaderQueue.back().second.shaderSource.size(), shaderData->data(), shaderData->size());
#else
        memcpy(m_shaderQueue.back().second.shaderSource.data(), shaderData->data(), shaderData->size());
#endif
        //        m_shaderQueue.back().second.shaderSource.resize(shaderData->size());
    }

    // We don't know how long it will take for the image queue to be loaded
    // so we'll spin around for a while sleeping, and then trying to get our
    // texture. If it's taking too long then we'll report it as a failure
    bool foundShader = false;
    unsigned int retryCount { 100 };
    do {

        using namespace std::chrono_literals;
        std::this_thread::sleep_for(5ms);

        std::scoped_lock scoped { m_shaderMapMutex };

        foundShader = m_shaderMap.contains(id.data());

        if (m_shaderQueue.empty() && !foundShader)
            break;

        --retryCount;

    } while (!foundShader && retryCount != 0);

    if (!foundShader)
        throw std::runtime_error(fmt::format("Failed to load shader with ID {}", id.data()));

    return &m_shaderMap[id.data()];
}

void AssetHolder::processTextureQueue()
{
    if (m_imageQueue.empty())
        return;

    // we will always want to yield on both.
    const auto unlockAll = [this]() {
        m_imageQueueMutex.unlock();
        m_textureMapMutex.unlock();
    };

    // If we can't grab the locks we'll yield, we want to be
    // able to continue displaying a loading screen & updating it
    if (!m_imageQueueMutex.try_lock())
        return;

    if (!m_textureMapMutex.try_lock()) {
        m_imageQueueMutex.unlock();
        return;
    }

    // We'll process one texture on the queue
    // then stop
    sf::Texture t;
    if (!t.loadFromImage(m_imageQueue.front().second)) {
        unlockAll();
        spdlog::error(std::format("Unable to load {} from image to texture", m_imageQueue.front().first));
        m_imageQueue.pop();
        return;
    }

    m_textureMap[m_imageQueue.front().first] = std::move(t);
    m_imageQueue.pop();

    unlockAll();
}

void AssetHolder::processShaderQueue()
{
    if (m_shaderQueue.empty())
        return;

    // we will always want to yield on both.
    const auto unlockAll = [this]() {
        m_shaderSourceMutex.unlock();
        m_shaderMapMutex.unlock();
    };

    // If we can't grab the locks we'll yield, we want to be
    // able to continue displaying a loading screen & updating it
    if (!m_shaderSourceMutex.try_lock())
        return;

    if (!m_shaderMapMutex.try_lock()) {
        m_shaderSourceMutex.unlock();
        return;
    }

    // We'll process one shader on the queue
    // then stop
    sf::Shader s;
    auto& entry { m_shaderQueue.front().second };
    auto defaultStr = sf::err().rdbuf();
    std::stringstream errStream;
    sf::err().rdbuf(errStream.rdbuf());
    if (!s.loadFromMemory(entry.shaderSource, entry.type)) {
        unlockAll();
        spdlog::error("{}", errStream.str());
        spdlog::error(std::format("Unable to load {} from shader source to shader instance", m_shaderQueue.front().first));
        m_shaderQueue.pop();
        return;
    }

    m_shaderMap[m_shaderQueue.front().first] = std::move(s);
    m_shaderQueue.pop();
    sf::err().rdbuf(defaultStr);
    unlockAll();
}

void AssetHolder::unloadAssets()
{
    // Need to grab these just in case something occurs on another thread
    std::scoped_lock lock(m_imageQueueMutex, m_shaderSourceMutex, m_textureMapMutex, m_shaderMapMutex);
    m_fontMap.clear();
    m_textureMap.clear();
    m_soundBufferMap.clear();
    m_musicMap.clear();
    m_imageMap.clear();
    m_shaderMap.clear();
}

AssetHolder::AssetHolder()
{
    AssetJSON assetJSON(AssetJSON::FileOrigin::Zip);
    if (auto result = assetJSON.getJSONData(); !result.has_value())
        throw std::runtime_error(result.error());
    else
        m_assetInfo.emplace(result.value());
}
} // nce namespace
