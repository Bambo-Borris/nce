#include "../stdafx.h"

#include "nce/App.hpp"
#include "nce/NCE.hpp"

#if _MSC_VER
#pragma comment(lib, "rpcrt4.lib")
#include <rpc.h>
#include <windows.h>
#endif

namespace nce {

auto GenerateUUID() -> std::string
{
    std::string outputStr;

#if _MSC_VER
    UUID uuid;
    char* str { nullptr };

    if (UuidCreate(&uuid) != RPC_S_OK)
        assert(false); // Unable to create UUID

    if (UuidToStringA(&uuid, reinterpret_cast<RPC_CSTR*>(&str)) != RPC_S_OK)
        assert(false); // Unable to convert UUID to string

    outputStr = str;

    if (RpcStringFreeA((RPC_CSTR*)&str) != RPC_S_OK)
        assert(false); // Unable to clean up UUID string
#else
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> dis(0, 15);
    static std::uniform_int_distribution<> dis2(8, 11);

    std::stringstream ss;
    int i;
    ss << std::hex;
    for (i = 0; i < 8; i++) {
        ss << dis(gen);
    }
    ss << "-";
    for (i = 0; i < 4; i++) {
        ss << dis(gen);
    }
    ss << "-4";
    for (i = 0; i < 3; i++) {
        ss << dis(gen);
    }
    ss << "-";
    ss << dis2(gen);
    for (i = 0; i < 3; i++) {
        ss << dis(gen);
    }
    ss << "-";
    for (i = 0; i < 12; i++) {
        ss << dis(gen);
    };
    outputStr = ss.str();
#endif
    return outputStr;
}

auto WorldPosToScreenPos(sf::Vector2f worldPos) -> sf::Vector2i
{
    const auto window = NCE_APP().getRenderWindow();
    const auto rTexture = NCE_APP().getRenderTexture();

    const auto rTextureScreenPos = rTexture->mapCoordsToPixel(worldPos);
    const auto uv = sf::Vector2f(rTextureScreenPos).cwiseDiv(sf::Vector2f(rTexture->getSize()));

    return sf::Vector2i(sf::Vector2f(window->getSize()).cwiseMul(uv));
}

} // nce namespace