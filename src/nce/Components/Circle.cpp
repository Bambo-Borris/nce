#include "../../stdafx.h"

#include "nce/Components/Circle.hpp"

#include "nce/Components/Tags.hpp"
#include "nce/GameObject.hpp"

namespace nce {
Circle::Circle(f32 radius, GameObject* parentObject)
    : Renderable(ComponentNames[ComponentTags::Circle], ComponentTags::Circle, parentObject)
{
    m_shape.setRadius(radius);
}

void Circle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getParentObject()->getTransform();
    target.draw(m_shape, states);
}
} // nce namespace