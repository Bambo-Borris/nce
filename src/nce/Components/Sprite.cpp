#include "../../stdafx.h"

#include "nce/Components/Sprite.hpp"

#include "nce/Components/Tags.hpp"

namespace nce {
Sprite::Sprite(sf::Texture* texture, GameObject* parentObject)
    : Renderable(ComponentNames[ComponentTags::Sprite], ComponentTags::Sprite, parentObject)
    , m_texture(texture)
{
    assert(m_texture);
}

ErrResult Sprite::init()
{
    if (!m_texture)
        return ErrVal("Unable to initialise sprite, null texture instance encountered");

    m_sprite.reset(NCE_NEW sf::Sprite(*m_texture));

    return Renderable::init();
}

void Sprite::setTexture(sf::Texture* texture)
{
    assert(texture);
    if (!texture) {
        spdlog::error("Null texture instance passed to nce::Sprite!");
        return;
    }

    m_sprite->setTexture(*texture);
}

void Sprite::setTextureRect(const sf::IntRect& rect) { m_sprite->setTextureRect(rect); }

void Sprite::setFillColor(const sf::Color& color) { m_sprite->setColor(color); }

sf::FloatRect Sprite::getLocalBounds() const { return m_sprite->getLocalBounds(); }

sf::FloatRect Sprite::getGlobalBounds() const { return getParentObject()->getTransform().transformRect(getLocalBounds()); }

void Sprite::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getParentObject()->getTransform();
    target.draw(*m_sprite, states);
}
} // nce namespace