#include "../../stdafx.h"

#include "nce/AppState.hpp"
#include "nce/Components/PhysicsObject.hpp"
#include "nce/Components/Tags.hpp"
#include "nce/GameObject.hpp"

namespace nce {
PhysicsObject::PhysicsObject(GameObject* parentObject, BehaviourType type)
    : Component(ComponentNames[ComponentTags::PhysicsObject], ComponentTags::PhysicsObject, parentObject)
    , m_type(type)
{
    NCE_UNUSED(m_type); // Supresses warning on Clang, we will use this var when physics is expanded
}

ErrResult PhysicsObject::onEnterState()
{
    getParentObject()->getAppState()->getPhysicsWorld().addObject(this);
    return Component::onEnterState();
}

ErrResult PhysicsObject::onExitState()
{
    getParentObject()->getAppState()->getPhysicsWorld().removeObject(this);
    return Component::onExitState();
}

void PhysicsObject::fixedUpdate(sf::Time dt, sf::Time fixedDt)
{
    NCE_UNUSED(dt);
    auto& physicsWorld = getParentObject()->getAppState()->getPhysicsWorld();
    // Linear integration step
    const auto elapsed = (fixedDt.asSeconds() * m_dtMultiplier);
    const auto acceleration = m_force * m_invMass;

    m_linearVelocity += acceleration * elapsed;

    // Oriented integration step
    m_angularVelocity += m_torque * m_invInertia * elapsed;
    m_linearVelocity = m_linearVelocity.rotatedBy(sf::radians(m_angularVelocity * elapsed));
    getParentObject()->move((m_linearVelocity * physicsWorld.getWorldScale()) * elapsed);
    getParentObject()->rotate(sf::radians(m_angularVelocity * elapsed));

    clearForces();
    clearTorque();
}

void PhysicsObject::applyForce(const sf::Vector2f& force) { m_force += force; }

void PhysicsObject::applyTorque(float torque) { m_torque += torque; }

void PhysicsObject::setActivity(bool active) { m_active = active; }

void PhysicsObject::setMass(float mass)
{
    m_mass = mass;
    if (mass != 0.f)
        m_invMass = 1.f / mass;
    else
        m_invMass = 0.f;
}

void PhysicsObject::setInertia(float inertia)
{
    m_inertia = inertia;
    if (inertia != 0.f)
        m_invInertia = 1.f / m_inertia;
    else
        m_invInertia = 0.f;
}

void PhysicsObject::setLinearVelocity(const sf::Vector2f& velocity) { m_linearVelocity = velocity; }

auto PhysicsObject::getLinearVelocity() const -> sf::Vector2f { return m_linearVelocity; }

void PhysicsObject::setAngularVelocity(float angularVel) { m_angularVelocity = angularVel; }

auto PhysicsObject::getAngularVelocity() const -> float { return m_angularVelocity; }

auto PhysicsObject::isPhysicsBodyActive() const -> bool { return m_active; }

void PhysicsObject::clearForces() { m_force = {}; }

void PhysicsObject::clearTorque() { m_torque = 0.f; }

} // nce namespace
