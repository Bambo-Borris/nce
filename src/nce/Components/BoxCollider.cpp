#include "../../stdafx.h"

#include "nce/App.hpp"
#include "nce/Components/BoxCollider.hpp"
#include "nce/GameObject.hpp"

namespace nce {
BoxCollider::BoxCollider(const sf::Vector2f& size, GameObject* go, i32 mask)
    : ColliderBase(ComponentTags::BoxCollider, go, mask)
    , m_size(size)
{
}

ErrResult BoxCollider::init()
{
    const auto activeState = getParentObject()->getAppState();
    const auto solver = activeState->getSolver();
    assert(solver);

    auto colliderInfo = MakeOBBCollider(m_size / 2.f, getParentObject(), getParentObject(), m_collisionMask);
    const auto solverReturn = solver->addCollider(colliderInfo);
    if (!solverReturn)
        return ErrVal("Unable to add AABB collider to solver");

    m_colliderID = *solverReturn;

    return ColliderBase::init();
}
} // nce namespace