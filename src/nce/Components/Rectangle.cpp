#include "../../stdafx.h"

#include "nce/Components/Rectangle.hpp"

#include "nce/Components/Tags.hpp"
#include "nce/GameObject.hpp"

namespace nce {

Rectangle::Rectangle(const sf::Vector2f& size, GameObject* parentObject)
    : Renderable(ComponentNames[ComponentTags::Rectangle], ComponentTags::Rectangle, parentObject)
    , m_shape(size)
{
}

void Rectangle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getParentObject()->getTransform();
    target.draw(m_shape, states);
}
} // nce namespace