#include "../../stdafx.h"

#include "nce/App.hpp"
#include "nce/Components/CircleCollider.hpp"
#include "nce/GameObject.hpp"

namespace nce {

CircleCollider::CircleCollider(f32 radius, GameObject* parentObject, i32 mask)
    : ColliderBase(ComponentTags::CircleCollider, parentObject, mask)
    , m_radius(radius)
{
}

ErrResult CircleCollider::init()
{
    const auto activeState = getParentObject()->getAppState();
    const auto solver = activeState->getSolver();
    assert(solver);

    auto colliderInfo = MakeCircleCollider(m_radius, getParentObject(), getParentObject(), m_collisionMask);
    const auto solverReturn = solver->addCollider(colliderInfo);
    if (!solverReturn)
        return ErrVal("Unable to add Circle collider to solver");

    m_colliderID = *solverReturn;

    return ColliderBase::init();
}
} // nce namespace