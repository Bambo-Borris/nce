#include "../../stdafx.h"

#include "nce/Components/ColliderBase.hpp"

namespace nce {

ErrResult ColliderBase::onExitState()
{
    const auto activeState = NCE_APP().getActiveState();
    const auto solver = activeState->getSolver();

    solver->removeCollider(m_colliderID);

    return Component::onExitState();
}

void ColliderBase::subscribeCollisionCallback(const CollisionSolver::OnCollisionCallback& info)
{
    const auto activeState = NCE_APP().getActiveState();
    const auto solver = activeState->getSolver();
    m_subIDCollision = solver->subscribeOnCollisionCallback({ m_colliderID, info });
}

void ColliderBase::unsubCollisionCallback()
{
    const auto activeState = NCE_APP().getActiveState();
    const auto solver = activeState->getSolver();
    if (m_subIDCollision)
        solver->unsubscribeOnCollisionCallBack(*m_subIDCollision);
}

void ColliderBase::subscribeCollisionExitCallback(const CollisionSolver::OnCollisionExitCallback& info)
{
    const auto activeState = NCE_APP().getActiveState();
    const auto solver = activeState->getSolver();
    m_subIDCollisionExit = solver->subscribeCollisionExitCallback({ m_colliderID, info });
}

void ColliderBase::unsubCollisionExitCallback()
{
    const auto activeState = NCE_APP().getActiveState();
    const auto solver = activeState->getSolver();
    if (m_subIDCollisionExit)
        solver->unsubscribeCollisionExitCallBack(*m_subIDCollisionExit);
}

sf::FloatRect ColliderBase::getBoundedBox() const
{
    auto colliderInfo = NCE_APP().getActiveState()->getSolver()->getColliderInfo(m_colliderID);
    assert(colliderInfo);
    return nce::BoundingBoxFromCollider(*colliderInfo);
}
}