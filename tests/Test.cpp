#include <catch2/catch_all.hpp>

#include <fstream>
#include <memory>
#include <nce/AppState.hpp>
#include <nce/CollisionSolver.hpp>
#include <nce/ConfigFile.hpp>
#include <nce/GameObject.hpp>
#include <nce/TimerController.hpp>
#include <nce/TweeningAnimation.hpp>

TEST_CASE("NCE Tests")
{
    SECTION("Collision Solver - Unit Tests [Utility Functionality]")
    {
        // TODO update tests to reflect new way to use solver
        //        using namespace nce;
        //        class DummyObj : nce::GameObject, public Collideable<OBBCollider> {
        //        public:
        //            DummyObj()
        //                : GameObject(nullptr)
        //                , Collideable(this)
        //            {
        //            }
        //            virtual void draw(sf::RenderTarget&, const sf::RenderStates&) const override { }
        //            virtual void update(const sf::Time& dt, sf::RenderWindow& window) override { }
        //        };
        //
        //        auto a = std::shared_ptr<DummyObj>(new DummyObj);
        //        auto b = std::shared_ptr<DummyObj>(new DummyObj);
        //
        //        CollisionSolver cs { { 10, 10 }, { 20, 20 } };
        //
        //        CHECK(cs.getCellSize() == sf::Vector2u { 10, 10 });
        //        CHECK(cs.getGridSize() == sf::Vector2u { 20, 20 });
        //
        //        // Test collideable insertion
        //        CHECK(cs.addCollider(a.get()));
        //        CHECK(cs.addCollider(b.get()));
        //        CHECK(cs.getNumberOfColliders() == 2);
        //
        //        // Test collideable removal
        //        CHECK(cs.removeCollider(a.get()));
        //        CHECK(cs.removeCollider(b.get()));
        //        CHECK(cs.getNumberOfColliders() == 0);
        //
        //        CollisionSolver::OnCollisionCallback cbA;
        //        CollisionSolver::OnCollisionCallback cbB;
        //
        //        // Check subscribing for collision notification
        //        CHECK(cs.subscribeOnCollisionCallback({ a.get(), &cbA }));
        //        CHECK(cs.subscribeOnCollisionCallback({ b.get(), &cbB }));
        //        CHECK(cs.getNumberOfSubscribers() == 2);
        //
        //        // Check unsubscribing for collision notification
        //        CHECK(cs.unsubscribeOnCollisionCallBack({ a.get(), &cbA }));
        //        CHECK(cs.unsubscribeOnCollisionCallBack({ b.get(), &cbB }));
        //        CHECK(cs.getNumberOfSubscribers() == 0);
        //
        //// Test invalid args
        // #ifndef _DEBUG
        //         CHECK(!cs.addCollider(nullptr));
        //         CHECK(!cs.removeCollider(nullptr));
        //         CHECK(!cs.subscribeOnCollisionCallback({ nullptr, nullptr }));
        //         CHECK(!cs.unsubscribeOnCollisionCallBack({ nullptr, nullptr }));
        // #endif
    }

    SECTION("TweeningAnimation - Unit Tests [Utility Functionality]")
    {
        nce::TimerController tc;
        nce::TAFloat animation(nce::AnimationCurve::Lerp, "test1", 0.f, 20.f, sf::seconds(1.f), &tc);
        CHECK(!animation.isComplete());
        CHECK(!animation.isPlaying());

        animation.play();
        CHECK(animation.isPlaying());

        for (auto i { 0 }; i < 61; ++i) {
            const auto v { animation.getTweenedValue() };
            NCE_UNUSED(v);
            tc.update(sf::seconds(1.f / 60.f));
        }
        CHECK(animation.getTweenedValue() == 20.f);
        CHECK(animation.isComplete());
        CHECK(!animation.isPlaying());

        nce::TAVec2f animation2(nce::AnimationCurve::Lerp, "test2", { 0.f, 0.f }, { 0.f, 20.f }, sf::seconds(1.f), &tc);
        animation2.play();
        for (auto i { 0 }; i < 61; ++i) {
            const auto v { animation2.getTweenedValue() };
            NCE_UNUSED(v);
            tc.update(sf::seconds(1.f / 60.f));
        }

        CHECK(animation2.getTweenedValue() == sf::Vector2f(0.f, 20.f));
        CHECK(animation2.isComplete());
        CHECK(!animation2.isPlaying());
    }

    SECTION("ConfigFile - Unit Tests [Utility Functionality]")
    {
        // We need a test file before we can do anything
        // so we'll write it to disk
        const auto basicTestFileContents =
            R"(# Test Comment
test_int 24
test_float 48.5f
test_string hello world how are you doing)";

        std::ofstream outputFile;
        outputFile.open("basicTest.cfg", std::ios::out | std::ios::trunc);
        CHECK(!outputFile.fail());
        outputFile << basicTestFileContents;
        outputFile.close();

        std::map<std::string, nce::ConfigFile::ConfigFieldType> spec;
        spec["test_int"] = nce::ConfigFile::ConfigFieldType::Int;
        spec["test_float"] = nce::ConfigFile::ConfigFieldType::Float;
        spec["test_string"] = nce::ConfigFile::ConfigFieldType::String;

        nce::ConfigFile cfgFile("basicTest.cfg", spec);
        CHECK(cfgFile.load());
        CHECK(cfgFile.getValue<int>("test_int"));
        CHECK(cfgFile.getValue<int>("test_int").value() == 24);

        CHECK(cfgFile.getValue<float>("test_float"));
        CHECK(cfgFile.getValue<float>("test_float") == 48.5f);

        CHECK(cfgFile.getValue<std::string>("test_string"));
        CHECK(cfgFile.getValue<std::string>("test_string") == "hello world how are you doing");

        cfgFile.setValue("test_int", 90);
        CHECK(cfgFile.getValue<int>("test_int") == 90);

        cfgFile.setValue("test_float", 3.5f);
        CHECK(cfgFile.getValue<float>("test_float") == 3.5f);

        cfgFile.setValue<std::string>("test_string", "new string");
        CHECK(cfgFile.getValue<std::string>("test_string") == "new string");

        cfgFile.save();

        // Verify the saved file yields the changed values when read back again
        nce::ConfigFile cfgFile2("basicTest.cfg", spec);
        CHECK(cfgFile2.load());
        CHECK(cfgFile2.getValue<int>("test_int"));
        CHECK(cfgFile2.getValue<int>("test_int").value() == 90);

        CHECK(cfgFile2.getValue<float>("test_float"));
        CHECK(cfgFile2.getValue<float>("test_float") == 3.5f);

        CHECK(cfgFile2.getValue<std::string>("test_string"));
        CHECK(cfgFile2.getValue<std::string>("test_string") == "new string");
    }

    SECTION("TimerController - Unit Tests [Utility Functionality]")
    {
        nce::TimerController tc;

        CHECK(!tc.exists("test_timer1"));
        tc.addTimer("test_timer1");
        CHECK(tc.exists("test_timer1"));

        CHECK(!tc.isPaused("test_timer1"));
        tc.addTimer("test_timer2", true);
        CHECK(tc.isPaused("test_timer2"));

        for (auto i { 0 }; i < 60; ++i) {
            tc.update(sf::milliseconds(1));
        }
        CHECK(tc.getElapsed("test_timer1") == sf::milliseconds(60));
        CHECK(tc.getElapsed("test_timer2") == sf::milliseconds(0));

        tc.pauseTimer("test_timer1");
        tc.playTimer("test_timer2");
        for (auto i { 0 }; i < 60; ++i) {
            tc.update(sf::milliseconds(1));
        }
        CHECK(tc.getElapsed("test_timer1") == sf::milliseconds(60));
        CHECK(tc.getElapsed("test_timer2") == sf::milliseconds(60));

        tc.playTimer("test_timer1");
        tc.resetTimer("test_timer1");
        tc.resetTimer("test_timer2", true);
        CHECK(tc.getElapsed("test_timer1") == sf::milliseconds(0));
        CHECK(tc.getElapsed("test_timer1") == sf::milliseconds(0));
        CHECK(!tc.isPaused("test_timer1"));
        CHECK(tc.isPaused("test_timer2"));

        tc.removeTimer("test_timer2");
        CHECK(!tc.exists("test_timer2"));

        const auto allTimerNames = tc.getKeys();
        CHECK(allTimerNames.size() == 1);
        CHECK(allTimerNames[0] == "test_timer1");
    }

    SECTION("Packed Asset Loading - Unit Tests [Utility Functionality]") { }
}
